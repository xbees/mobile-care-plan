﻿using Newtonsoft.Json;

namespace MobileCarePlan.Service.Models
{
    public class CarePlanDto
    {
        public CarePlanDto(Guid serviceUserId, CarePlanLastUpdateDto lastUpdate, IEnumerable<CarePlanSectionItemDto> sections)
        {
            ServiceUserId = serviceUserId;
            LastUpdate = lastUpdate;
            Sections = sections;
        }

        [JsonProperty("service_user_id")]
        public Guid ServiceUserId { get; set; }

        [JsonProperty("last_update")]
        public CarePlanLastUpdateDto LastUpdate { get; }

        [JsonProperty("sections")]
        public IEnumerable<CarePlanSectionItemDto> Sections { get; }
    }
}