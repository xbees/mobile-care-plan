﻿using Newtonsoft.Json;

namespace MobileCarePlan.Service.Models
{
    public class CarePlanLastUpdateDto
    {
        public CarePlanLastUpdateDto(DateTime timestamp, string author, IEnumerable<string> updatedSections = null)
        {
            Timestamp = timestamp;
            Author = author;
            UpdatedSections = updatedSections;
        }

        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; }

        [JsonProperty("author")]
        public string Author { get; }

        [JsonProperty("updated_sections")]
        public IEnumerable<string> UpdatedSections { get; }
    }
}