﻿using Newtonsoft.Json;

namespace MobileCarePlan.Service.Models
{
    public class CarePlanSectionContentItemDto
    {
        public CarePlanSectionContentItemDto(string id, string title, string content, string type, bool needsReview, ulong sortOrder)
        {
            Id = id;
            Title = title;
            Content = content;
            Type = type;
            NeedsReview = needsReview;
            SortOrder = sortOrder;
        }

        [JsonProperty("id")]
        public string Id { get; }

        [JsonProperty("title")]
        public string Title { get; }

        [JsonProperty("content")]
        public string Content { get; }

        [JsonProperty("type")]
        public string Type { get; }

        [JsonProperty("needsReview")]
        public bool NeedsReview { get; }

        [JsonProperty("sortOrder")]
        public ulong SortOrder { get; }
    }
}