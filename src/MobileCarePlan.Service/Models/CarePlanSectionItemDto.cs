﻿using Newtonsoft.Json;

namespace MobileCarePlan.Service.Models
{
    public class CarePlanSectionItemDto
    {
        public CarePlanSectionItemDto(string id, string title, string subtitle, string category, int sortOrder, bool needsReview, DateTime? lastUpdatedAt, IEnumerable<CarePlanSectionContentItemDto> contents)
        {
            Id = id;
            Title = title;
            Subtitle = subtitle;
            Category = category;
            SortOrder = sortOrder;
            LastUpdatedAt = lastUpdatedAt;
            NeedsReview = needsReview;
            Contents = contents;
        }

        [JsonProperty("id")]
        public string Id { get; }

        [JsonProperty("title")]
        public string Title { get; }

        [JsonProperty("subtitle")]
        public string Subtitle { get; }

        [JsonProperty("category")]
        public string Category { get; }

        [JsonProperty("sortOrder")]
        public int SortOrder { get; }

        [JsonProperty("needsReview")]
        public bool NeedsReview { get; }

        [JsonProperty("lastUpdated")]
        public DateTime? LastUpdatedAt { get; }

        [JsonProperty("contents")]
        public IEnumerable<CarePlanSectionContentItemDto> Contents { get; }
    }
}
