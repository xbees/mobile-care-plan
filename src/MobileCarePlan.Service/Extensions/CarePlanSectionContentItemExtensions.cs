﻿using MobileCarePlan.Data.Models;

namespace MobileCarePlan.Service.Extensions
{
    internal static class CarePlanSectionContentItemExtensions
    {
        public static bool HasChanged(this CarePlanSectionContentItem previousCarePlanSectionContentItem, CarePlanSectionContentItem newCarePlanSectionContentItem)
        {
            if (newCarePlanSectionContentItem is null || previousCarePlanSectionContentItem is null)
            {
                return true;
            }


            return !string.Equals(previousCarePlanSectionContentItem.Content, newCarePlanSectionContentItem.Content, StringComparison.OrdinalIgnoreCase);
        }
    }
}
