﻿using MobileCarePlan.Data.Models;

namespace MobileCarePlan.Service.Extensions
{
    internal static class CarerReviewDataExtensions
    {
        public static DateTime GetSectionReviewDateTime(this CarerReviewData carerReviewData, string sectionId)
        {
            DateTime carerReviewedSectionAt = new(1970, 1, 1);
            if(carerReviewData != null && carerReviewData.SectionReviews != null)
                carerReviewData.SectionReviews.TryGetValue(sectionId, out carerReviewedSectionAt);
            return carerReviewedSectionAt;
        }
    }
}
