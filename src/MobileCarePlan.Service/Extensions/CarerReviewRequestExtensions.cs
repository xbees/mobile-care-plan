﻿using MobileCarePlan.Service.CarerReviewContracts;

namespace MobileCarePlan.Service.Extensions
{
    public static class CarerReviewRequestExtensions
    {
        public static void Validate(this CarerReviewRequest carerReviewRequest)
        {
            if (carerReviewRequest == null)
                throw new ArgumentNullException(nameof(carerReviewRequest));

            if (!CarerReviewHelper.GetCarePlanSectionsList().Contains(carerReviewRequest.SectionId))
                throw new ArgumentException($"SectionId '{carerReviewRequest.SectionId}' is not valid");
        }
    }
}
