﻿using System.ComponentModel;
using System.Reflection;
using System.Runtime.Serialization;

namespace MobileCarePlan.Service.Extensions
{
    internal static class DisplayTextExtensions
    {
        public static string GetDescription(this Enum value)
        {
            if (value == null)
                return string.Empty;

            FieldInfo field = value.GetType().GetField(value.ToString())!;

            object[] attribs = field.GetCustomAttributes(typeof(DescriptionAttribute), true);

            if (attribs.Length > 0)
                return ((DescriptionAttribute)attribs[0]).Description;

            attribs = field.GetCustomAttributes(typeof(EnumMemberAttribute), true);

            if (attribs.Length > 0)
                return ((EnumMemberAttribute)attribs[0]).Value;

            return value.ToString();
        }
    }
}
