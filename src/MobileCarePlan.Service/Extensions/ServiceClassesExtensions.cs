﻿using System.Collections;
using System.Text.RegularExpressions;
using MobileCarePlan.Data.Models;
using MobileCarePlan.Service.CarePlanEventContracts;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.Extensions
{
    public static class CarePlanPublishedEventExtensions
    {
        public static CarePlanPublishedEventData ToCarePlanPublishedEventData(this CareplanPublishedV1 carePlanPublishedEvent)
        {
            return new CarePlanPublishedEventData
            {
                ServiceUserId = carePlanPublishedEvent.Client.Guid,
                DateTime = carePlanPublishedEvent.PublishedAt.UtcDateTime,
                CreatedBy = carePlanPublishedEvent.PublishedBy.Guid,
                EventId = carePlanPublishedEvent.Guid,
                Event = JsonConvert.SerializeObject(carePlanPublishedEvent)
            };
        }

        public static CarePlanData ToCarePlanData(this CareplanPublishedV1 carePlanPublishedEvent, CarePlanData previousCarePlanData)
        {
            var carePlan = new CarePlanData
            {
                ServiceUserId = carePlanPublishedEvent.Client.Guid,
                DateTime = carePlanPublishedEvent.PublishedAt.UtcDateTime,
                LastEventId = carePlanPublishedEvent.Guid,
                LastUpdatedAt = carePlanPublishedEvent.PublishedAt.UtcDateTime,
                LastUpdatedById = carePlanPublishedEvent.PublishedBy.Guid,
                LastUpdatedByName = carePlanPublishedEvent.PublishedBy.Name,
                Sections = carePlanPublishedEvent.SectionsWithData.GetType().GetProperties()
                .Where(p => p.GetCustomAttributes(false).Any(a => a is SectionMapAttribute))
                .Aggregate(new List<CarePlanSectionItem>(), (acc, p) =>
                {
                    var map = (SectionMapAttribute)p.GetCustomAttributes(false).FirstOrDefault(a => a is SectionMapAttribute);
                    var section = p.GetValue(carePlanPublishedEvent.SectionsWithData) as Section;
                    if (section != null)
                    {
                        uint contentOrder = 0;
                        var contents = MapToCarePlanContentItems(section, ref contentOrder).OrderBy(s => s.SortOrder).ToArray();

                        var sectionItem = new CarePlanSectionItem
                        {
                            Id = map.Name,
                            Category = "main",
                            Title = section.Title,
                            Subtitle = section.Description,
                            SortOrder = map.Order,
                            Contents = contents
                        };

                        if (previousCarePlanData is not null)
                        {
                            var previousSection = previousCarePlanData.Sections.FirstOrDefault(s => string.Equals(s.Id, map.Name));

                            var hasChanges = false;
                            foreach (var c in sectionItem.Contents)
                            {
                                var previousContent = previousSection?.Contents.FirstOrDefault(x => string.Equals(x.Id, c.Id));
                                var contentChanged = previousContent.HasChanged(c);
                                hasChanges |= contentChanged;

                                c.LastUpdatedAt = (contentChanged || previousContent == null ? carePlanPublishedEvent.PublishedAt : (previousContent.LastUpdatedAt ?? previousCarePlanData.LastUpdatedAt)).UtcDateTime;
                            }

                            if (previousSection?.Contents.Any(c => !sectionItem.Contents.Any(x => string.Equals(x.Id, c.Id))) == true)
                            {
                                hasChanges = true;
                            }

                            sectionItem.LastUpdatedAt = (hasChanges || previousSection == null ? carePlanPublishedEvent.PublishedAt : (previousSection.LastUpdatedAt ?? previousCarePlanData.LastUpdatedAt)).UtcDateTime;
                        }
                        else
                        {
                            sectionItem.LastUpdatedAt = carePlanPublishedEvent.PublishedAt.UtcDateTime;
                        }

                        acc.Add(sectionItem);
                    }

                    return acc;
                })
                .OrderBy(s => s.SortOrder).ToList()
            };

            return carePlan;

            IEnumerable<CarePlanSectionContentItem> MapToCarePlanContentItems(object boundedContentValue, ref uint contentOrder, string parentContentName = null, IEnumerable<OverrideChildContentMapAttribute> overrideChildContentMaps = null)
            {
                var contents = new List<CarePlanSectionContentItem>();

                if (boundedContentValue == null)
                {
                    return contents;
                }

                var contentInfos = boundedContentValue.GetType().GetProperties()
                    .Where(p => p.GetCustomAttributes(false).Any(a => a is ContentMapAttribute))
                    .Select(p =>
                    {
                        var currentContentMap = (ContentMapAttribute)p.GetCustomAttributes(false).FirstOrDefault(a => a is ContentMapAttribute);
                        var overridechildMaps = p.GetCustomAttributes(false).Where(a => a is OverrideChildContentMapAttribute).Select(a => (OverrideChildContentMapAttribute)a).ToList();

                        var contentId = string.IsNullOrWhiteSpace(parentContentName) ? currentContentMap.Name : $"{parentContentName}.{currentContentMap.Name}";

                        var overrideMap = overrideChildContentMaps?.FirstOrDefault(cm =>
                        {
                            var pattern = @"^([a-zA-Z0-9_]+(\[[0-9]+\])*\.)*" + Regex.Escape(cm.ChildName) + @"$";
                            var regex = new Regex(pattern);
                            return regex.IsMatch(contentId);
                        });
                        if (overrideMap != null)
                        {
                            currentContentMap.Override(overrideMap.Title, overrideMap.Order, overrideMap.Type);
                        }

                        var resOverridechildMaps = new List<OverrideChildContentMapAttribute>();
                        if (overridechildMaps?.Any() == true)
                        {
                            resOverridechildMaps.AddRange(overridechildMaps);
                        }

                        if (overrideChildContentMaps?.Any() == true)
                        {
                            resOverridechildMaps.AddRange(overrideChildContentMaps.Where(cm => cm.ChildName != overrideMap?.ChildName).ToList());
                        }

                        return new
                        {
                            ContentId = contentId,
                            ContentProperty = p,
                            ContentMap = currentContentMap,
                            OverrideChildContentMaps = resOverridechildMaps
                        };
                    })
                    .OrderBy(c => c.ContentMap.Order).ToList();

                foreach (var contentInfo in contentInfos)
                {
                    var contentId = contentInfo.ContentId;

                    var contentPropertyType = Nullable.GetUnderlyingType(contentInfo.ContentProperty.PropertyType) ?? contentInfo.ContentProperty.PropertyType;
                    var contentPropertyValue = contentInfo.ContentProperty.GetValue(boundedContentValue);

                    if (contentPropertyValue == null)
                    {
                        continue;
                    }

                    if (contentPropertyType.IsEnum || contentPropertyType.IsPrimitive || contentPropertyType == typeof(string))
                    {
                        if (string.IsNullOrEmpty(contentPropertyValue.ToString()) && !string.Equals(contentInfo.ContentMap.Type, "header", StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }

                        var content = contentPropertyValue.ToString();
                        if (contentPropertyType == typeof(bool))
                        {
                            content = (bool)contentPropertyValue ? "Yes" : "No";
                        }
                        else if (contentPropertyType.IsEnum)
                        {
                            content = ((Enum)contentPropertyValue).GetDescription();
                        }

                        contents.Add(new CarePlanSectionContentItem
                        {
                            Id = contentId,
                            Title = contentInfo.ContentMap.Title ?? "N/A",
                            Content = content,
                            Type = contentInfo.ContentMap.Type,
                            SortOrder = ++contentOrder
                        });
                    }
                    else if (contentPropertyValue is IEnumerable || contentPropertyType.GetInterface(typeof(IEnumerable<>).FullName) != null)
                    {
                        var items = new List<CarePlanSectionContentItem>();
                        if (!string.IsNullOrWhiteSpace(contentInfo.ContentMap.Title))
                        {
                            items.Add(new CarePlanSectionContentItem
                            {
                                Id = contentId,
                                Title = contentInfo.ContentMap.Title,
                                Type = $"header",
                                SortOrder = ++contentOrder
                            });
                        }

                        uint index = 0;
                        foreach (var p in contentPropertyValue as IEnumerable)
                        {
                            items.AddRange(MapToCarePlanContentItems(p, ref contentOrder, $"{contentId}[{index}]", contentInfo.OverrideChildContentMaps));
                            index++;
                        }
                        if (index > 0)
                        {
                            contents.AddRange(items);
                        }
                    }
                    else if (contentPropertyType.IsClass)
                    {
                        if (!string.IsNullOrWhiteSpace(contentInfo.ContentMap.Title))
                        {
                            contents.Add(new CarePlanSectionContentItem
                            {
                                Id = contentId,
                                Title = contentInfo.ContentMap.Title,
                                Type = $"header",
                                SortOrder = ++contentOrder
                            });
                        }

                        contents.AddRange(MapToCarePlanContentItems(contentPropertyValue, ref contentOrder, contentId, contentInfo.OverrideChildContentMaps));
                    }
                }

                return contents;
            }
        }
    }
}