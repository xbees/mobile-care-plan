﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MobileCarePlan.Data;

namespace MobileCarePlan.Service.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterServicesScoped(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            return serviceCollection
                .AddScoped<IMobileCarePlanPublishedEventRepository, MobileCarePlanPublishedEventRepository>()
                .AddScoped<IMobileCarePlanRepository, MobileCarePlanRepository>()
                .AddScoped<ICarerReviewRepository, CarerReviewRepository>()
                .AddScoped<IMobileCarePlanService, MobileCarePlanService>()
                .AddScoped<ICarerReviewService, CarerReviewService>()
                .Configure<StorageSettings>(configuration.GetSection("Storage"));
        }

        public static IServiceCollection RegisterDynamoDB(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            var awsOptions = configuration.GetAWSOptions();
            return serviceCollection
                .AddDefaultAWSOptions(awsOptions)
                .AddSingleton<IDynamoDBContext, DynamoDBContext>()
                .AddAWSService<IAmazonDynamoDB>();
        }
    }
}
