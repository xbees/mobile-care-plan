﻿using MobileCarePlan.Data.Models;
using MobileCarePlan.Service.Models;

namespace MobileCarePlan.Service.Extensions
{
    public static class CarePlanDataExtensions
    {
        public static CarePlanDto ToCarePlanDto(this CarePlanData carePlanData, CarerReviewData carerReviewData)
        {
            if (carePlanData is null)
            {
                return null;
            }

            var carePlanLastUpdateDto = new CarePlanLastUpdateDto(carePlanData.LastUpdatedAt.ToUniversalTime(), carePlanData.LastUpdatedByName);
            return new CarePlanDto(
                carePlanData.ServiceUserId,
                carePlanLastUpdateDto,
                carePlanData.Sections?
                .Select(x => x.ToCarePlanSectionItemDto(x.LastUpdatedAt ?? carePlanData.LastUpdatedAt, carerReviewData.GetSectionReviewDateTime(x.Id)))
                .Where(x => x is not null).OrderBy(x => x.SortOrder).ToArray());
        }

        public static CarePlanSectionItemDto ToCarePlanSectionItemDto(this CarePlanSectionItem carePlanSectionItem, DateTime sectionLastUpdatedAt, DateTime carerReviewedSectionAt)
        {
            if (carePlanSectionItem is null)
            {
                return null;
            }

            var needsReview = carerReviewedSectionAt < sectionLastUpdatedAt;

            return new CarePlanSectionItemDto(
                carePlanSectionItem.Id,
                carePlanSectionItem.Title,
                carePlanSectionItem.Subtitle,
                carePlanSectionItem.Category,
                carePlanSectionItem.SortOrder,
                needsReview,
                carePlanSectionItem.LastUpdatedAt,
                carePlanSectionItem.Contents?
                .Select(x => x.ToCarePlanSectionContentItemDto(sectionLastUpdatedAt, carerReviewedSectionAt))
                .Where(x => x is not null).OrderBy(x => x.SortOrder).ToArray()
            );
        }

        public static CarePlanSectionContentItemDto ToCarePlanSectionContentItemDto(this CarePlanSectionContentItem carePlanSectionContentItem, DateTime sectionLastUpdatedAt, DateTime carerReviewedSectionAt)
        {
            if (carePlanSectionContentItem is null)
            {
                return null;
            }

            var needsReview = carerReviewedSectionAt < (carePlanSectionContentItem.LastUpdatedAt ?? sectionLastUpdatedAt);

            return new CarePlanSectionContentItemDto(
                carePlanSectionContentItem.Id,
                carePlanSectionContentItem.Title,
                carePlanSectionContentItem.Content,
                carePlanSectionContentItem.Type,
                needsReview,
                carePlanSectionContentItem.SortOrder
            );
        }
    }
}
