﻿using MobileCarePlan.Service.CarePlanEventContracts;
using MobileCarePlan.Service.Models;

namespace MobileCarePlan.Service
{
    public interface IMobileCarePlanService
    {
        Task<CarePlanLastUpdateDto> GetCarePlanLastUpdateAsync(Guid serviceUserId, Guid carerId, CancellationToken cancellationToken = default);

        Task<CarePlanDto> GetCarePlanAsync(Guid serviceUserId, Guid carerId, CancellationToken cancellationToken = default);

        Task UpdateCarePlanAsync(CareplanPublishedV1 carePlanPublishedEvent, CancellationToken cancellationToken = default);
    }
}
