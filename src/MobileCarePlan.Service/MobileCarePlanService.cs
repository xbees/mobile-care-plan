﻿using MobileCarePlan.Data;
using MobileCarePlan.Service.CarePlanEventContracts;
using MobileCarePlan.Service.Extensions;
using MobileCarePlan.Service.Models;

namespace MobileCarePlan.Service
{
    public class MobileCarePlanService : IMobileCarePlanService
    {
        private readonly IMobileCarePlanRepository _carePlanRepository;
        private readonly IMobileCarePlanPublishedEventRepository _carePlanPublishedEventRepository;
        private readonly ICarerReviewRepository _carerReviewRepository;

        public MobileCarePlanService(IMobileCarePlanPublishedEventRepository carePlanPublishedEventRepository, IMobileCarePlanRepository carePlanRepository, ICarerReviewRepository carerReviewRepository)
        {
            _carePlanPublishedEventRepository = carePlanPublishedEventRepository;
            _carePlanRepository = carePlanRepository;
            _carerReviewRepository = carerReviewRepository;
        }

        public async Task<CarePlanLastUpdateDto> GetCarePlanLastUpdateAsync(Guid serviceUserId, Guid carerId, CancellationToken cancellationToken = default)
        {
            var careplan = await GetCarePlanAsync(serviceUserId, carerId, cancellationToken).ConfigureAwait(false);

            if (careplan == null)
                return null;

            return new CarePlanLastUpdateDto(careplan.LastUpdate.Timestamp,
                                             careplan.LastUpdate.Author,
                                             careplan.Sections.Where(s => s.NeedsReview).Select(x => x.Id).ToArray());
        }

        public async Task<CarePlanDto> GetCarePlanAsync(Guid serviceUserId, Guid carerId, CancellationToken cancellationToken = default)
        {
            var cpData = await _carePlanRepository.GetAsync(serviceUserId, cancellationToken).ConfigureAwait(false);

            if (cpData == null)
                return null;

            var carerReviewData = await _carerReviewRepository.GetAsync(carerId, serviceUserId, cancellationToken).ConfigureAwait(false);
            return cpData.ToCarePlanDto(carerReviewData);
        }

        public async Task UpdateCarePlanAsync(CareplanPublishedV1 carePlanPublishedEvent, CancellationToken cancellationToken = default)
        {
            await _carePlanPublishedEventRepository.SaveAsync(carePlanPublishedEvent.ToCarePlanPublishedEventData(), cancellationToken).ConfigureAwait(false);
            var previousCarePlan = await _carePlanRepository.GetAsync(carePlanPublishedEvent.Client.Guid, cancellationToken).ConfigureAwait(false);
            var currentCarePlan = carePlanPublishedEvent.ToCarePlanData(previousCarePlan);
            await _carePlanRepository.SaveAsync(currentCarePlan, cancellationToken).ConfigureAwait(false);
        }
    }
}
