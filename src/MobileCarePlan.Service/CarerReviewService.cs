﻿using MobileCarePlan.Data;
using MobileCarePlan.Service.CarerReviewContracts;

namespace MobileCarePlan.Service
{
    public class CarerReviewService : ICarerReviewService
    {
        private readonly ICarerReviewRepository _carerReviewRepository;

        public CarerReviewService(ICarerReviewRepository carerReviewRepository)
        {
            _carerReviewRepository = carerReviewRepository;
        }

        public async Task UpdateCarerReviewAsync(Guid carerId, Guid serviceUserId, CarerReviewRequest carerReviewUpdate, CancellationToken cancellationToken = default)
        {
            await _carerReviewRepository.UpdateAsync(carerId, serviceUserId, carerReviewUpdate.SectionId, carerReviewUpdate.ReviewedAt.UtcDateTime, cancellationToken).ConfigureAwait(false);
        }
    }
}
