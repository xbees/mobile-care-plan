﻿using MobileCarePlan.Service.CarerReviewContracts;

namespace MobileCarePlan.Service
{
    public interface ICarerReviewService
    {
        Task UpdateCarerReviewAsync(Guid carerId, Guid serviceUserId, CarerReviewRequest carerReviewUpdate, CancellationToken cancellationToken = default);
    }
}
