﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarerReviewContracts
{
    public class CarerReviewRequest
    {
        [JsonProperty("section_id")]
        [JsonPropertyName("section_id")]
        public string SectionId { get; set; }

        [JsonProperty("reviewed_at")]
        [JsonPropertyName("reviewed_at")]
        public DateTimeOffset ReviewedAt { get; set; }
    }
}
