using System.ComponentModel;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Enums
{
    [JsonConverter(typeof(SafeStringEnumConverter), Unknown)]
    public enum CommunicationType
    {
        [Description("Directly with Service User")]
        DirectlyWithServiceUser,
        [Description("Nominated Person")]
        NominatedPerson,
        [Description("POA")]
        Poa,
        Unknown
    }
}
