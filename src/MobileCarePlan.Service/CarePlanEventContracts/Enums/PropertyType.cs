using System.ComponentModel;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Enums
{
    [JsonConverter(typeof(SafeStringEnumConverter), Unknown)]
    public enum PropertyType
    {
        [Description("Detached House")]
        DetachedHouse,
        [Description("Semi-detached House")]
        SemiDetachedHouse,
        [Description("Terraced House")]
        TerracedHouse,
        [Description("Town House")]
        Townhouse,
        [Description("Bungalow")]
        Bungalow,
        [Description("Flat")]
        Flat,
        [Description("Cottage")]
        Cottage,
        [Description("Other")]
        Other,
        Unknown
    }
}
