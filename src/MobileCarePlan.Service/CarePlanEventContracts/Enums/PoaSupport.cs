using System.ComponentModel;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Enums
{
    [JsonConverter(typeof(SafeStringEnumConverter), Unknown)]
    public enum PoaSupport
    {
        [Description("Deputies")]
        Deputies = 1,
        [Description("Health and Welfare")]
        HealthAndWelfare = 2,
        [Description("Property and Finance")]
        PropertyAndFinance = 3,
        [Description("Advocates")]
        Advocates = 4,
        Unknown
    }
}
