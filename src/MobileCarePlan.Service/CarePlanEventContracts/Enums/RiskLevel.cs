using System.ComponentModel;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Enums
{
    [JsonConverter(typeof(SafeStringEnumConverter), Unknown)]
    public enum RiskLevel
    {
        [Description("Low")]
        Low,
        [Description("Medium")]
        Medium,
        [Description("High")]
        High,
        Unknown
    }
}
