﻿namespace MobileCarePlan.Service.CarePlanEventContracts.Enums
{
    public enum PackageType
    {
        Standard = 0,
        LiveIn = 1,
        TwentyFourHour = 2,
        Complex = 3
    }
}
