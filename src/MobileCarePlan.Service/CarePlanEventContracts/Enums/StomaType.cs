using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Enums
{
    [JsonConverter(typeof(SafeStringEnumConverter), Unknown)]
    public enum StomaType
    {
        Colostomy = 1,
        Ileostomy = 2,
        Urostomy = 3,
        Unknown
    }
}
