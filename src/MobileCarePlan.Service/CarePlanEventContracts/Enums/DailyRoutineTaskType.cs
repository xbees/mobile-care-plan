﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Enums
{
    [JsonConverter(typeof(SafeStringEnumConverter), Unknown)]
    public enum DailyRoutineTaskType
    {
        [Description("In and out of bed")]
        InAndOutOfBed,
        [Description("In and out of bed to static chair")]
        InAndOutOfBedToStaticChair,
        [Description("In and out of bed to wheelchair/commode")]
        InAndOutOfBedToWheelchair,
        [Description("Wheelchair to static chair")]
        WheelchairToStaticChair,
        [Description("Toileting")]
        Toileting,
        [Description("In and out of bath/shower")]
        InAndOutOfBath,
        [Description("Dressing/undressing")]
        Dressing,
        [Description("Other")]
        Other,
        Unknown
    }
}
