using System.ComponentModel;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Enums
{
    [JsonConverter(typeof(SafeStringEnumConverter), Unknown)]
    public enum YesNoOption
    {
        [Description("Yes")]
        Yes = 1,
        [Description("No")]
        No = -1,
        [Description("N/A")]
        NotApplicable = 0,
        Unknown
    }
}
