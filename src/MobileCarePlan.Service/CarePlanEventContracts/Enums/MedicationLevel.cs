using System.Runtime.Serialization;

namespace Cera.Sdk.Dcp.CarePlan.Events.Enums
{
    public enum MedicationLevel
    {
        [EnumMember(Value = "NA")]
        NotDefined = 0,

        [EnumMember(Value = "1")]
        One = 1,

        [EnumMember(Value = "2")]
        Two = 2,

        [EnumMember(Value = "3")]
        Three = 3,

        [EnumMember(Value = "4")]
        Four = 4,

        [EnumMember(Value = "2/3")]
        TwoThirds = 5,

        [EnumMember(Value = "2/4")]
        TwoFourths = 6,

        [EnumMember(Value = "3/4")]
        ThreeFourths = 7
    }
}
