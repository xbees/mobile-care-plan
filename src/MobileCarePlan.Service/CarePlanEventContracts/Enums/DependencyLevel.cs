﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Enums
{
    [JsonConverter(typeof(SafeStringEnumConverter), Unknown)]
    public enum DependencyLevel
    {
        [Description("Low")]
        Low = 1,
        [Description("Medium")]
        Medium = 2,
        [Description("High")]
        High = 3,
        Unknown
    }
}
