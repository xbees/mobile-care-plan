using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Enums
{
    [JsonConverter(typeof(SafeStringEnumConverter), Unknown)]
    public enum DayPeriod
    {
        Morning,
        Lunch,
        TeaTime,
        Evening,
        Overnight,
        Unknown
    }
}
