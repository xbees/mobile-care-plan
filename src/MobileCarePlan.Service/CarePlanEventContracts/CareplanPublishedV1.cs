﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts
{
    public sealed class CareplanPublishedV1 : Careplan
    {
        [Required]
        [JsonRequired]
        [JsonProperty("published_at")]
        [JsonPropertyName("published_at")]
        public DateTimeOffset PublishedAt { get; set; }

        [Required]
        [JsonRequired]
        [JsonProperty("published_by")]
        [JsonPropertyName("published_by")]
        public UserOperator PublishedBy { get; set; }

        [Required]
        [JsonRequired]
        [JsonProperty("sections")]
        [JsonPropertyName("sections")]
        public SectionCollection SectionsWithData { get; set; } = new SectionCollection();

        public class SectionCollection
        {
            [SectionMap("personal_details", 1)]
            [JsonProperty("personal_details")]
            [JsonPropertyName("personal_details")]
            public Section<PersonalDetails> PersonalDetails { get; set; }

            [SectionMap("key_details", 2)]
            [JsonProperty("key_details")]
            [JsonPropertyName("key_details")]
            public Section<KeyDetails> KeyDetails { get; set; }

            [SectionMap("decisions_around_care", 3)]
            [JsonProperty("decisions_around_care")]
            [JsonPropertyName("decisions_around_care")]
            public Section<DecisionsAroundCare> DecisionsAroundCare { get; set; }

            [SectionMap("communications_and_accessibility", 4)]
            [JsonProperty("communications_and_accessibility")]
            [JsonPropertyName("communications_and_accessibility")]
            public Section<CommunicationsAndAccessibility> CommunicationsAndAccessibility { get; set; }

            [SectionMap("biography", 5)]
            [JsonProperty("biography")]
            [JsonPropertyName("biography")]
            public Section<Biography> Biography { get; set; }

            [SectionMap("health_conditions", 6)]
            [JsonProperty("health_conditions")]
            [JsonPropertyName("health_conditions")]
            public Section<List<HealthCondition>> HealthConditions { get; set; }

            [SectionMap("daily_routine", 7)]
            [JsonProperty("daily_routine")]
            [JsonPropertyName("daily_routine")]
            public Section<DailyRoutine> DailyRoutine { get; set; }

            [SectionMap("live_in_arrangements", 8)]
            [JsonProperty("live_in_arrangements")]
            [JsonPropertyName("live_in_arrangements")]
            public Section<LiveInArrangements> LiveInArrangements { get; set; }

            [SectionMap("health_and_wellbeing", 9)]
            [JsonProperty("health_and_wellbeing")]
            [JsonPropertyName("health_and_wellbeing")]
            public Section<HealthAndWellbeing> HealthAndWellbeing { get; set; }

            [SectionMap("infection_control", 10)]
            [JsonProperty("infection_control")]
            [JsonPropertyName("infection_control")]
            public Section<InfectionControl> InfectionControl { get; set; }

            [SectionMap("home_and_work_environment", 11)]
            [JsonProperty("home_and_work_environment")]
            [JsonPropertyName("home_and_work_environment")]
            public Section<HomeAndWorkEnvironment> HomeAndWorkEnvironment { get; set; }

            [SectionMap("medication", 12)]
            [JsonProperty("medication")]
            [JsonPropertyName("medication")]
            public Section<Medication> Medication { get; set; }

            [SectionMap("nutrition_and_hydration", 13)]
            [JsonProperty("nutrition_and_hydration")]
            [JsonPropertyName("nutrition_and_hydration")]
            public Section<NutritionAndHydration> NutritionAndHydration { get; set; }

            [SectionMap("continence", 14)]
            [JsonProperty("continence")]
            [JsonPropertyName("continence")]
            public Section<Continence> Continence { get; set; }

            [SectionMap("mobility", 15)]
            [JsonProperty("mobility")]
            [JsonPropertyName("mobility")]
            public Section<Mobility> Mobility { get; set; }

            [SectionMap("activities_and_financial_support", 16)]
            [JsonProperty("activities_and_financial_support")]
            [JsonPropertyName("activities_and_financial_support")]
            public Section<ActivitiesAndFinancialSupport> ActivitiesAndFinancialSupport { get; set; }

            [SectionMap("oxygen", 17)]
            [JsonProperty("oxygen")]
            [JsonPropertyName("oxygen")]
            public Section<Oxygen> Oxygen { get; set; }

            [SectionMap("overall_significant_risks", 18)]
            [JsonProperty("overall_significant_risks")]
            [JsonPropertyName("overall_significant_risks")]
            public Section<OverallSignificantRisks> OverallSignificantRisks { get; set; }
        }
    }
}
