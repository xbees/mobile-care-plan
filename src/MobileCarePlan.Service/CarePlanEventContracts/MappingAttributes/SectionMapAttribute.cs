﻿namespace MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    internal class SectionMapAttribute : Attribute
    {
        public SectionMapAttribute(string name, int order)
        {
            Name = name;
            Order = order;
        }

        public string Name { get; private set; }
        public int Order { get; private set; }
    }
}
