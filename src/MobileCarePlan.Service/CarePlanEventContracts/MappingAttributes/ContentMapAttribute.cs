﻿namespace MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    internal class ContentMapAttribute : Attribute
    {
        public ContentMapAttribute() { }

        public ContentMapAttribute(string name, string title = null, uint order = 0, string type = "text")
        {
            Name = name;
            Title = title;
            Order = order;
            Type = type;
        }

        public void Override(string title, uint order, string type)
        {
            if (!string.IsNullOrWhiteSpace(title))
                Title = title;

            if (order > 0 && Order != order)
                Order = order;

            if (!string.IsNullOrWhiteSpace(type))
                Type = type;
        }

        public string Name { get; private set; }
        public string Title { get; private set; }
        public string Type { get; private set; }
        public uint Order { get; private set; }
    }
}
