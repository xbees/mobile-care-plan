﻿namespace MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    internal class OverrideChildContentMapAttribute : Attribute
    {
        public OverrideChildContentMapAttribute(string childName, string title = null, uint order = 0, string type = null)
        {
            ChildName = childName;
            Title = title;
            Order = order;
            Type = type;
        }

        public string ChildName { get; private set; }
        public string Title { get; private set; }
        public string Type { get; private set; }
        public uint Order { get; private set; }
    }
}
