using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class Continence
    {
        [JsonProperty("support_with_continence", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("support_with_continence")]
        public string SupportWithContinence { get; set; }

        [JsonProperty("continence", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("continence")]
        public Hazard ContinenceHazard { get; set; }

        [JsonProperty("su_continence", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("su_continence")]
        public SuContinence SuContinence { get; set; }

        [JsonProperty("catheter", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("catheter")]
        public DetailedHazard Catheter { get; set; }

        [JsonProperty("stoma", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("stoma")]
        public Stoma Stoma { get; set; }
    }
}
