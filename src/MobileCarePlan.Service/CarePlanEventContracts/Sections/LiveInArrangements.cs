using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class LiveInArrangements
    {
        [JsonProperty("breaks_and_food", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("breaks_and_food")]
        public string BreaksAndFood { get; set; }

        [JsonProperty("sleeping_and_environmental", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("sleeping_and_environmental")]
        public string SleepingAndEnvironmental { get; set; }
    }
}
