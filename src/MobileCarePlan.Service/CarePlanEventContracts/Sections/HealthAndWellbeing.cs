using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.HealthAndWellbeing;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class HealthAndWellbeing
    {
        [JsonProperty("mental_wellbeing_diagnostic", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("mental_wellbeing_diagnostic")]
        public YesNoOption MentalWellbeingDiagnostic { get; set; }

        [JsonProperty("mental_wellbeing", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("mental_wellbeing")]
        public MentalWellbeing MentalWellbeing { get; set; }

        [JsonProperty("health_conditions_hazards")]
        [JsonPropertyName("health_conditions_hazards")]
        public List<DetailedHazard> HealthConditionsHazards { get; set; } = new List<DetailedHazard>();

        [JsonProperty("cognitive_impairment", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("cognitive_impairment")]
        public DetailedHazard CognitiveImpairment { get; set; }

        [JsonProperty("hearing_impairment", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("hearing_impairment")]
        public DetailedHazard HearingImpairment { get; set; }

        [JsonProperty("sight_impairment", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("sight_impairment")]
        public DetailedHazard SightImpairment { get; set; }

        [JsonProperty("speech_impediment", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("speech_impediment")]
        public DetailedHazard SpeechImpediment { get; set; }

        [JsonProperty("skin_breakdowns", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("skin_breakdowns")]
        public DetailedHazard SkinBreakdowns { get; set; }

        [JsonProperty("nurse_involved_in_pressure_areas", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("nurse_involved_in_pressure_areas")]
        public DetailedHazard NurseInvolvedInPressureAreas { get; set; }

        [JsonProperty("open_wound", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("open_wound")]
        public OpenWound OpenWound { get; set; }

        [JsonProperty("behaviour_challenges", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("behaviour_challenges")]
        public BehaviourChallenges BehaviourChallenges { get; set; }

        [JsonProperty("pain_management", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("pain_management")]
        public DetailedHazard PainManagement { get; set; }

        [JsonProperty("emollient_creams", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("emollient_creams")]
        public DetailedHazard EmollientCreams { get; set; }

        [JsonProperty("other_allergies", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("other_allergies")]
        public DetailedHazard OtherAllergies { get; set; }

        [JsonProperty("medication_allergies", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("medication_allergies")]
        public List<DetailedHazard> MedicationAllergies { get; set; } = new List<DetailedHazard>();
    }
}
