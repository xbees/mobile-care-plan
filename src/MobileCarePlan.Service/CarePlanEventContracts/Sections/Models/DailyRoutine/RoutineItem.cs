using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.DailyRoutine
{
    public class RoutineItem
    {
        [JsonProperty("period")]
        [JsonPropertyName("period")]
        public DayPeriod DayPeriod { get; set; }

        [ContentMap("self_sufficient", "What can they do for themselves ?", 1)]
        [JsonProperty("self_sufficient")]
        [JsonPropertyName("self_sufficient")]
        public string SelfSufficient { get; set; }

        [ContentMap("routine", "What is their routine ?", 2)]
        [JsonProperty("routine")]
        [JsonPropertyName("routine")]
        public string Routine { get; set; }

        [ContentMap("support_for_transfers", "Do they require support to enable them to mobilise or transfer safely ?", 3)]
        [JsonProperty("support_for_transfers")]
        [JsonPropertyName("support_for_transfers")]
        public bool SupportForTransfers { get; set; }

        [ContentMap("risks", "Associated risks and mitigations", 5)]
        [JsonProperty("risks")]
        [JsonPropertyName("risks")]
        public string Risks { get; set; }

        [ContentMap("outcomes", "Outcomes", 6)]
        [JsonProperty("outcomes")]
        [JsonPropertyName("outcomes")]
        public string Outcomes { get; set; }

        [ContentMap("tasks", order: 4)]
        [JsonProperty("tasks")]
        [JsonPropertyName("tasks")]
        public List<RoutineTaskItem> Tasks { get; set; } = new List<RoutineTaskItem>();
    }
}
