using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.DailyRoutine
{
    public class RoutineTaskItem
    {
        [ContentMap("type", "Task to be completed", 1)]
        [JsonProperty("type")]
        [JsonPropertyName("type")]
        public DailyRoutineTaskType Type { get; set; }

        [ContentMap("equipment", "Equipment to be used", 2)]
        [JsonProperty("equipment")]
        [JsonPropertyName("equipment")]
        public string Equipment { get; set; }

        [ContentMap("number_of_carers", "Number of carers needed", 3)]
        [JsonProperty("number_of_carers")]
        [JsonPropertyName("number_of_carers")]
        public int NumberOfCarers { get; set; }

        [ContentMap("safe_system", "Agreed safe system to be followed to achieve outcome", 4)]
        [JsonProperty("safe_system")]
        [JsonPropertyName("safe_system")]
        public string SafeSystem { get; set; }
    }
}
