using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace Cera.Sdk.Dcp.CarePlan.Events.Sections.Models.CommunicationsAndAccessibility
{
    public class NominatedPerson
    {
        [ContentMap("name", "Name of person", 1)]
        [JsonProperty("name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [ContentMap("matters_regarding", "The nominated person will deal with matters regarding", 3)]
        [JsonProperty("matters_regarding")]
        [JsonPropertyName("matters_regarding")]
        public string MattersRegarding { get; set; }

        [ContentMap("name", "Relationship to the service user", 2)]
        [JsonProperty("relationship")]
        [JsonPropertyName("relationship")]
        public string Relationship { get; set; }

        [ContentMap("access_to_records", "The service user would like their nominated person to have access to their care records on their behalf using the Cera family circle app", 4)]
        [JsonProperty("access_to_records")]
        [JsonPropertyName("access_to_records")]
        public bool AccessToRecords { get; set; }
    }
}
