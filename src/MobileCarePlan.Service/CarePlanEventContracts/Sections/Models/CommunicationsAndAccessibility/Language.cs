using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Cera.Sdk.Dcp.CarePlan.Events.Sections.Models.CommunicationsAndAccessibility
{
    public class Language
    {
        [JsonProperty("name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        [JsonPropertyName("code")]
        public string Code { get; set; }
    }
}
