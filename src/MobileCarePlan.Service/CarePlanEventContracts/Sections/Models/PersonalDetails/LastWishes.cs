﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.PersonalDetails
{
    public class LastWishes
    {
        [JsonProperty("details")]
        [JsonPropertyName("details")]
        public string Details { get; set; }

        [JsonProperty("support_required")]
        [JsonPropertyName("support_required")]
        public bool SupportRequired { get; set; }

        [JsonProperty("support_details")]
        [JsonPropertyName("support_details")]
        public string SupportDetails { get; set; }
    }
}
