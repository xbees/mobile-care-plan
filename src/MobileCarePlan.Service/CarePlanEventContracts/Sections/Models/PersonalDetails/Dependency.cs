﻿using System.Text.Json.Serialization;
using Cera.Sdk.Dcp.CarePlan.Events.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.PersonalDetails
{
    public sealed class Dependency
    {
        [ContentMap("level", "Dependency Level", 1)]
        [JsonProperty("level")]
        [JsonPropertyName("level")]
        public DependencyLevel Level { get; set; }

        [ContentMap("reason", "Reason", 2)]
        [JsonProperty("reason")]
        [JsonPropertyName("reason")]
        public string Reason { get; set; }

        [JsonProperty("medication_level")]
        [JsonPropertyName("medication_level")]
        public int? MedicationLevel { get; set; }

        [ContentMap("medication_level", "Medication level", 3)]
        [JsonProperty("medication_level_label")]
        [JsonPropertyName("medication_level_label")]
        public MedicationLevel? MedicationLevelLabel { get; set; }
    }
}
