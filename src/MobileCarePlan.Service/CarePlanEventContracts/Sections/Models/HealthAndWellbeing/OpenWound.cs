using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.HealthAndWellbeing
{
    public class OpenWound
    {
        [ContentMap("details", "Details of the wounds", 1)]
        [JsonProperty("details", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("details")]
        public string Details { get; set; }

        [ContentMap("care", "Who is caring for the wounds?", 2)]
        [JsonProperty("care", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("care")]
        public string Care { get; set; }
    }
}
