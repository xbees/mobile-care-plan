using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.HealthAndWellbeing
{
    public class BehaviourChallenges : DetailedHazard
    {
        [JsonProperty("triggers")]
        [JsonPropertyName("triggers")]
        public string Triggers { get; set; }

        [JsonProperty("triggers_other")]
        [JsonPropertyName("triggers_other")]
        public string TriggersOther { get; set; }
    }
}
