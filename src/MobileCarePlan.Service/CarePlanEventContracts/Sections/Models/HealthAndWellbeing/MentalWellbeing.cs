using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.HealthAndWellbeing
{
    public class MentalWellbeing
    {
        [ContentMap("details", "Details of the condition/s", 1)]
        [JsonProperty("details")]
        [JsonPropertyName("details")]
        public string Details { get; set; }

        [ContentMap("hazards", order: 2)]
        [JsonProperty("hazards")]
        [JsonPropertyName("hazards")]
        public List<Hazard> Hazards { get; set; } = new List<Hazard>();
    }
}
