using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.Medication
{
    public class MedicationCovert : DetailedHazard
    {
        [ContentMap("best_interest_inc_gp", "Has a best interest decision been made including a GP?")]
        [JsonProperty("best_interest_inc_gp", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("best_interest_inc_gp")]
        public bool BestInterestIncGp { get; set; }
    }
}
