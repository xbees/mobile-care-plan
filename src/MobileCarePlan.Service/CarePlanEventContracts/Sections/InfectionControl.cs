using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class InfectionControl
    {
        [JsonProperty("covid_19")]
        [JsonPropertyName("covid_19")]
        public Hazard Covid19 { get; set; }

        [JsonProperty("seasonal_flu")]
        [JsonPropertyName("seasonal_flu")]
        public Hazard SeasonalFlu { get; set; }

        [JsonProperty("norovirus")]
        [JsonPropertyName("norovirus")]
        public Hazard Norovirus { get; set; }

        [JsonProperty("mrsa")]
        [JsonPropertyName("mrsa")]
        public Hazard Mrsa { get; set; }

        [JsonProperty("other")]
        [JsonPropertyName("other")]
        public DetailedHazard Other { get; set; }
    }
}
