﻿using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.PersonalDetails;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class PersonalDetails
    {
        [JsonProperty("full_name", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("full_name")]
        public string FullName { get; set; }

        [JsonProperty("address", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("address")]
        public string Address { get; set; }

        [JsonProperty("date_of_birth", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("date_of_birth")]
        public DateTimeOffset? DateOfBirth { get; set; }

        [JsonProperty("phone_number", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("phone_number")]
        public string PhoneNumber { get; set; }

        [JsonProperty("start_date")]
        [JsonPropertyName("start_date")]
        public DateTimeOffset StartDate { get; set; }

        [JsonProperty("national_identity_reference", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("national_identity_reference")]
        public string NationalIdentityReference { get; set; }

        [JsonProperty("effective_from")]
        [JsonPropertyName("effective_from")]
        public DateTimeOffset EffectiveFrom { get; set; }

        [JsonProperty("care_plan_created_by")]
        [JsonPropertyName("care_plan_created_by")]
        public string CarePlanCreatedBy { get; set; }

        [JsonProperty("authors")]
        [JsonPropertyName("authors")]
        public List<Author> Authors { get; set; } = new List<Author>();

        [JsonProperty("dependency", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("dependency")]
        public Dependency DependencyLevel { get; set; }

        [JsonProperty("dnacpr_location", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("dnacpr_location")]
        public string DnacprLocation { get; set; }

        [JsonProperty("last_wishes", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("last_wishes")]
        public LastWishes LastWishes { get; set; }

        [JsonProperty("advanced_statement", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("advanced_statement")]
        public string AdvancedStatement { get; set; }

        [JsonProperty("event_of_death", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("event_of_death")]
        public string EventOfDeath { get; set; }

        [JsonProperty("funeral_plan", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("funeral_plan")]
        public string FuneralPlan { get; set; }

        [JsonProperty("poa", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("poa")]
        public List<PoaSupport> PoaSupports { get; set; } = new List<PoaSupport>();

        [JsonProperty("next_of_kin", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("next_of_kin")]
        public Relationship NextOfKin { get; set; }

        [JsonProperty("additional_next_of_kins", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("additional_next_of_kins")]
        public List<Relationship> AdditionalNextOfKins { get; set; }

        [JsonProperty("gp", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("gp")]
        public Contact Gp { get; set; }

        [JsonProperty("pharmacy", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("pharmacy")]
        public Contact Pharmacy { get; set; }
    }
}
