using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class ActivitiesAndFinancialSupport
    {
        [JsonProperty("support_with_housekeeping", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("support_with_housekeeping")]
        public string SupportWithHousekeeping { get; set; }

        [JsonProperty("housekeeping", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("housekeeping")]
        public Hazard Housekeeping { get; set; }

        [JsonProperty("who_supports_su_with_housekeeping", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("who_supports_su_with_housekeeping")]
        public string WhoSupportsSuWithHousekeeping { get; set; }

        [JsonProperty("support_with_shopping", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("support_with_shopping")]
        public string SupportWithShopping { get; set; }

        [JsonProperty("shopping", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("shopping")]
        public Hazard Shopping { get; set; }

        [JsonProperty("who_supports_su_with_shopping", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("who_supports_su_with_shopping")]
        public string WhoSupportsSuWithShopping { get; set; }

        [JsonProperty("support_with_social", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("support_with_social")]
        public string SupportWithSocial { get; set; }

        [JsonProperty("social", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("social")]
        public Hazard Social { get; set; }

        [JsonProperty("who_supports_su_with_social", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("who_supports_su_with_social")]
        public string WhoSupportsSuWithSocial { get; set; }

        [JsonProperty("support_with_finances", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("support_with_finances")]
        public string SupportWithFinances { get; set; }

        [JsonProperty("finances_further_instructions", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("finances_further_instructions")]
        public string FinancesFurtherInstructions { get; set; }

        [JsonProperty("finances", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("finances")]
        public Hazard Finances { get; set; }

        [JsonProperty("who_supports_su_with_finances", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("who_supports_su_with_finances")]
        public string WhoSupportsSuWithFinances { get; set; }
    }
}
