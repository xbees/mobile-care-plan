using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class Continence
    {
        [JsonIgnore]
        [ContentMap("support_with_continence", "What support do they require with managing their continence needs?", 1)]
        public string MappedSupportWithContinence => SupportWithContinence;

        [JsonIgnore]
        [ContentMap("continence", order: 2)]
        public Hazard MappedContinenceHazard => ContinenceHazard;

        [JsonIgnore]
        [ContentMap("su_continence", order: 3)]
        [OverrideChildContentMap("enabled", "Are they continent?")]
        public Group<SuContinenceMap> MappedSuContinence => new(SuContinenceMap.FromSuContinence(SuContinence), SuContinence == null);

        [JsonIgnore]
        [ContentMap("catheter", order: 4)]
        [OverrideChildContentMap("enabled", "Do they have a catheter?")]
        public Group<DetailedHazard> MappedCatheter => new(Catheter);

        [JsonIgnore]
        [ContentMap("stoma", order: 5)]
        [OverrideChildContentMap("enabled", "Do they have a stoma?")]
        public Group<Stoma> MappedStoma => new(Stoma);
    }

    public class SuContinenceMap
    {
        [ContentMap("details", "Details", 1)]
        public string Details { get; set; }

        [ContentMap("has_continence_aids", "Do they use continence aids?", 2)]
        public bool ContinenceAidsExists { get; set; }

        [ContentMap("su_continent_hazard", order:3)]
        public Hazard Hazard { get; set; }

        public static SuContinenceMap FromSuContinence(SuContinence suContinence)
        {
            if(suContinence == null)
                return null;

            return new SuContinenceMap
            {
                Details = suContinence.SuContinent?.Details,
                ContinenceAidsExists = suContinence.ContinenceAidsExists,
                Hazard = suContinence.SuContinent == null ? null : new Hazard
                {
                    HazardInfo = suContinence.SuContinent.HazardInfo,
                    InitialRisk = suContinence.SuContinent.InitialRisk,
                    ControlMeasures = suContinence.SuContinent.ControlMeasures,
                    ResidualRisk = suContinence.SuContinent.ResidualRisk
                }
            };
        }
    }
}
