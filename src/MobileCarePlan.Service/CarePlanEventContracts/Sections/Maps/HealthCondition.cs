using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class HealthCondition
    {
        [JsonIgnore]
        [ContentMap("name", "Condition name", 1)]
        public string MappedName => Name;

        [JsonIgnore]
        [ContentMap("diagnosis_date", "Diagnosis date (if known)", 2)]
        public string MappedDiagnosisDate => (DiagnosisMonth > 0 && DiagnosisYear > 0) ? new DateTime(DiagnosisYear, DiagnosisMonth, 1).ToString("MM/yyyy") : null;

        [JsonIgnore]
        [ContentMap("details", "How does this affect them?", 2)]
        public string MappedDetails => Details;

        [JsonIgnore]
        [ContentMap("treatment", "Ongoing management/treatment (if applicable)", 2)]
        public string MappedTreatment => Treatment;
    }
}
