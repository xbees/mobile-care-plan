using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class OverallSignificantRisks
    {
        [JsonIgnore]
        [ContentMap("details", "Details", 1)]
        public string MappedSignificantRisksDetails => SignificantRisksDetails;
    }
}
