using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class LiveInArrangements
    {
        [JsonIgnore]
        [ContentMap("breaks_and_food", "Live in breaks and food for carer", 1)]
        public string MappedBreaksAndFood => BreaksAndFood;

        [JsonIgnore]
        [ContentMap("sleeping_and_environmental", "Sleeping and environmental arrangements for carer", 2)]
        public string MappedSleepingAndEnvironmental => SleepingAndEnvironmental;
    }
}
