using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class InfectionControl
    {
        [JsonIgnore]
        [ContentMap("covid_19", "COVID-19", 1)]
        public Hazard MappedCovid19 => Covid19;

        [JsonIgnore]
        [ContentMap("seasonal_flu", "Seasonal Flu", 2)]
        public Hazard MappedSeasonalFlu => SeasonalFlu;

        [JsonIgnore]
        [ContentMap("norovirus", "Norovirus", 3)]
        public Hazard MappedNorovirus => Norovirus;

        [JsonIgnore]
        [ContentMap("mrsa", "MRSA", 4)]
        public Hazard MappedMrsa => Mrsa;

        [ContentMap("other", order: 5)]
        [OverrideChildContentMap("enabled", "Are they at risk of contracting any other infections?")]
        [OverrideChildContentMap("details", "Details of what they're at risk of contracting")]
        public Group<DetailedHazard> MappedOther => new(Other);
    }
}
