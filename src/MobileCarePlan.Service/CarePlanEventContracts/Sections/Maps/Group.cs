﻿using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps
{
    public class Group<T> where T : class
    {
        public Group(T @object, bool? enabled = null)
        {
            Object = @object;
            if (!enabled.HasValue)
                Enabled = @object != null;
            else
                Enabled = enabled.Value;
        }

        [ContentMap("enabled", "Is object enable ?", order: 1)]
        public bool Enabled { get; private set; }

        [ContentMap("object", order: 2)]
        public T Object { get; private set; }
    }
}
