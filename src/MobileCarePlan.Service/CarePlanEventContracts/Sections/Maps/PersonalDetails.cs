﻿using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.PersonalDetails;
using MobileCarePlan.Service.Extensions;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class PersonalDetails
    {
        [JsonIgnore]
        [ContentMap("full_name", "Full name", 1)]
        public string MappedFullName => FullName;

        [JsonIgnore]
        [ContentMap("address", "Address (inc postcode)", 2)]
        public string MappedAddress => Address;

        [JsonIgnore]
        [ContentMap("date_of_birth", "Date of birth", 3, "date")]
        public string MappedDateOfBirth => DateOfBirth?.ToString("dd/MM/yyyy");

        [JsonIgnore]
        [ContentMap("phone_number", "Phone number", 4)]
        public string MappedPhoneNumber => PhoneNumber;

        [JsonIgnore]
        [ContentMap("start_date", "Care start date", 5, "date")]
        public string MappedStartDate => StartDate.ToString("dd/MM/yyyy");

        [JsonIgnore]
        [ContentMap("national_identity_reference", "Social services ref (Optional)", 6)]
        public string MappedNationalIdentityReference => NationalIdentityReference;

        [JsonIgnore]
        [ContentMap("effective_from", "Care plan effective from", 7, "date")]
        public string MappedEffectiveFrom => EffectiveFrom.ToString("dd/MM/yyyy");

        [JsonIgnore]
        [ContentMap("care_plan_created_by", "Care plan created by", 8)]
        public string CarePlanCreatedByContent => CarePlanCreatedBy;

        [JsonIgnore]
        [ContentMap("authors", order: 9)]
        [OverrideChildContentMap("enabled", "Was there anyone else involved in creating the care plan in addition to the service user?")]
        public Group<List<Author>> MappedAuthors => new(Authors, Authors.HasValue());

        [JsonIgnore]
        [ContentMap("dependency", order: 10)]
        public Dependency MappedDependencyLevel => DependencyLevel;

        [JsonIgnore]
        [ContentMap("dnacpr_location", order: 11)]
        [OverrideChildContentMap("enabled", "Do they have a DNACPR in place?")]
        [OverrideChildContentMap("object", "Location of DNACPR")]
        public Group<string> MappedDnacprLocation => new(DnacprLocation, DnacprLocation.HasValue());

        [JsonIgnore]
        [ContentMap("advanced_statement", order: 12)]
        [OverrideChildContentMap("enabled", "Do they have an advanced decision or statement in place?")]
        [OverrideChildContentMap("object", "Details")]
        public Group<string> MappedAdvancedStatement => new(AdvancedStatement, AdvancedStatement.HasValue());

        [JsonIgnore]
        [ContentMap("last_wishes.details", order: 13)]
        [OverrideChildContentMap("enabled", "Do they wish to discuss any last wishes or preferences they have for when they reach the end of life?")]
        [OverrideChildContentMap("object", "Details")]
        public Group<string> MappedLastWishesDetails => new(LastWishes?.Details, (LastWishes?.Details).HasValue());

        [JsonIgnore]
        [ContentMap("event_of_death", order: 14)]
        [OverrideChildContentMap("enabled", "Is there anyone they would like us to contact for them in the event of their death?")]
        [OverrideChildContentMap("object", "Details")]
        public Group<string> MappedEventOfDeath => new(EventOfDeath, EventOfDeath.HasValue());

        [ContentMap("funeral_plan", order: 15)]
        [OverrideChildContentMap("enabled", "Do they have a funeral plan in place?")]
        [OverrideChildContentMap("object", "Details")]
        public Group<string> MappedFuneralPlan => new(FuneralPlan, FuneralPlan.HasValue());

        [JsonIgnore]
        [ContentMap("last_wishes.support_details", order: 16)]
        [OverrideChildContentMap("enabled", "Is there anything Cera can do to support them with their last wishes?")]
        [OverrideChildContentMap("object", "Details")]
        public Group<string> MappedLastWishesSupportDetails => new(LastWishes?.SupportDetails, (LastWishes?.SupportDetails).HasValue());

        [JsonIgnore]
        [ContentMap("poa_supports", order: 17)]
        [OverrideChildContentMap("enabled", "Do they have a POA in place?")]
        [OverrideChildContentMap("object", "POA support")]
        public Group<string> MappedPoaSupports => new(string.Join(", ", PoaSupports.Select(p => p.GetDescription())), PoaSupports.HasValue());

        [JsonIgnore]
        [ContentMap("next_of_kin", order: 18)]
        [OverrideChildContentMap("enabled", "Do they have a next of kin?")]
        [OverrideChildContentMap("name", order: 1)]
        [OverrideChildContentMap("relation", order: 2)]
        [OverrideChildContentMap("address", "Address inc postcode", 3)]
        [OverrideChildContentMap("phone_number", order: 4)]
        public Group<Relationship> MappedNextOfKin => new(NextOfKin);

        [JsonIgnore]
        [ContentMap("additional_next_of_kins", order: 19)]
        [OverrideChildContentMap("enabled", "Do they have any additional next of kin?")]
        [OverrideChildContentMap("name", order: 1)]
        [OverrideChildContentMap("relation", order: 2)]
        [OverrideChildContentMap("address", "Address inc postcode", 3)]
        [OverrideChildContentMap("phone_number", order: 4)]
        public Group<List<Relationship>> MappedAdditionalNextOfKins => MappedNextOfKin.Enabled ? new (AdditionalNextOfKins, AdditionalNextOfKins.HasValue()) : null;

        [JsonIgnore]
        [ContentMap("gp", order: 20)]
        [OverrideChildContentMap("name", "Gp name", 1)]
        [OverrideChildContentMap("phone_number", "Gp phone number", 2)]
        [OverrideChildContentMap("address", "Gp address (if known)", 3)]
        public Contact MappedGp => Gp;

        [JsonIgnore]
        [ContentMap("pharmacy", order: 21)]
        [OverrideChildContentMap("name", "Pharmacy name", 1)]
        [OverrideChildContentMap("phone_number", "Pharmacy phone number", 2)]
        [OverrideChildContentMap("address", "Pharmacy address (if known)", 3)]
        public Contact MappedPharmacy => Pharmacy;
    }
}
