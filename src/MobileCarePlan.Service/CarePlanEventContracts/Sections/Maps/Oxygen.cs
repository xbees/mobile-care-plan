using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class Oxygen
    {
        [JsonIgnore]
        [ContentMap("oxygen_support", order: 1)]
        [OverrideChildContentMap("details", "Please outline the support needed with oxygen therapy")]
        public DetailedHazard MappedOxygenSupport => OxygenSupport;

        [JsonIgnore]
        [ContentMap("oxygen_environmental", "Please fill in the environmental hazards associated to the oxygen use", 2)]
        [OverrideChildContentMap("hazards", "Environmental hazard")]
        [OverrideChildContentMap("initial_risk", "Environmental initial risk level")]
        [OverrideChildContentMap("control_measures", "Environmental control measures")]
        [OverrideChildContentMap("residual_risk", "Environmental residual risk level")]
        public Hazard MappedOxygenEnvironmental => OxygenEnvironmental;
    }
}
