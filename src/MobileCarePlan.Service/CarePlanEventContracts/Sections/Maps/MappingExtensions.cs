﻿namespace MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps
{
    internal static class MappingExtensions
    {
        public static bool HasValue<T>(this List<T> objectValue)
        {
            return objectValue != null && objectValue.Any(t => t != null);
        }

        public static bool HasValue(this string objectValue)
        {
            return !string.IsNullOrWhiteSpace(objectValue);
        }
    }
}
