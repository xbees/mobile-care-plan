using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class DecisionsAroundCare
    {
        [JsonIgnore]
        [ContentMap("can_make_formal_decisions", "Is the service user able to make decisions on all aspects of their life including day to day decisions, finance and welfare?", 1)]
        public bool MappedCanMakeFormalDecisions => CanMakeFormalDecisions;

        [JsonIgnore]
        [ContentMap("formal_decision_details", "Ability to make decisions", 2)]
        public string MappedFormalDecisionsDetails => FormalDecisionsDetails;

        [JsonIgnore]
        [ContentMap("can_make_informal_decisions", "Is the service user usually able to make day-to-day informal decisions about their care and support?", 3)]
        public bool MappedCanMakeInformalDecisions => CanMakeInformalDecisions;

        [JsonIgnore]
        [ContentMap("informal_decision_details", "Ability to make informal decisions", 4)]
        public string MappedInformalDecisionsDetails => InformalDecisionsDetails;

        [JsonIgnore]
        [ContentMap("has_formal_decision_assistance", "Does the service user need support to make more formal, complex decisions such as consent to their care plan?", 5)]
        public bool HasFormalDecisionsAssistance => FormalDecisionsAssistance?.SupportRequired ?? false;

        [JsonIgnore]
        [ContentMap("formal_decision_assistance", "Support to make formal decisions", order: 6)]
        public string MappedFormalDecisionsAssistance => FormalDecisionsAssistance?.Details;

        [JsonIgnore]
        [ContentMap("mental_capacity_assessment_required", "Is a mental capacity assessment required?", 7)]
        public YesNoOption MappedMentalCapacityAssessmentRequired => MentalCapacityAssessmentRequired;
    }
}
