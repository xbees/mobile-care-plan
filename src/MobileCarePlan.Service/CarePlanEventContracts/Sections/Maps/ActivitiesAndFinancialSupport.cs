using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class ActivitiesAndFinancialSupport
    {
        [JsonIgnore]
        [ContentMap("support_with_housekeeping", "What support do they require with housekeeping?", 1)]
        public string MappedSupportWithHousekeeping => SupportWithHousekeeping;

        [JsonIgnore]
        [ContentMap("housekeeping", order: 2)]
        public Hazard MappedHousekeeping => Housekeeping;

        [JsonIgnore]
        [ContentMap("who_supports_su_with_housekeeping", "Please outline who supports the service user with housekeeping?", 3)]
        public string MappedWhoSupportsSuWithHousekeeping => WhoSupportsSuWithHousekeeping;

        [JsonIgnore]
        [ContentMap("support_with_shopping", "What support do they require with shopping?", 4)]
        public string MappedSupportWithShopping => SupportWithShopping;

        [JsonIgnore]
        [ContentMap("shopping", order: 5)]
        public Hazard MappedShopping => Shopping;

        [JsonIgnore]
        [ContentMap("who_supports_su_with_shopping", "Please outline who supports the service user with shopping?", 6)]
        public string MappedWhoSupportsSuWithShopping => WhoSupportsSuWithShopping;

        [JsonIgnore]
        [ContentMap("support_with_social", "What support do they require with social activities? I.e. going out, socialising, holidays", 7)]
        public string MappedSupportWithSocial => SupportWithSocial;

        [JsonIgnore]
        [ContentMap("social", order: 8)]
        public Hazard MappedSocial => Social;

        [JsonIgnore]
        [ContentMap("who_supports_su_with_social", "Please outline who supports the service user with social activities?", 9)]
        public string MappedWhoSupportsSuWithSocial => WhoSupportsSuWithSocial;

        [JsonIgnore]
        [ContentMap("support_with_finances", "What financial transaction are we to handle on their behalf?", 10)]
        public string MappedSupportWithFinances => SupportWithFinances;

        [JsonIgnore]
        [ContentMap("finances_further_instructions", "Further instructions (Optional)", 11)]
        public string MappedFinancesFurtherInstructions => FinancesFurtherInstructions;

        [JsonIgnore]
        [ContentMap("finances", order: 12)]
        public Hazard MappedFinances => Finances;

        [JsonIgnore]
        [ContentMap("who_supports_su_with_finances", "Who will handle their financial transactions?", 13)]
        public string MappedWhoSupportsSuWithFinances => WhoSupportsSuWithFinances;
    }
}
