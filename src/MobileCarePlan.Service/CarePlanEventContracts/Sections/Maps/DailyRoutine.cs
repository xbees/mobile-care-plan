using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.DailyRoutine;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class DailyRoutine
    {
        [JsonIgnore]
        [ContentMap("morning_care", order: 1)]
        [OverrideChildContentMap("enabled", "Do they require care in the morning ?")]
        [OverrideChildContentMap("self_sufficient", "What can they do for themselves in the morning ?")]
        [OverrideChildContentMap("routine", "What is their routine in the morning ?")]
        [OverrideChildContentMap("support_for_transfers", "Do they require support to enable them to mobilise or transfer safely in the morning ?")]
        public Group<RoutineItem> MorningCare => new(Routines.FirstOrDefault(r => r.DayPeriod == DayPeriod.Morning));

        [JsonIgnore]
        [ContentMap("lunch_care", order: 2)]
        [OverrideChildContentMap("enabled", "Do they require care at lunch ?")]
        [OverrideChildContentMap("self_sufficient", "What can they do for themselves  at lunch ?")]
        [OverrideChildContentMap("routine", "What is their routine at lunch ?")]
        [OverrideChildContentMap("support_for_transfers", "Do they require support to enable them to mobilise or transfer safely at lunch ?")]
        public Group<RoutineItem> LunchCare => new(Routines.FirstOrDefault(r => r.DayPeriod == DayPeriod.Lunch));

        [JsonIgnore]
        [ContentMap("teatime_care", order: 3)]
        [OverrideChildContentMap("enabled", "Do they require care at teatime ?")]
        [OverrideChildContentMap("self_sufficient", "What can they do for themselves  at teatime ?")]
        [OverrideChildContentMap("routine", "What is their routine at teatime ?")]
        [OverrideChildContentMap("support_for_transfers", "Do they require support to enable them to mobilise or transfer safely at teatime ?")]
        public Group<RoutineItem> TeatimeCare => new(Routines.FirstOrDefault(r => r.DayPeriod == DayPeriod.TeaTime));

        [JsonIgnore]
        [ContentMap("evening_care", order: 4)]
        [OverrideChildContentMap("enabled", "Do they require care in the evening ?")]
        [OverrideChildContentMap("self_sufficient", "What can they do for themselves in the evening ?")]
        [OverrideChildContentMap("routine", "What is their routine in the evening ?")]
        [OverrideChildContentMap("support_for_transfers", "Do they require support to enable them to mobilise or transfer safely in the evening ?")]
        public Group<RoutineItem> EveningCare => new(Routines.FirstOrDefault(r => r.DayPeriod == DayPeriod.Evening));

        [JsonIgnore]
        [ContentMap("overnight_care", order: 5)]
        [OverrideChildContentMap("enabled", "Do they require care overnight ?")]
        [OverrideChildContentMap("self_sufficient", "What can they do for themselves overnight ?")]
        [OverrideChildContentMap("routine", "What is their routine overnight ?")]
        [OverrideChildContentMap("support_for_transfers", "Do they require support to enable them to mobilise or transfer safely overnight ?")]
        public Group<RoutineItem> OvernightCare => new(Routines.FirstOrDefault(r => r.DayPeriod == DayPeriod.Overnight));

        [JsonIgnore]
        [ContentMap("oral_health", order: 6)]
        [OverrideChildContentMap("oral_health.support_required", "Do they need support with oral health ?")]
        [OverrideChildContentMap("oral_health.details", "Support needed")]
        public Assistance MappedOralHealth => new Assistance
        {
            SupportRequired = OralHealth?.SupportRequired ?? false,
            Details = OralHealth?.Details
        };

        [JsonIgnore]
        [ContentMap("dietary_requirements", order: 7)]
        [OverrideChildContentMap("enabled", "Do they have any dietary requirements ?")]
        [OverrideChildContentMap("object", "What are their requirements ?")]
        public Group<string> MappedDietaryRequirements => new(DietaryRequirements, DietaryRequirements.HasValue());

        [JsonIgnore]
        [ContentMap("has_food_allegies", "Do they have any food allergies ?", 8)]
        public bool HasFoodAllegies => FoodAllegies.HasValue();

        [JsonIgnore]
        [ContentMap("food_allegies", "What foods are they allergic to ?", 9)]
        public string MappedFoodAllegies => HasFoodAllegies ? string.Join(", ", FoodAllegies) : null;
    }
}
