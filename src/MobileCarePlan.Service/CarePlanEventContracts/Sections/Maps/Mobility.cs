using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class Mobility
    {
        [JsonIgnore]
        [ContentMap("height_in_cm", "Service users approx height (cm)", 1)]
        public string MappedHeightInCm => HeightInCm.HasValue ? ((int)HeightInCm.Value).ToString() : null;

        [JsonIgnore]
        [ContentMap("weight_in_kgs", "Service users approx weight (kgs)", 2)]
        public string MappedWeightInKgs => WeightInKgs.HasValue ? ((int)WeightInKgs.Value).ToString() : null;

        [JsonIgnore]
        [ContentMap("ability_to_bear_weight", order: 3)]
        [OverrideChildContentMap("enabled", "Are they mobile and able to bear weight?")]
        [OverrideChildContentMap("details", "Details of why they can't bear weight")]
        public Group<DetailedHazard> MappedAbilityToBearWeight => new(AbilityToBearWeight, AbilityToBearWeight == null);

        [JsonIgnore]
        [ContentMap("walking_aids", order: 4)]
        [OverrideChildContentMap("enabled", "Do they use any walking aids?")]
        public Group<DetailedHazard> MappedWalkingAids => new(WalkingAids);

        [JsonIgnore]
        [ContentMap("independence_with_dressing", order: 5)]
        [OverrideChildContentMap("enabled", "Are they independent with dressing and undressing?")]
        [OverrideChildContentMap("details", "What do they need support with?")]
        public Group<DetailedHazard> MappedIndependenceWithDressing => new(IndependenceWithDressing, IndependenceWithDressing == null);

        [JsonIgnore]
        [ContentMap("history_of_falls", order: 6)]
        [OverrideChildContentMap("enabled", "Do they have a history of falls?")]
        [OverrideChildContentMap("details", "Details of previous falls")]
        public Group<DetailedHazard> MappedHistoryOfFalls => new(HistoryOfFalls);

        [JsonIgnore]
        [ContentMap("risk_of_falls", order: 7)]
        [OverrideChildContentMap("enabled", "Are they at risk of falls?")]
        public Group<DetailedHazard> MappedRiskOfFalls => new(RiskOfFalls);

        [JsonIgnore]
        [ContentMap("lifeline_pendant", order: 8)]
        [OverrideChildContentMap("enabled", "Do they have a lifeline pendant?")]
        public Group<DetailedHazard> MappedLifelinePendant => new(LifelinePendant);

        [JsonIgnore]
        [ContentMap("ot_risk_assessment", order: 9)]
        [OverrideChildContentMap("enabled", "Is there an OT moving and handling risk assessment in place?")]
        [OverrideChildContentMap("object", "Details")]
        public Group<string> MappedOtRiskAssessment => new(OtRiskAssessment);

        [JsonIgnore]
        [ContentMap("in_and_out_of_bed", order: 10)]
        [OverrideChildContentMap("enabled", "Can they get in and out and move independently in bed?")]
        public Group<DetailedHazard> MappedInAndOutOfBed => new(InAndOutOfBed, InAndOutOfBed == null);

        [JsonIgnore]
        [ContentMap("transfer_to_toilet", order: 11)]
        [OverrideChildContentMap("enabled", "Are they independent with transferring to the toilet?")]
        [OverrideChildContentMap("details", "Details of support needed")]
        public Group<DetailedHazard> MappedTransferToToilet => new(TransferToToilet, TransferToToilet == null);

        [JsonIgnore]
        [ContentMap("assistance_on_toilet", order: 12)]
        [OverrideChildContentMap("enabled", "Do they require assistance when on the toilet?")]
        public Group<DetailedHazard> MappedAssistanceOnToilet => new(AssistanceOnToilet);

        [JsonIgnore]
        [ContentMap("transfer_to_and_from_chair", order: 13)]
        [OverrideChildContentMap("enabled", "Are they independent with transferring to and from a chair?")]
        [OverrideChildContentMap("details", "Details of support needed")]
        public Group<DetailedHazard> MappedTransferToAndFromChair => new(TransferToAndFromChair, TransferToAndFromChair == null);

        [JsonIgnore]
        [ContentMap("shower_or_bath", order: 14)]
        [OverrideChildContentMap("details", "Details of what they use")]
        public ShowerOrbath MappedShowerOrBath => ShowerOrBath;

        [JsonIgnore]
        [ContentMap("hoist_or_lift", order: 15)]
        [OverrideChildContentMap("enabled", "Do they have a hoist/lift?")]
        [OverrideChildContentMap("date_of_last_inspection", "Date of last inspection (month and year)")]
        [OverrideChildContentMap("date_of_next_inspection", "Date of next inspection (month and year)")]
        public Group<Equipment> MappedHoistOrLift => new(HoistOrLift);

        [JsonIgnore]
        [ContentMap("standing_aids", order: 16)]
        [OverrideChildContentMap("enabled", "Do they have any standing aids?")]
        public Group<Equipment> MappedStandingAids => new(StandingAids);

        [JsonIgnore]
        [ContentMap("sliding_equipment", order: 17)]
        [OverrideChildContentMap("enabled", "Do they have any sliding equipment (slide sheets etc)?")]
        public Group<Equipment> MappedSlidingEquipment => new(SlidingEquipment);

        [JsonIgnore]
        [ContentMap("wheel_chair", order: 18)]
        [OverrideChildContentMap("enabled", "Do they have a wheelchair?")]
        public Group<Equipment> MappedWheelChair => new(WheelChair);

        [JsonIgnore]
        [ContentMap("other_equipment", "Other equipments", order: 19)]
        [OverrideChildContentMap("date_of_last_inspection", "Date of last inspection (month and year)")]
        [OverrideChildContentMap("date_of_next_inspection", "Date of next inspection (month and year)")]
        public IEnumerable<OtherEquipment> MappedOtherEquipments => OtherEquipment;
    }
}
