using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class KeyDetails
    {
        [JsonIgnore]
        [ContentMap("alias", "Name service user wants to be known by (Optional)", 1)]
        public string MappedAlias => Alias;

        [JsonIgnore]
        [ContentMap("email", "Email address (Optional)", 2)]
        public string MappedEmail => Email;

        [JsonIgnore]
        [ContentMap("house_mate", order: 3)]
        [OverrideChildContentMap("enabled", "Do they live with anyone?")]
        [OverrideChildContentMap("object", "Their name")]
        public Group<string> MappedHouseMate => new(HouseMate, HouseMate.HasValue());

        [JsonIgnore]
        [ContentMap("ablle_to_open_door", "Can the service user open the door?", 4)]
        public bool MappedAbleToOpenDoor => AbleToOpenDoor;

        [JsonIgnore]
        [ContentMap("key_safe_number", order: 5)]
        [OverrideChildContentMap("enabled", "Does the service user have a key safe?")]
        [OverrideChildContentMap("object", "Key safe number")]
        public Group<string> MappedKeySafeNumber => new(KeySafeNumber, KeySafeNumber.HasValue());

        [JsonIgnore]
        [ContentMap("ethnicity", "Ethnicity", 6)]
        public string MappedEthnicity => EthnicityOther.HasValue() ? EthnicityOther : Ethnicity;

        [JsonIgnore]
        [ContentMap("religion", "Religious or cultural beliefs", 7)]
        public string MappedReligion => ReligionOther.HasValue() ? ReligionOther : Religion;

        [JsonIgnore]
        [ContentMap("has_relationship_status", "Relationship status", 8)]
        public string MappedRelationshipStatus => RelationshipStatus;

        [JsonIgnore]
        [ContentMap("sexual_orientation", "Sexual orientation", 9)]
        public string MappedSexualOrientation => SexualOrientation;

        [JsonIgnore]
        [ContentMap("gender", "Gender", 10)]
        public string MappedGender => GenderOther.HasValue() ? GenderOther : Gender;

        [JsonIgnore]
        [ContentMap("contacts", "Useful contacts", 11)]
        public List<Relationship> MappedContacts => Contacts;
    }
}
