using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.HealthAndWellbeing;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class HealthAndWellbeing
    {
        [JsonIgnore]
        [ContentMap("mental_wellbeing_diagnostic", "Have they been diagnosed with any mental health / wellbeing conditions?", 1)]
        public YesNoOption MappedMentalWellbeingDiagnostic => MentalWellbeingDiagnostic;

        [JsonIgnore]
        [ContentMap("mental_wellbeing", order: 2)]
        public MentalWellbeing MappedMentalWellbeing => MentalWellbeing;

        [JsonIgnore]
        [ContentMap("cognitive_impairment", order: 3)]
        [OverrideChildContentMap("enabled", "Do they have any cognitive impairments?")]
        [OverrideChildContentMap("details", "Details of impairment")]
        public Group<DetailedHazard> MappedCognitiveImpairment => new(CognitiveImpairment);

        [JsonIgnore]
        [ContentMap("hearing_impairment", order: 4)]
        [OverrideChildContentMap("enabled", "Do they have any hearing impairments?")]
        [OverrideChildContentMap("details", "Details of impairment")]
        public Group<DetailedHazard> MappedHearingImpairment => new(HearingImpairment);

        [JsonIgnore]
        [ContentMap("sight_impairment", order: 5)]
        [OverrideChildContentMap("enabled", "Do they have any sight impairments?")]
        [OverrideChildContentMap("details", "Details of impairment")]
        public Group<DetailedHazard> MappedSightImpairment => new(SightImpairment);

        [JsonIgnore]
        [ContentMap("speech_impediment", order: 6)]
        [OverrideChildContentMap("enabled", "Do they have a speech impediment?")]
        [OverrideChildContentMap("details", "Details of impediment")]
        public Group<DetailedHazard> MappedSpeechImpediment => new(SpeechImpediment);

        [JsonIgnore]
        [ContentMap("health_conditions_hazards", order: 7)]
        [OverrideChildContentMap("details", "Health condition")]
        public List<DetailedHazard> MappedHealthConditionsHazards => HealthConditionsHazards;

        [JsonIgnore]
        [ContentMap("skin_breakdowns", order: 8)]
        [OverrideChildContentMap("enabled", "Are they at risk of skin breakdown or pressure damage?")]
        [OverrideChildContentMap("details", "Details including affected areas")]
        public Group<DetailedHazard> MappedSkinBreakdowns => new(SkinBreakdowns);

        [JsonIgnore]
        [ContentMap("nurse_involved_in_pressure_areas", order: 9)]
        [OverrideChildContentMap("enabled", "Is a community nurse and/or tissue viability or district nurse involved in any aspect of care for their pressure areas?")]
        public Group<DetailedHazard> MappedNurseInvolvedInPressureAreas => new(NurseInvolvedInPressureAreas);

        [JsonIgnore]
        [ContentMap("open_wound", order: 10)]
        [OverrideChildContentMap("enabled", "Do they have any wounds?")]
        public Group<OpenWound> MappedOpenWound => new(OpenWound);

        [JsonIgnore]
        [ContentMap("behaviour_challenges", order: 11)]
        [OverrideChildContentMap("enabled", "Do they feel they experience behaviour that challenges?")]
        [OverrideChildContentMap("details", "Details of behaviour")]
        public Group<HealthAndWellbeingBehaviourChallenges> MappedBehaviourChallenges
            => new(HealthAndWellbeingBehaviourChallenges.FromBehaviourChallenges(BehaviourChallenges));

        [JsonIgnore]
        [ContentMap("pain_management", order: 12)]
        [OverrideChildContentMap("enabled", "Do they have any pain management requirements?")]
        [OverrideChildContentMap("details", "What are the requirements?")]
        public Group<DetailedHazard> MappedPainManagement => new(PainManagement);

        [JsonIgnore]
        [ContentMap("emollient_creams", order: 13)]
        [OverrideChildContentMap("enabled", "Do they use any emollient creams?")]
        [OverrideChildContentMap("details", "Details of the cream/where this needs to be applied/how often")]
        public Group<DetailedHazard> MappedEmollientCreams => new(EmollientCreams);

        [JsonIgnore]
        [ContentMap("other_allergies", order: 14)]
        [OverrideChildContentMap("enabled", "Do they have any other allergies (excluding food and medication)?")]
        [OverrideChildContentMap("details", "Details of the allergy")]
        public Group<DetailedHazard> MappedOtherAllergies => new(OtherAllergies);

        [JsonIgnore]
        [ContentMap("medication_allergies", order: 15)]
        [OverrideChildContentMap("enabled", "Do they have any medication allergies?")]
        [OverrideChildContentMap("details", "What is the allergy?")]
        public Group<List<DetailedHazard>> MappedMedicationAllergies => new(MedicationAllergies, MedicationAllergies.HasValue());
    }

    public class HealthAndWellbeingBehaviourChallenges : BehaviourChallenges
    {
        [ContentMap("triggers", "Potential triggers", order: 2)]
        public string MappedTriggers { get; set; }

        public static HealthAndWellbeingBehaviourChallenges FromBehaviourChallenges(BehaviourChallenges behaviourChallenges)
        {
            if (behaviourChallenges == null)
                return null;

            return new HealthAndWellbeingBehaviourChallenges
            {
                MappedTriggers = behaviourChallenges.TriggersOther.HasValue() ? behaviourChallenges.TriggersOther : behaviourChallenges.Triggers,
                Details = behaviourChallenges.Details,
                HazardInfo = behaviourChallenges.HazardInfo,
                InitialRisk = behaviourChallenges.InitialRisk,
                ControlMeasures = behaviourChallenges.ControlMeasures,
                ResidualRisk = behaviourChallenges.ResidualRisk
            };
        }
    }
}
