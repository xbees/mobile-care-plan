using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class NutritionAndHydration
    {
        [JsonIgnore]
        [ContentMap("nutrition_and_hydration_support", order: 1)]
        [OverrideChildContentMap("details", "What support do they require with nutrition and hydration?")]
        public DetailedHazard MappedNutritionAndHydrationSupport => NutritionAndHydrationSupport;

        [JsonIgnore]
        [ContentMap("risk_of_dehydration", order: 2)]
        [OverrideChildContentMap("enabled", "Are they at risk of dehydration?")]
        public Group<DetailedOptionalHazard> MappedRiskOfDehydration => new(RiskOfDehydration, RiskOfDehydration != null && RiskOfDehydration.Hazard != null);

        [JsonIgnore]
        [ContentMap("risk_of_malnutrition", order: 3)]
        [OverrideChildContentMap("enabled", "Are they at risk of malnutrition?")]
        public Group<DetailedOptionalHazard> MappedRiskOfMalnutrition => new(RiskOfMalnutrition, RiskOfMalnutrition != null && RiskOfMalnutrition.Hazard != null);

        [JsonIgnore]
        [ContentMap("food_allergies", order: 4)]
        public IEnumerable<FoodAllergyHazard> MappedFoodAllergies => FoodAllergies;

        [JsonIgnore]
        [ContentMap("dentures", order: 5)]
        [OverrideChildContentMap("enabled", "Do they wear dentures?")]
        public Group<DetailedHazard> MappedDentures => new(Dentures);

        [JsonIgnore]
        [ContentMap("difficulty_swallowing", order: 6)]
        [OverrideChildContentMap("enabled", "Do they have difficulty with swallowing?")]
        public Group<DetailedHazard> MappedDifficultySwallowing => new(DifficultySwallowing);

        [JsonIgnore]
        [ContentMap("risk_of_choking", order: 7)]
        [OverrideChildContentMap("enabled", "Are they at risk of choking when eating and drinking?")]
        public Group<DetailedHazard> MappedRiskOfChoking => new(RiskOfChoking);

        [JsonIgnore]
        [ContentMap("speech_and_language_therapy", order: 8)]
        [OverrideChildContentMap("enabled", "Is a speech and language therapy (SALT) assessment in place?")]
        public Group<DetailedHazard> MappedSpeechAndLanguageTherapy => new(SpeechAndLanguageTherapy);

        [JsonIgnore]
        [ContentMap("support_with_eating", order: 9)]
        [OverrideChildContentMap("enabled", "Do they require any aids or support with eating?")]
        public Group<DetailedOptionalHazard> MappedSupportWithEating => new(SupportWithEating, SupportWithEating != null && SupportWithEating.Hazard != null);

        [JsonIgnore]
        [ContentMap("support_with_drinking", order: 10)]
        [OverrideChildContentMap("enabled", "Do they require any aids or support with drinking?")]
        public Group<DetailedOptionalHazard> MappedSupportWithDrinking => new(SupportWithDrinking, SupportWithDrinking != null && SupportWithDrinking.Hazard != null);

        [JsonIgnore]
        [ContentMap("feeding_tube", order: 11)]
        [OverrideChildContentMap("enabled", "Do they have a feeding tube?")]
        public Group<DetailedHazard> MappedFeedingTube => new(FeedingTube);
    }
}
