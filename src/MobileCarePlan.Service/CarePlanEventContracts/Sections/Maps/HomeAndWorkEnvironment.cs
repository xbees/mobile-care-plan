using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using MobileCarePlan.Service.Extensions;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class HomeAndWorkEnvironment
    {
        [JsonIgnore]
        [ContentMap("property_type", "Property type", 1)]
        public string MappedPropertyType => PropertyTypeOther.HasValue() ? PropertyTypeOther : PropertyType.GetDescription();

        [JsonIgnore]
        [ContentMap("personal_safety_risks", order: 2)]
        [OverrideChildContentMap("enabled", "Are there any personal safety or lone working risks?")]
        public Group<DetailedHazard> MappedPersonalSafetyRisks => new(PersonalSafetyRisks);

        [JsonIgnore]
        [ContentMap("access_risks", order: 3)]
        [OverrideChildContentMap("enabled", "Are there any risks in relation to access and exiting the property? (Parking/Gates/Flooring/Lighting/etc)?")]
        public Group<DetailedHazard> MappedAccessRisks => new(AccessRisks);

        [JsonIgnore]
        [ContentMap("entry_hazard", order: 4)]
        [OverrideChildContentMap("details", "How do carers gain entry to the property?")]
        public DetailedHazard MappedEntryHazard => EntryHazard;

        [JsonIgnore]
        [ContentMap("pets_in_home", order: 5)]
        [OverrideChildContentMap("enabled", "Are there any pets within the property?")]
        public Group<DetailedHazard> MappedPetsInHome => new(PetsInHome);

        [JsonIgnore]
        [ContentMap("ventilation_risks", order: 6)]
        [OverrideChildContentMap("enabled", "Are there any ventilation related risks (including smoking)?")]
        public Group<DetailedHazard> MappedVentilationRisks => new(VentilationRisks);

        [JsonIgnore]
        [ContentMap("fire_alarms", order: 7)]
        public FireAlarms MappedFireAlarms => FireAlarms;

        [JsonIgnore]
        [ContentMap("slips_trips_falls", order: 8)]
        [OverrideChildContentMap("enabled", "Are there any risks of slips, trips and falls?")]
        public Group<DetailedHazard> MappedSlipsTripsFalls => new(SlipsTripsFalls);

        [JsonIgnore]
        [ContentMap("stairs_steps", order: 9)]
        [OverrideChildContentMap("enabled", "Are there any stairs / steps within the property?")]
        public Group<DetailedHazard> MappedStairsSteps => new(StairsSteps);

        [JsonIgnore]
        [ContentMap("gas_or_electric", order: 10)]
        [OverrideChildContentMap("enabled", "Will you be using electric and or gas equipment?")]
        public Group<DetailedHazard> MappedGasOrElectric => new(GasOrElectric);

        [JsonIgnore]
        [ContentMap("appropriate_lighting", order: 11)]
        public AppropriateLighting MappedAppropriateLighting => AppropriateLighting;

        [JsonIgnore]
        [ContentMap("hazardous_substances", order: 12)]
        [OverrideChildContentMap("enabled", "Are there any hazardous substances in the property?")]
        public Group<DetailedHazard> MappedHazardousSubstances => new(HazardousSubstances);

        [JsonIgnore]
        [ContentMap("emergency_details", "Emergency details", order: 13)]
        public Emergency EmergencyDetails => new Emergency
        {
            EscapeRoutes = EscapeRoutes,
            FireEvacuation = FireEvacuation,
            LocationOfElectricityMeter = LocationOfElectricityMeter,
            LocationOfGasMeter = LocationOfGasMeter,
            LocationOfFuseBox = LocationOfFuseBox,
            LocationOfWaterMains = LocationOfWaterMains,
        };
    }

    public class Emergency
    {
        [ContentMap("escape_routes", "Escape routes", order: 1)]
        public string EscapeRoutes { get; set; }

        [ContentMap("fire_evacuation", "Fire evacuation procedure", order: 2)]
        public string FireEvacuation { get; set; }

        [ContentMap("location_of_gas_meter", "Location of gas meter", order: 3)]
        public string LocationOfGasMeter { get; set; }

        [ContentMap("location_of_electricity_meter", "Location of electricity meter", order: 4)]
        public string LocationOfElectricityMeter { get; set; }

        [ContentMap("location_of_fuse_box", "Location of fuse box", order: 5)]
        public string LocationOfFuseBox { get; set; }

        [ContentMap("location_of_water_mains", "Location of water mains tap", order: 6)]
        public string LocationOfWaterMains { get; set; }
    }
}
