using Cera.Sdk.Dcp.CarePlan.Events.Sections.Models.CommunicationsAndAccessibility;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class CommunicationsAndAccessibility
    {
        [JsonIgnore]
        [ContentMap("preferred_comms_format", "Preferred method of communication", 1)]
        public string MappedPreferredCommunicationFormat => PreferredCommunicationFormat;

        [JsonIgnore]
        [ContentMap("learning_disabilities", order: 2)]
        [OverrideChildContentMap("enabled", "Do they have any learning disabilities?")]
        [OverrideChildContentMap("object", "Details and what support is needed")]
        public Group<string> MappedLearningDisabilities => new(LearningDisabilities, LearningDisabilities.HasValue());

        [JsonIgnore]
        [ContentMap("hearing_aid", order: 3)]
        [OverrideChildContentMap("enabled", "Do they use a hearing aid?")]
        public Group<Assistance> MappedHearingAid => new(HearingAid);

        [JsonIgnore]
        [ContentMap("glasses", order: 4)]
        [OverrideChildContentMap("enabled", "Do they need to wear glasses?")]
        public Group<Assistance> MappedGlasses => new(Glasses);

        [JsonIgnore]
        [ContentMap("first_language", "What is their first language?", 5)]
        public string MappedFirstLanguage => FirstLanguage?.Name;

        [JsonIgnore]
        [ContentMap("preferred_comms", "Preferred communication with Cera", 6)]
        public CommunicationType? MappedPreferredCommunication => PreferredCommunication;

        [JsonIgnore]
        [ContentMap("nominated_person", order: 7)]
        public NominatedPerson MappedNominatedPerson => NominatedPerson;
    }
}
