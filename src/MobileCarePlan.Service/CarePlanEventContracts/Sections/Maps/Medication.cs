using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.Medication;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class Medication
    {
        [JsonIgnore]
        [ContentMap("medication_support", order: 1)]
        [OverrideChildContentMap("details", "What support do they require with medication?")]
        public DetailedHazard MappedMedicationSupport => MedicationSupport;

        [JsonIgnore]
        [ContentMap("medication_support_storage", "Where is their medication stored?", order: 2)]
        public MedicationSupportStorage MappedMedicationSupportStorage => MedicationSupportStorage.Create(MedicationStorage, MedicationOrderedBy);

        [JsonIgnore]
        [ContentMap("blood_thinning_medication", order: 3)]
        [OverrideChildContentMap("enabled", "Do they currently take blood thinning medication?")]
        [OverrideChildContentMap("details", "Details of what they take")]
        public Group<DetailedHazard> MappedBloodThinningMedication => new(BloodThinningMedication);

        [JsonIgnore]
        [ContentMap("prn_medications", order: 4)]
        [OverrideChildContentMap("enabled", "Do they take any PRN medications?")]
        [OverrideChildContentMap("details", "Details of what they take")]
        public Group<DetailedHazard> MappedPrnMedications => new(PrnMedications);

        [JsonIgnore]
        [ContentMap("time_sensitive_medication", order: 5)]
        [OverrideChildContentMap("enabled", "Do they take any time-sensitive medications?")]
        [OverrideChildContentMap("details", "Details of what they take")]
        public Group<DetailedHazard> MappedTimeSensitiveMedication => new(TimeSensitiveMedication);

        [JsonIgnore]
        [ContentMap("covert_medications", order: 6)]
        [OverrideChildContentMap("enabled", "Are any medications covert?")]
        [OverrideChildContentMap("details", "Details of covert medications")]
        public Group<MedicationCovert> MappedCovertMedications => new(CovertMedications);

        [JsonIgnore]
        [ContentMap("homely_remedies", order: 7)]
        [OverrideChildContentMap("enabled", "Do they use any non-prescribed medications (homely remedies)?")]
        [OverrideChildContentMap("details", "Details of what they use")]
        public Group<DetailedHazard> MappedHomelyRemedies => new(HomelyRemedies);

        [JsonIgnore]
        [ContentMap("medication_allergies", order: 8)]
        [OverrideChildContentMap("enabled", "Do they have any medication allergies?")]
        [OverrideChildContentMap("details", "What is the allergy?")]
        public Group<List<DetailedHazard>> MappedMedicationAllergies => new(MedicationAllergies, MedicationAllergies.HasValue());
    }

    public class MedicationSupportStorage
    {
        [ContentMap("medication_storage", "Location", 1)]
        public string MedicationStorage { get; set; }

        [ContentMap("medication_ordered_by", "Who orders and collects their medication?", 2)]
        public string MedicationOrderedBy { get; set; }

        public static MedicationSupportStorage Create(string medicationStorage, string medicationOrderedBy)
        {
            if (!medicationStorage.HasValue() && !medicationOrderedBy.HasValue())
                return null;

            return new MedicationSupportStorage
            {
                MedicationStorage = medicationStorage,
                MedicationOrderedBy = medicationOrderedBy
            };
        }
    }
}
