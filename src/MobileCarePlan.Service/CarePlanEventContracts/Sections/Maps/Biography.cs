using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class Biography
    {
        [JsonIgnore]
        [ContentMap("life_story", "Service users life history and expectations for the future", 1)]
        public string MappedLifeHistory => LifeHistory;

        [JsonIgnore]
        [ContentMap("interests_and_hobbies", "Service users interests and hobbies", 2)]
        public string MappedInterestsAndHobbies => InterestsAndHobbies;
    }
}
