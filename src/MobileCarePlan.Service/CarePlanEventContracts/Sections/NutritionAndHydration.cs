using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class NutritionAndHydration
    {
        [JsonProperty("nutrition_and_hydration_support", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("nutrition_and_hydration_support")]
        public DetailedHazard NutritionAndHydrationSupport { get; set; }

        [JsonProperty("risk_of_dehydration", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("risk_of_dehydration")]
        public DetailedOptionalHazard RiskOfDehydration { get; set; }

        [JsonProperty("risk_of_malnutrition", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("risk_of_malnutrition")]
        public DetailedOptionalHazard RiskOfMalnutrition { get; set; }

        [JsonProperty("food_allergies", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("food_allergies")]
        public IEnumerable<FoodAllergyHazard> FoodAllergies { get; set; }

        [JsonProperty("dentures", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("dentures")]
        public DetailedHazard Dentures { get; set; }

        [JsonProperty("difficulty_swallowing", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("difficulty_swallowing")]
        public DetailedHazard DifficultySwallowing { get; set; }

        [JsonProperty("risk_of_choking", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("risk_of_choking")]
        public DetailedHazard RiskOfChoking { get; set; }

        [JsonProperty("speech_and_language_therapy", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("speech_and_language_therapy")]
        public DetailedHazard SpeechAndLanguageTherapy { get; set; }

        [JsonProperty("support_with_eating", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("support_with_eating")]
        public DetailedOptionalHazard SupportWithEating { get; set; }

        [JsonProperty("support_with_drinking", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("support_with_drinking")]
        public DetailedOptionalHazard SupportWithDrinking { get; set; }

        [JsonProperty("feeding_tube", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("feeding_tube")]
        public DetailedHazard FeedingTube { get; set; }
    }
}
