using System.Text.Json.Serialization;
using Cera.Sdk.Dcp.CarePlan.Events.Sections.Models.CommunicationsAndAccessibility;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class CommunicationsAndAccessibility
    {
        [JsonProperty("first_language")]
        [JsonPropertyName("first_language")]
        public Language FirstLanguage { get; set; }

        [JsonProperty("learning_disabilities", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("learning_disabilities")]
        public string LearningDisabilities { get; set; }

        [JsonProperty("preferred_comms_format")]
        [JsonPropertyName("preferred_comms_format")]
        public string PreferredCommunicationFormat { get; set; }

        [JsonProperty("preferred_comms", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("preferred_comms")]
        public CommunicationType? PreferredCommunication { get; set; }

        [JsonProperty("nominated_person")]
        [JsonPropertyName("nominated_person")]
        public NominatedPerson NominatedPerson { get; set; }

        [JsonProperty("hearing_aid")]
        [JsonPropertyName("hearing_aid")]
        public Assistance HearingAid { get; set; }

        [JsonProperty("glasses")]
        [JsonPropertyName("glasses")]
        public Assistance Glasses { get; set; }
    }
}
