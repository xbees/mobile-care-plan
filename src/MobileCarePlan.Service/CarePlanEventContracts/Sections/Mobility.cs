using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class Mobility
    {
        [JsonProperty("height_in_cm", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("height_in_cm")]
        public decimal? HeightInCm { get; set; }

        [JsonProperty("weight_in_kgs", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("weight_in_kgs")]
        public decimal? WeightInKgs { get; set; }

        [JsonProperty("ability_to_bear_weight", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("ability_to_bear_weight")]
        public DetailedHazard AbilityToBearWeight { get; set; }

        [JsonProperty("walking_aids", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("walking_aids")]
        public DetailedHazard WalkingAids { get; set; }

        [JsonProperty("independence_with_dressing", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("independence_with_dressing")]
        public DetailedHazard IndependenceWithDressing { get; set; }

        [JsonProperty("history_of_falls", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("history_of_falls")]
        public DetailedHazard HistoryOfFalls { get; set; }

        [JsonProperty("risk_of_falls", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("risk_of_falls")]
        public DetailedHazard RiskOfFalls { get; set; }

        [JsonProperty("lifeline_pendant", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("lifeline_pendant")]
        public DetailedHazard LifelinePendant { get; set; }

        [JsonProperty("ot_risk_assessment", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("ot_risk_assessment")]
        public string OtRiskAssessment { get; set; }

        [JsonProperty("in_and_out_of_bed", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("in_and_out_of_bed")]
        public DetailedHazard InAndOutOfBed { get; set; }

        [JsonProperty("transfer_to_toilet", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("transfer_to_toilet")]
        public DetailedHazard TransferToToilet { get; set; }

        [JsonProperty("assistance_on_toilet", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("assistance_on_toilet")]
        public DetailedHazard AssistanceOnToilet { get; set; }

        [JsonProperty("transfer_to_and_from_chair", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("transfer_to_and_from_chair")]
        public DetailedHazard TransferToAndFromChair { get; set; }

        [JsonProperty("shower_or_bath", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("shower_or_bath")]
        public ShowerOrbath ShowerOrBath { get; set; }

        [JsonProperty("hoist_or_lift", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("hoist_or_lift")]
        public Equipment HoistOrLift { get; set; }

        [JsonProperty("standing_aids", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("standing_aids")]
        public Equipment StandingAids { get; set; }

        [JsonProperty("sliding_equipment", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("sliding_equipment")]
        public Equipment SlidingEquipment { get; set; }

        [JsonProperty("wheel_chair", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("wheel_chair")]
        public Equipment WheelChair { get; set; }

        [JsonProperty("other_equipment", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("other_equipment")]
        public IEnumerable<OtherEquipment> OtherEquipment { get; set; }
    }
}
