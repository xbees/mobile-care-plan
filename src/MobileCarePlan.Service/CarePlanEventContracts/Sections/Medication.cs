using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.Medication;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class Medication
    {
        [JsonProperty("medication_support")]
        [JsonPropertyName("medication_support")]
        public DetailedHazard MedicationSupport { get; set; }

        [JsonProperty("medication_storage", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("medication_storage")]
        public string MedicationStorage { get; set; }

        [JsonProperty("medication_ordered_by", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("medication_ordered_by")]
        public string MedicationOrderedBy { get; set; }

        [JsonProperty("blood_thinning_medication", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("blood_thinning_medication")]
        public DetailedHazard BloodThinningMedication { get; set; }

        [JsonProperty("prn_medications", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("prn_medications")]
        public DetailedHazard PrnMedications { get; set; }

        [JsonProperty("time_sensitive_medication", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("time_sensitive_medication")]
        public DetailedHazard TimeSensitiveMedication { get; set; }

        [JsonProperty("covert_medications", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("covert_medications")]
        public MedicationCovert CovertMedications { get; set; }

        [JsonProperty("homely_remedies", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("homely_remedies")]
        public DetailedHazard HomelyRemedies { get; set; }

        [JsonProperty("medication_allergies", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("medication_allergies")]
        public List<DetailedHazard> MedicationAllergies { get; set; } = new List<DetailedHazard>();
    }
}
