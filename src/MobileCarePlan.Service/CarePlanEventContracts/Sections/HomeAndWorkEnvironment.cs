using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class HomeAndWorkEnvironment
    {
        [JsonProperty("property_type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("property_type")]
        public PropertyType? PropertyType { get; set; }

        [JsonProperty("property_type_other", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("property_type_other")]
        public string PropertyTypeOther { get; set; }

        [JsonProperty("personal_safety_risks", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("personal_safety_risks")]
        public DetailedHazard PersonalSafetyRisks { get; set; }

        [JsonProperty("access_risks", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("access_risks")]
        public DetailedHazard AccessRisks { get; set; }

        [JsonProperty("entry_hazard")]
        [JsonPropertyName("entry_hazard")]
        public DetailedHazard EntryHazard { get; set; }

        [JsonProperty("pets_in_home", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("pets_in_home")]
        public DetailedHazard PetsInHome { get; set; }

        [JsonProperty("ventilation_risks", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("ventilation_risks")]
        public DetailedHazard VentilationRisks { get; set; }

        [JsonProperty("fire_alarms", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("fire_alarms")]
        public FireAlarms FireAlarms { get; set; }

        [JsonProperty("slips_trips_falls", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("slips_trips_falls")]
        public DetailedHazard SlipsTripsFalls { get; set; }

        [JsonProperty("stairs_steps", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("stairs_steps")]
        public DetailedHazard StairsSteps { get; set; }

        [JsonProperty("gas_or_electric", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("gas_or_electric")]
        public DetailedHazard GasOrElectric { get; set; }

        [JsonProperty("appropriate_lighting", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("appropriate_lighting")]
        public AppropriateLighting AppropriateLighting { get; set; }

        [JsonProperty("hazardous_substances", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("hazardous_substances")]
        public DetailedHazard HazardousSubstances { get; set; }

        [JsonProperty("escape_routes", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("escape_routes")]
        public string EscapeRoutes { get; set; }

        [JsonProperty("fire_evacuation", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("fire_evacuation")]
        public string FireEvacuation { get; set; }

        [JsonProperty("location_of_gas_meter", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("location_of_gas_meter")]
        public string LocationOfGasMeter { get; set; }

        [JsonProperty("location_of_electricity_meter", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("location_of_electricity_meter")]
        public string LocationOfElectricityMeter { get; set; }

        [JsonProperty("location_of_fuse_box", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("location_of_fuse_box")]
        public string LocationOfFuseBox { get; set; }

        [JsonProperty("location_of_water_mains", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("location_of_water_mains")]
        public string LocationOfWaterMains { get; set; }
    }
}
