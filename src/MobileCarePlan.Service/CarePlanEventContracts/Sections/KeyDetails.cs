using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class KeyDetails
    {
        [JsonProperty("alias")]
        [JsonPropertyName("alias")]
        public string Alias { get; set; }

        [JsonProperty("email")]
        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonProperty("house_mate")]
        [JsonPropertyName("house_mate")]
        public string HouseMate { get; set; }

        [JsonProperty("able_to_open_door")]
        [JsonPropertyName("able_to_open_door")]
        public bool AbleToOpenDoor { get; set; }

        [JsonProperty("key_safe_number")]
        [JsonPropertyName("key_safe_number")]
        public string KeySafeNumber { get; set; }

        [JsonProperty("ethnicity")]
        [JsonPropertyName("ethnicity")]
        public string Ethnicity { get; set; }

        [JsonProperty("ethnicity_other")]
        [JsonPropertyName("ethnicity_other")]
        public string EthnicityOther { get; set; }

        [JsonProperty("religion")]
        [JsonPropertyName("religion")]
        public string Religion { get; set; }

        [JsonProperty("religion_other")]
        [JsonPropertyName("religion_other")]
        public string ReligionOther { get; set; }

        [JsonProperty("relationship_status")]
        [JsonPropertyName("relationship_status")]
        public string RelationshipStatus { get; set; }

        [JsonProperty("sexual_orientation")]
        [JsonPropertyName("sexual_orientation")]
        public string SexualOrientation { get; set; }

        [JsonProperty("gender")]
        [JsonPropertyName("gender")]
        public string Gender { get; set; }

        [JsonProperty("gender_other")]
        [JsonPropertyName("gender_other")]
        public string GenderOther { get; set; }

        [JsonProperty("contacts")]
        [JsonPropertyName("contacts")]
        public List<Relationship> Contacts { get; set; } = new List<Relationship>();
    }
}
