using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class DecisionsAroundCare
    {
        [JsonProperty("can_make_formal_decisions")]
        [JsonPropertyName("can_make_formal_decisions")]
        public bool CanMakeFormalDecisions { get; set; }

        [JsonProperty("formal_decision_details", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("formal_decision_details")]
        public string FormalDecisionsDetails { get; set; }

        [JsonProperty("formal_decision_assistance")]
        [JsonPropertyName("formal_decision_assistance")]
        public Assistance FormalDecisionsAssistance { get; set; }

        [JsonProperty("can_make_informal_decisions")]
        [JsonPropertyName("can_make_informal_decisions")]
        public bool CanMakeInformalDecisions { get; set; }

        [JsonProperty("informal_decision_details", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("informal_decision_details")]
        public string InformalDecisionsDetails { get; set; }

        [JsonProperty("mental_capacity_assessment_required")]
        [JsonPropertyName("mental_capacity_assessment_required")]
        public YesNoOption MentalCapacityAssessmentRequired { get; set; }
    }
}
