using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class Oxygen
    {
        [JsonProperty("oxygen_support", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("oxygen_support")]
        public DetailedHazard OxygenSupport { get; set; }

        [JsonProperty("oxygen_environmental", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("oxygen_environmental")]
        public Hazard OxygenEnvironmental { get; set; }
    }
}
