using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class Biography
    {
        [JsonProperty("life_story", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("life_story")]
        public string LifeHistory { get; set; }

        [JsonProperty("interests_and_hobbies", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("interests_and_hobbies")]
        public string InterestsAndHobbies { get; set; }
    }
}
