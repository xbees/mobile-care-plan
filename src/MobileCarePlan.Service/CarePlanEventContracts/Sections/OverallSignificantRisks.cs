using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class OverallSignificantRisks
    {
        [JsonProperty("details", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("details")]
        public string SignificantRisksDetails { get; set; }
    }
}
