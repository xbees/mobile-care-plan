using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Common;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Models.DailyRoutine;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class DailyRoutine
    {
        [JsonProperty("oral_health")]
        [JsonPropertyName("oral_health")]
        public Assistance OralHealth { get; set; }

        [JsonProperty("dietary_requirements", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("dietary_requirements")]
        public string DietaryRequirements { get; set; }

        [JsonProperty("routines")]
        [JsonPropertyName("routines")]
        public List<RoutineItem> Routines { get; set; } = new List<RoutineItem>();

        [JsonProperty("food_allegies")]
        [JsonPropertyName("food_allegies")]
        public List<string> FoodAllegies { get; set; } = new List<string>();
    }
}
