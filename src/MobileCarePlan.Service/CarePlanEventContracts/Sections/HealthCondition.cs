using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Sections
{
    public partial class HealthCondition
    {
        [JsonProperty("name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonProperty("diagnosis_month", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("diagnosis_month")]
        public int DiagnosisMonth { get; set; }

        [JsonProperty("diagnosis_year", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("diagnosis_year")]
        public int DiagnosisYear { get; set; }

        [JsonProperty("details", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("details")]
        public string Details { get; set; }

        [JsonProperty("treatment", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("treatment")]
        public string Treatment { get; set; }
    }
}
