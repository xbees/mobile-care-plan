using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class FoodAllergyHazard
    {
        [ContentMap("name", "Food allergy", 1)]
        [JsonProperty("name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [ContentMap("hazard", order: 2)]
        [JsonProperty("hazard")]
        [JsonPropertyName("hazard")]
        public Hazard Hazard { get; set; }
    }
}
