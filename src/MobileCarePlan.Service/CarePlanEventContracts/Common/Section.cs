﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class Section : GuidItem
    {
        [Required]
        [JsonRequired]
        [JsonProperty("title")]
        [JsonPropertyName("title")]
        public string Title { get; set; }

        [Required]
        [JsonRequired]
        [JsonProperty("description")]
        [JsonPropertyName("description")]
        public string Description { get; set; }
    }

    public class Section<TData> : Section where TData : class, new()
    {
        [Required]
        [JsonRequired]
        [JsonProperty("is_completed")]
        [JsonPropertyName("is_completed")]
        public bool IsCompleted { get; set; }

        [ContentMap]
        [Required]
        [JsonRequired]
        [JsonProperty("data")]
        [JsonPropertyName("data")]
        public TData Data { get; set; }
    }
}
