﻿using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public abstract class Careplan : GuidItem
    {
        [JsonProperty("client")]
        [JsonPropertyName("client")]
        public Client Client { get; set; }

        [JsonProperty("package_type")]
        [JsonPropertyName("package_type")]
        public PackageType PackageType { get; set; }
    }
}
