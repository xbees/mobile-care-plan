using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using MobileCarePlan.Service.CarePlanEventContracts.Sections.Maps;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class Equipment
    {
        [ContentMap("provider", "Who provided this?", 1)]
        [JsonProperty("provider")]
        [JsonPropertyName("provider")]
        public string Provider { get; set; }

        [ContentMap("make_and_model", "Make and model", 2)]
        [JsonProperty("make_and_model")]
        [JsonPropertyName("make_and_model")]
        public string MakeAndModel { get; set; }

        [JsonProperty("date_of_last_inspection", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("date_of_last_inspection")]
        public DateTime? DateOfLastInspection { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        [System.Text.Json.Serialization.JsonIgnore]
        [ContentMap("date_of_last_inspection", "Date of last inspection (month and year) (Optional)", 3)]
        public string DateOfLastInspectionString => DateOfLastInspection?.ToString("MM/yyyy");

        [JsonProperty("date_of_next_inspection", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("date_of_next_inspection")]
        public DateTime? DateOfNextInspection { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        [System.Text.Json.Serialization.JsonIgnore]
        [ContentMap("date_of_next_inspection", "Date of next inspection (month and year) (Optional)", 4)]
        public string DateOfNextInspectionString => DateOfNextInspection?.ToString("MM/yyyy");

        [JsonProperty("support_su_weight", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("support_su_weight")]
        public string SupportSuWeight { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        [System.Text.Json.Serialization.JsonIgnore]
        [ContentMap("support_su_weight", order: 5)]
        [OverrideChildContentMap("enabled", "Does this equipment support the weight of the service user?")]
        [OverrideChildContentMap("object", "Details")]
        public Group<string> MappedSupportSuWeight => new(SupportSuWeight, !SupportSuWeight.HasValue());

        [ContentMap("concerns", "Any concerns with this equipment?", 6)]
        [JsonProperty("concerns")]
        [JsonPropertyName("concerns")]
        public string Concerns { get; set; }

        [ContentMap("hazard", order: 7)]
        [JsonProperty("hazard")]
        [JsonPropertyName("hazard")]
        public Hazard Hazard { get; set; }
    }
}
