﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public sealed class Client : GuidItem
    {
        [JsonProperty("first_name")]
        [JsonPropertyName("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("middle_name")]
        [JsonPropertyName("middle_name")]
        public string MiddleName { get; set; }

        [JsonProperty("last_name")]
        [JsonPropertyName("last_name")]
        public string LastName { get; set; }
    }
}
