using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class Hazard
    {
        [ContentMap("hazards", "Hazard", 11)]
        [JsonProperty("hazard")]
        [JsonPropertyName("hazard")]
        public string HazardInfo { get; set; }

        [ContentMap("initial_risk", "Initial risk level", 12)]
        [JsonProperty("initial_risk")]
        [JsonPropertyName("initial_risk")]
        public RiskLevel InitialRisk { get; set; }

        [ContentMap("control_measures", "Control measures", 13)]
        [JsonProperty("control_measures")]
        [JsonPropertyName("control_measures")]
        public string ControlMeasures { get; set; }

        [ContentMap("residual_risk", "Residual risk level", 14)]
        [JsonProperty("residual_risk")]
        [JsonPropertyName("residual_risk")]
        public RiskLevel ResidualRisk { get; set; }
    }
}
