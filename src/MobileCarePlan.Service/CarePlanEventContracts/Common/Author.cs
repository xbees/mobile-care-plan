﻿using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public sealed class Author
    {
        [ContentMap("name", "Full name", 1)]
        [JsonProperty("name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [ContentMap("relationship", "Relationship to the service user", 2)]
        [JsonProperty("relationship")]
        [JsonPropertyName("relationship")]
        public string Relationship { get; set; }
    }
}
