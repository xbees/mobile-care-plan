using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class SuContinence
    {
        [JsonProperty("has_continence_aids", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("has_continence_aids")]
        public bool ContinenceAidsExists { get; set; }

        [JsonProperty("su_continent", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("su_continent")]
        public DetailedHazard SuContinent { get; set; }
    }
}
