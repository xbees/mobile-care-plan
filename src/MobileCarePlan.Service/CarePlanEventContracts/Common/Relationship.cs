using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public sealed class Relationship : Contact
    {
        [ContentMap("relation", "Relationship to the service user", order: 2)]
        [JsonProperty("relation")]
        [JsonPropertyName("relation")]
        public string Relation { get; set; }
    }
}
