using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class Assistance
    {
        [ContentMap("support_required", "Do they need support with this?", 1)]
        [JsonProperty("support_required")]
        [JsonPropertyName("support_required")]
        public bool SupportRequired { get; set; }

        [ContentMap("details", "What support do they need?", 2)]
        [JsonProperty("details")]
        [JsonPropertyName("details")]
        public string Details { get; set; }
    }
}
