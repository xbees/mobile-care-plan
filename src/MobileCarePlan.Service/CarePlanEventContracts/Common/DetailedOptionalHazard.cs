using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class DetailedOptionalHazard
    {
        [ContentMap("details", "Details", 1)]
        [JsonProperty("details")]
        [JsonPropertyName("details")]
        public string Details { get; set; }

        [ContentMap("hazard", order: 2)]
        [JsonProperty("hazard", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("hazard")]
        public Hazard Hazard { get; set; }
    }
}
