﻿using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class Contact
    {
        [ContentMap("name", "Full name", order: 1)]
        [JsonProperty("name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [ContentMap("phone_number", "Phone number", order: 4)]
        [JsonProperty("phone_number")]
        [JsonPropertyName("phone_number")]
        public string PhoneNumber { get; set; }

        [ContentMap("second_phone_number", "Second phone number (Optional)", order: 5)]
        [JsonProperty("second_phone_number")]
        [JsonPropertyName("second_phone_number")]
        public string SecondPhoneNumber { get; set; }

        [ContentMap("address", "Address (inc postcode)", order: 3)]
        [JsonProperty("address")]
        [JsonPropertyName("address")]
        public string Address { get; set; }
    }
}
