using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class ShowerOrbath : DetailedHazard
    {
        [ContentMap("uses_shower_or_bath", "Do they use the shower or the bath?")]
        [JsonProperty("uses_shower_or_bath")]
        [JsonPropertyName("uses_shower_or_bath")]
        public bool UsesShowerOrBath { get; set; }
    }
}
