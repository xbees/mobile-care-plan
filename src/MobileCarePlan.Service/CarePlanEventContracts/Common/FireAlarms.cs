using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class FireAlarms : DetailedHazard
    {
        [ContentMap("has_fire_alarms", "Does the property have working fire / smoke alarms fitted?")]
        [JsonProperty("has_fire_alarms")]
        [JsonPropertyName("has_fire_alarms")]
        public bool HasFireAlarms { get; set; }
    }
}
