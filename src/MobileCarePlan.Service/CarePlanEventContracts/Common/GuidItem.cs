﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class GuidItem
    {
        [JsonProperty("guid")]
        [JsonPropertyName("guid")]
        public Guid Guid { get; set; }
    }
}
