using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.Enums;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class Stoma
    {
        [ContentMap("stoma_type", "What type of stoma?", 1)]
        [JsonProperty("stoma_type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("stoma_type")]
        public StomaType StomaType { get; set; }

        [ContentMap("stoma_support", "What support does the service user need with this?", 2)]
        [JsonProperty("stoma_support", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("stoma_support")]
        public string StomaSupport { get; set; }

        [ContentMap("stoma_hazard", order: 3)]
        [JsonProperty("stoma_hazard", NullValueHandling = NullValueHandling.Ignore)]
        [JsonPropertyName("stoma_hazard")]
        public Hazard StomaHazard { get; set; }
    }
}
