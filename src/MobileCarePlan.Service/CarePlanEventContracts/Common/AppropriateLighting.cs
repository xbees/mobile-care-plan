using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class AppropriateLighting : DetailedHazard
    {
        [ContentMap("is_lighting_appropriated", "Is the lighting appropriate within the working area?")]
        [JsonProperty("is_lighting_appropriated")]
        [JsonPropertyName("is_lighting_appropriated")]
        public bool IsLightingAppropriated { get; set; }
    }
}
