using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class UserOperator
    {
        [JsonProperty("name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [Required]
        [JsonRequired]
        [JsonProperty("guid")]
        [JsonPropertyName("guid")]
        public Guid Guid { get; set; }

        public static UserOperator Create(Guid guid, string name)
        {
            return new UserOperator { Guid = guid, Name = name };
        }
    }
}
