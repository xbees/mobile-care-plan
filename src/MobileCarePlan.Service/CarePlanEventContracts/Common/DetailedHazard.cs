using System.Text.Json.Serialization;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;
using Newtonsoft.Json;

namespace MobileCarePlan.Service.CarePlanEventContracts.Common
{
    public class DetailedHazard : Hazard
    {
        [ContentMap("details", "Details", 1)]
        [JsonProperty("details")]
        [JsonPropertyName("details")]
        public string Details { get; set; }
    }
}
