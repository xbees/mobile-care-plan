﻿using MobileCarePlan.Service.CarePlanEventContracts;
using MobileCarePlan.Service.CarePlanEventContracts.MappingAttributes;

namespace MobileCarePlan.Service
{
    public static class CarerReviewHelper
    {
        public static List<string> GetCarePlanSectionsList()
        {
            var sectionIds = typeof(CareplanPublishedV1.SectionCollection).GetProperties().Where(p => p.GetCustomAttributes(false).Any(a => a is SectionMapAttribute)).Select(p =>
            {
                var map = (SectionMapAttribute)p.GetCustomAttributes(false).FirstOrDefault(a => a is SectionMapAttribute);
                return map?.Name;
            });

            return sectionIds.ToList();
        }
    }
}
