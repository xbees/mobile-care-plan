using MobileCarePlan.Data;
using MobileCarePlan.Data.Models;
using MobileCarePlan.Service.CarePlanEventContracts;
using MobileCarePlan.Service.Extensions;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace MobileCarePlan.Service.Tests
{
    public class MobileCarePlanServiceTests
    {
        private readonly MobileCarePlanService service;
        private readonly Mock<IMobileCarePlanPublishedEventRepository> carePlanPublishedEventRepositoryMock;
        private readonly Mock<IMobileCarePlanRepository> carePlanRepositoryMock;
        private readonly Mock<ICarerReviewRepository> carerReviewRepositoryMock;

        public MobileCarePlanServiceTests()
        {
            carePlanPublishedEventRepositoryMock = new Mock<IMobileCarePlanPublishedEventRepository>();
            carePlanRepositoryMock = new Mock<IMobileCarePlanRepository>();
            carerReviewRepositoryMock = new Mock<ICarerReviewRepository>();
            service = new MobileCarePlanService(carePlanPublishedEventRepositoryMock.Object, carePlanRepositoryMock.Object, carerReviewRepositoryMock.Object);
        }

        [Fact]
        public async Task GetCarePlanAsync_Returns()
        {
            // Arrange
            var carePlanDataEvent = JsonConvert.DeserializeObject<CareplanPublishedV1>(TestData.CarePlanPublishedEvent);
            carePlanDataEvent.SectionsWithData.OverallSignificantRisks = null;// delete OverallSignificantRisks section from current event
            var carePlanData = carePlanDataEvent.ToCarePlanData(null);

            var newCarePlanDataEvent = JsonConvert.DeserializeObject<CareplanPublishedV1>(TestData.CarePlanPublishedEvent);
            newCarePlanDataEvent.PublishedAt = DateTime.UtcNow;
            newCarePlanDataEvent.SectionsWithData.Biography.Data.InterestsAndHobbies = null; // update Biography section by removing InterestsAndHobbies content
            newCarePlanDataEvent.SectionsWithData.PersonalDetails.Data.Authors.Add( // update PersonalDetails section by adding new author
                new CarePlanEventContracts.Common.Author { 
                    Name = "New Author", 
                    Relationship = "Cooworker" 
                });
            var updatedCarePlanData = newCarePlanDataEvent.ToCarePlanData(carePlanData);

            var x = carePlanData.Sections.ToDictionary(x => x.Id, x => carePlanData.LastUpdatedAt);

            carerReviewRepositoryMock.Setup(x => x.GetAsync(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new CarerReviewData(
                    Guid.NewGuid(),
                    Guid.NewGuid(),
                    carePlanData.Sections.ToDictionary(x => x.Id, x => carePlanData.LastUpdatedAt)
                ));

            carePlanRepositoryMock.Setup(x => x.GetAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync(updatedCarePlanData);

            // Act
            var result = await service.GetCarePlanAsync(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<CancellationToken>()).ConfigureAwait(false);
            var needsReviewSections = result.Sections.Where(s => s.NeedsReview).Select(s => new
            {
                s.Id,
                Contents = s.Contents.Where(c => c.NeedsReview).ToList(),
            }).ToList();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(3, needsReviewSections.Count);
            Assert.Contains("personal_details", needsReviewSections.Select(s => s.Id));
            Assert.Equal(2, needsReviewSections.First(s => s.Id == "personal_details").Contents.Count);
            Assert.Contains("overall_significant_risks", needsReviewSections.Select(s => s.Id));
            Assert.Single(needsReviewSections.First(s => s.Id == "overall_significant_risks").Contents);
            Assert.Contains("biography", needsReviewSections.Select(s => s.Id));
        }

        [Fact]
        public async Task GetCarePlanLastUpdateAsync_WhenNotFoundCarerReviewData_ReviewAllSections()
        {
            // Arrange
            var lateUpdateDate = DateTime.UtcNow;
            const string lastUpdateName = "Test User";

            var carePlanData = MakeTestUpdatedCarePlan(lateUpdateDate, lastUpdateName);

            carePlanRepositoryMock.Setup(x => x.GetAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync(carePlanData);

            // Act
            var result = await service.GetCarePlanLastUpdateAsync(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<CancellationToken>()).ConfigureAwait(false);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(lateUpdateDate, result.Timestamp);
            Assert.Equal(lastUpdateName, result.Author);
            Assert.Equal(3, result.UpdatedSections.Count());
            Assert.Contains("personal_details", result.UpdatedSections);
            Assert.Contains("key_details", result.UpdatedSections);
            Assert.Contains("decisions_around_care", result.UpdatedSections);
        }

        [Fact]
        public async Task GetCarePlanLastUpdateAsync_WhenFoundCarerReviewData_ReviewProperSections()
        {
            // Arrange
            var lateUpdateDate = DateTime.UtcNow;
            const string lastUpdateName = "Test User";

            var carePlanData = MakeTestUpdatedCarePlan(lateUpdateDate, lastUpdateName);

            carerReviewRepositoryMock.Setup(x => x.GetAsync(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new CarerReviewData(
                    Guid.NewGuid(),
                    Guid.NewGuid(), 
                    new Dictionary<string, DateTime>
                    {
                        ["personal_details"] = lateUpdateDate.AddDays(-1),
                        ["key_details"] = lateUpdateDate.AddDays(-1)
                    }
                ));
            carePlanRepositoryMock.Setup(x => x.GetAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync(carePlanData);

            // Act
            var result = await service.GetCarePlanLastUpdateAsync(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<CancellationToken>()).ConfigureAwait(false);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(lateUpdateDate, result.Timestamp);
            Assert.Equal(lastUpdateName, result.Author);
            Assert.Equal(2, result.UpdatedSections.Count());
            Assert.Contains("personal_details", result.UpdatedSections);
            Assert.Contains("decisions_around_care", result.UpdatedSections);
        }

        private CarePlanData MakeTestUpdatedCarePlan(DateTime lateUpdateDate, string lastUpdateName)
        {
            return new CarePlanData
            {
                LastUpdatedAt = lateUpdateDate,
                LastUpdatedByName = lastUpdateName,
                Sections = new List<CarePlanSectionItem>
                {
                    new CarePlanSectionItem
                    {
                        Id = "personal_details",
                        SortOrder = 1,
                        Category = "main",
                        Subtitle = "Personal details and advance care planning inc DNACPR, next of kin and POA",
                        Title = "Personal details and advance care planning",
                        LastUpdatedAt = lateUpdateDate,
                        Contents = new List<CarePlanSectionContentItem>
                        {
                            new CarePlanSectionContentItem{
                                Content =  "Peter Apple",
                                Id = "full_name",
                                SortOrder = 1,
                                Title = "Full name",
                                Type = "text",
                                LastUpdatedAt = lateUpdateDate,
                            },
                            new CarePlanSectionContentItem{
                                Content =  "King James Ct, London SE1 0DH, UK",
                                Id = "address",
                                SortOrder = 2,
                                Title = "Address (inc postcode)",
                                Type = "text",
                                LastUpdatedAt = lateUpdateDate.AddDays(-1)
                            }
                        }
                    },
                    new CarePlanSectionItem
                    {
                        Id = "key_details",
                        SortOrder = 2,
                        Category = "main",
                        Subtitle = "Other personal details and key contacts",
                        Title = "Details and key contacts",
                        LastUpdatedAt = lateUpdateDate.AddDays(-1),
                        Contents = new List<CarePlanSectionContentItem>
                        {
                            new CarePlanSectionContentItem{
                                Content =  "P",
                                Id = "alias",
                                SortOrder = 1,
                                Title = "Name service user wants to be known by (Optional)",
                                Type = "text",
                                LastUpdatedAt = lateUpdateDate.AddDays(-1),
                            },
                            new CarePlanSectionContentItem{
                                Content =  "test@test.com",
                                Id = "email",
                                SortOrder = 2,
                                Title = "Email address (Optional)",
                                Type = "text",
                                LastUpdatedAt = lateUpdateDate.AddDays(-1)
                            }
                        }
                    },
                    new CarePlanSectionItem
                    {
                        Id = "decisions_around_care",
                        SortOrder = 3,
                        Category = "main",
                        Subtitle = "Level of support needed from service user",
                        Title = "Decisions around care",
                        LastUpdatedAt = lateUpdateDate,
                        Contents = new List<CarePlanSectionContentItem>
                        {
                            new CarePlanSectionContentItem{
                                Content =  "NO",
                                Id = "can_make_formal_decisions",
                                SortOrder = 1,
                                Title = "Is the service user able to make decisions on all aspects of their life including day to day decisions, finance and welfare?",
                                Type = "text",
                                LastUpdatedAt = lateUpdateDate,
                            },
                            new CarePlanSectionContentItem{
                                Content =  "They need support",
                                Id = "formal_decision_details",
                                SortOrder = 2,
                                Title = "Ability to make decisions",
                                Type = "text",
                                LastUpdatedAt = lateUpdateDate
                            }
                        }
                    }
                }
            };
        }
    }
}
