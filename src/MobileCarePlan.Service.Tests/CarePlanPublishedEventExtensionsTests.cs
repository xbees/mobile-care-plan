﻿using MobileCarePlan.Service.CarePlanEventContracts;
using MobileCarePlan.Service.Extensions;
using Newtonsoft.Json;
using Xunit;

namespace MobileCarePlan.Service.Tests
{
    public class CarePlanPublishedEventExtensionsTests
    {
        [Fact]
        public void ToCarePlanData_WithContentItemValues_ReturnsContent()
        {
            // Arrange
            var carePlanEvent = JsonConvert.DeserializeObject<CareplanPublishedV1>(TestData.CarePlanPublishedEvent);

            // Act
            var carePlan = carePlanEvent.ToCarePlanData(null);

            // Assert
            Assert.NotNull(carePlan);
            Assert.True(carePlan.Sections.All(s => s.Contents.All(c => !string.IsNullOrEmpty(c.Title) && (c.Type == "header" || !string.IsNullOrEmpty(c.Content)))));
        }

        [Fact]
        public void ToCarePlanData_WithEmptyContentItemValue_ReturnsContentWithouthEmptyContentItem()
        {
            // Arrange
            var carePlanEvent = JsonConvert.DeserializeObject<CareplanPublishedV1>(TestData.CarePlanPublishedEvent);

            // Act
            var carePlan = carePlanEvent.ToCarePlanData(null);

            // Assert
            var personalDetailsContentItems = carePlan.Sections.First(x => x.Id == "personal_details");
            var pharmacyAddress = personalDetailsContentItems.Contents.FirstOrDefault(x => x.Id == "pharmacy.address");
            Assert.Null(pharmacyAddress);
        }
    }
}
