using MobileCarePlan.Data;
using MobileCarePlan.Service.CarerReviewContracts;
using Moq;
using Xunit;

namespace MobileCarePlan.Service.Tests
{
    public class CarerReviewServiceTests
    {
        private readonly CarerReviewService _sut;
        private readonly Mock<ICarerReviewRepository> _carerReviewRepositoryMock;

        public CarerReviewServiceTests()
        {
            _carerReviewRepositoryMock = new Mock<ICarerReviewRepository>();

            _sut = new CarerReviewService(_carerReviewRepositoryMock.Object);
        }

        [Fact]
        public async Task UpdateCarePlanCarerSectionReviewAsync_CallsUpdate()
        {
            // Arrange
            var request = new CarerReviewRequest() { SectionId = "section1", ReviewedAt = DateTime.Now };

            // Act
            await _sut.UpdateCarerReviewAsync(Guid.NewGuid(), Guid.NewGuid(), request);

            // Assert
            _carerReviewRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
