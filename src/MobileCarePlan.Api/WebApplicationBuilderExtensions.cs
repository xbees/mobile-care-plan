﻿using System.Reflection;

namespace MobileCarePlan.Api
{
    public static class WebApplicationBuilderExtensions
    {
        private static readonly Assembly assembly = Assembly.GetEntryAssembly();
        public static WebApplicationBuilder ConfigureApiVersion(this WebApplicationBuilder builder)
        {
            var assemblyName = assembly.GetName();
            builder.Configuration.AddInMemoryCollection(new Dictionary<string, string>
            {
                ["Api:Name"] = assemblyName.Name,
                ["Api:Version"] = assemblyName.Version.ToString(),
            });

            return builder;
        }
    }
}
