﻿namespace MobileCarePlan.Api.Instrumentation
{
    public class InstrumentationSettings
    {
        public string CollectorEndpoint { get; set; }
        public TimeSpan CollectorEndpointTimeout { get; set; }

        public int CollectorTimeoutMilliseconds => (int)CollectorEndpointTimeout.TotalMilliseconds;
    }
}
