﻿using OpenTelemetry;
using OpenTelemetry.Exporter;
using OpenTelemetry.Metrics;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;

namespace MobileCarePlan.Api.Instrumentation
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection SetupTelemetry(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            var instrumentationSettings = configuration.GetSection(ApiConfigSectionNames.Instrumentation).Get<InstrumentationSettings>();

            if (string.IsNullOrWhiteSpace(instrumentationSettings?.CollectorEndpoint))
            {
                return serviceCollection;
            }

            var apiVersionInfo = configuration.GetSection(ApiConfigSectionNames.Api).Get<ApiSettings>();

            serviceCollection
                .AddOpenTelemetry()
                .WithTracing(builder =>
                {
                    builder.SetResourceBuilder(ResourceBuilder.CreateDefault().AddService(serviceName: apiVersionInfo.Name, serviceVersion: apiVersionInfo.Version));
                    builder.AddOtlpExporter(options => ConfigureOtlpExporter(options, instrumentationSettings));
                    builder.AddAspNetCoreInstrumentation();
                    builder.AddAWSInstrumentation();
                })
                .WithMetrics(builder =>
                {
                    builder.AddMeter(apiVersionInfo.Name);
                    builder.AddOtlpExporter(options => ConfigureOtlpExporter(options, instrumentationSettings));
                    builder.AddAspNetCoreInstrumentation();
                })
                .StartWithHost();

            return serviceCollection;

            static void ConfigureOtlpExporter(OtlpExporterOptions options, InstrumentationSettings instrumentationSettings)
            {
                options.Endpoint = new Uri(instrumentationSettings.CollectorEndpoint);
                options.Protocol = OtlpExportProtocol.Grpc;
                options.TimeoutMilliseconds = instrumentationSettings.CollectorTimeoutMilliseconds;
            }
        }
    }
}
