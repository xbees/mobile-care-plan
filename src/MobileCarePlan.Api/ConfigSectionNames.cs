﻿namespace MobileCarePlan.Api
{
    public static class ApiConfigSectionNames
    {
        public const string Api = "Api";
        public const string Logs = "Logs";
        public const string Instrumentation = "Instrumentation";
    }
}
