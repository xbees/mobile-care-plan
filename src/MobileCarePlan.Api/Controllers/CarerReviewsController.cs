﻿using Microsoft.AspNetCore.Mvc;
using MobileCarePlan.Service;
using MobileCarePlan.Service.CarerReviewContracts;
using MobileCarePlan.Service.Extensions;
using MobileCarePlan.Shared.Security;

namespace MobileCarePlan.Api.Controllers
{
    [Route("carer-reviews")]
    public class CarerReviewsController : ControllerBase
    {
        private readonly ICarerReviewService _carerReviewService;

        public CarerReviewsController(ICarerReviewService carerReviewService)
        {
            _carerReviewService = carerReviewService;
        }

        [HttpPost]
        [Route("{serviceUserId}")]
        public async Task<IActionResult> UpdateCarePlanCarerSectionReview([FromRoute] Guid serviceUserId, [FromBody] CarerReviewRequest carerReviewRequest, CancellationToken cancellationToken = default)
        {
            carerReviewRequest.Validate();

            var carerId = HttpContext.User.GetUserId();
            await _carerReviewService.UpdateCarerReviewAsync(carerId.Value, serviceUserId, carerReviewRequest, cancellationToken);

            return Ok();
        }
    }
}
