﻿using Microsoft.AspNetCore.Mvc;
using MobileCarePlan.Service;
using MobileCarePlan.Shared.Security;

namespace MobileCarePlan.Api.Controllers
{
    [Route("/care-plans")]
    public class CarePlansController : ControllerBase
    {
        private readonly IMobileCarePlanService mobileCarePlanService;

        public CarePlansController(IMobileCarePlanService mobileCarePlanService)
        {
            this.mobileCarePlanService = mobileCarePlanService;
        }

        [HttpGet]
        [Route("/care-plans/{serviceUserId}")]
        public async Task<IActionResult> GetCarePlan([FromRoute] Guid serviceUserId, CancellationToken cancellationToken = default)
        {
            var carerId = HttpContext.User.GetUserId().Value;
            var cp = await mobileCarePlanService.GetCarePlanAsync(serviceUserId, carerId, cancellationToken);
            if (cp is null)
            {
                return NotFound();
            }

            return Ok(cp);
        }

        [HttpGet]
        [Route("/care-plans/{serviceUserId}/last-update")]
        public async Task<IActionResult> GetCarePlanLastUpdate([FromRoute] Guid serviceUserId, CancellationToken cancellationToken = default)
        {
            var carerId = HttpContext.User.GetUserId().Value;
            var lastUpdate = await mobileCarePlanService.GetCarePlanLastUpdateAsync(serviceUserId, carerId, cancellationToken);
            if (lastUpdate is null)
            {
                return NotFound();
            }

            return Ok(lastUpdate);
        }
    }
}
