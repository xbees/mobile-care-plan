﻿namespace MobileCarePlan.Api
{
    public class ApiSettings
    {
        // Api name (see WebApplicationBuilderExtensions.cs)
        public string Name { get; set; }
        // Api version (see WebApplicationBuilderExtensions.cs)
        public string Version { get; set; }
        public bool SwaggerEnabled { get; set; }
        public string HealthEndpointPath { get; set; }
    }
}
