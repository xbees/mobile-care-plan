﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using MobileCarePlan.Data;
using MobileCarePlan.Service;

namespace MobileCarePlan.Api.Setup
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection SetupServices(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddSingleton<IMobileCarePlanService, MobileCarePlanService>()
                .AddSingleton<ICarerReviewService, CarerReviewService>();
        }

        public static IServiceCollection SetupRepositories(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            return serviceCollection
                .AddSingleton<IMobileCarePlanPublishedEventRepository, MobileCarePlanPublishedEventRepository>()
                .AddSingleton<IMobileCarePlanRepository, MobileCarePlanRepository>()
                .AddSingleton<ICarerReviewRepository, CarerReviewRepository>()
                .Configure<StorageSettings>(configuration.GetSection("Storage"))
                .AddSingleton<IDynamoDBContext, DynamoDBContext>()
                .AddAWSService<IAmazonDynamoDB>();
        }
    }
}
