﻿using MobileCarePlan.Shared.Security;

namespace MobileCarePlan.Api.Setup.Middlewares
{
    public class AuthMiddleware
    {
        private readonly ILogger<AuthMiddleware> logger;
        private readonly RequestDelegate next;

        public AuthMiddleware(ILogger<AuthMiddleware> logger, RequestDelegate next)
        {
            this.logger = logger;
            this.next = next;
        }

        public Task Invoke(HttpContext context)
        {
            var branchId = context.User.GetBranchId();
            var userId = context.User.GetUserId();
            var userType = context.User.GetUserType();

            using (logger.BeginScope("BranchId: {BranchId}, UserId: {UserId}, UserType: {UserType}", branchId, userId, userType))
            {
                return next(context);
            }
        }
    }
}
