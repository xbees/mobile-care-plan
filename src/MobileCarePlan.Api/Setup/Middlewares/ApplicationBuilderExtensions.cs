﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;

namespace MobileCarePlan.Api.Setup.Middlewares
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder SetupHealthChecks(this IApplicationBuilder applicationBuilder, string path)
        {
            return applicationBuilder.UseHealthChecks(
                new PathString(path),
                new HealthCheckOptions { AllowCachingResponses = false });
        }

        public static IApplicationBuilder SetupMiddlewares(this IApplicationBuilder applicationBuilder)
        {
            // Note: The order is important here! The first one to be registered will execute first.
            return applicationBuilder
                .UseMiddleware<AuthMiddleware>()
                .UseMiddleware<CorrelationIdMiddleware>()
                .UseMiddleware<ProblemDetailsMiddleware>();
        }
    }
}
