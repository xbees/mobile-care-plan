﻿namespace MobileCarePlan.Api.Setup.Middlewares
{
    public class CorrelationIdMiddleware
    {
        private readonly ILogger<CorrelationIdMiddleware> logger;
        private readonly RequestDelegate next;

        public CorrelationIdMiddleware(ILogger<CorrelationIdMiddleware> logger, RequestDelegate next)
        {
            this.logger = logger;
            this.next = next;
        }

        public Task Invoke(HttpContext context)
        {
            var correlationId = Guid.NewGuid().ToString();

            using (logger.BeginScope("TraceId: {TraceId}, CorrelationId: {CorrelationId}, Url: {Url}", context.TraceIdentifier, correlationId, context.Request.Path))
            {
                return next(context);
            }
        }
    }
}
