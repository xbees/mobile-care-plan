﻿using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace MobileCarePlan.Api.Setup.Middlewares
{
    public class ProblemDetailsMiddleware
    {
        private readonly ILogger<ProblemDetailsMiddleware> logger;
        private readonly RequestDelegate next;

        public ProblemDetailsMiddleware(ILogger<ProblemDetailsMiddleware> logger, RequestDelegate next)
        {
            this.logger = logger;
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                logger.LogError("An unhandled error occurred.", ex);

                var pd = new ProblemDetails()
                {
                    Status = (int)HttpStatusCode.InternalServerError,
                    Title = "An unhandled error occurred.",
                    Detail = "Please report this problem to an admin."
                };

                pd.Extensions.Add("TraceId", context.TraceIdentifier);

                var res = new ObjectResult(pd);
                await res.ExecuteResultAsync(new ActionContext { HttpContext = context });
            }
        }
    }

}
