﻿using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MobileCarePlan.Api.Authorization;

namespace MobileCarePlan.Api.Setup.Security
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection SetupAuth(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            var jwtSettings = configuration.GetSection("Jwt").Get<JwtSettings>();

            serviceCollection
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(o =>
                {
                    var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSettings.Key));
                    o.SaveToken = true;
                    o.RequireHttpsMetadata = false;
                    o.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = key,
                        ValidateIssuer = true,
                        ValidIssuer = jwtSettings.Issuer,
                        ValidateAudience = false,
                        ValidateLifetime = true
                    };

                    o.MapInboundClaims = true;
                });

            serviceCollection
                .AddAuthorization(o =>
                {
                    // Endpoints in this api can only be called by carers
                    o.AddPolicy(Policies.CarerOnlyPolicyName, o => o.RequireClaim("user_type", "Carer"));
                });

            return serviceCollection;
        }

        public static IServiceCollection SetupSwagger(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddEndpointsApiExplorer()
                .AddSwaggerGen(o =>
                {
                    o.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    {
                        Description = "Input the JWT bearer token.",
                        Type = SecuritySchemeType.Http,
                        In = ParameterLocation.Header,
                        Name = "Authorization",
                        Scheme = "Bearer"
                    });

                    o.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                        [CreateOpenApiSecuritySchemeRequirement()] = new List<string>(),
                    });
                });

            return serviceCollection;

            static OpenApiSecurityScheme CreateOpenApiSecuritySchemeRequirement()
            {
                return new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    },
                    Scheme = "oauth2",
                    Name = "Bearer",
                    In = ParameterLocation.Header
                };
            }
        }
    }
}
