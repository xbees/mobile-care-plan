﻿namespace MobileCarePlan.Api.Setup.Security
{
    public class JwtSettings
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
    }
}
