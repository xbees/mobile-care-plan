using System.Text.Json.Serialization;
using Microsoft.Extensions.Options;
using MobileCarePlan.Api.Authorization;
using MobileCarePlan.Api.Instrumentation;
using MobileCarePlan.Api.Setup;
using MobileCarePlan.Api.Setup.Logging;
using MobileCarePlan.Api.Setup.Middlewares;
using MobileCarePlan.Api.Setup.Security;

namespace MobileCarePlan.Api;

public partial class Program
{
    public static void Main(string[] args)
    {
        try
        {
            var builder = WebApplication.CreateBuilder(args);
            builder.ConfigureApiVersion();

            var logSettings = builder.Configuration.GetSection(ApiConfigSectionNames.Logs).Get<LoggingSettings>();
            SerilogLoggerBootstrap.Init(logSettings);

            var logger = SerilogLoggerBootstrap.CreateLogger<Program>();

            logger.LogInformation("Retrieving settings");

            builder.Services
                .AddControllers()
                .AddJsonOptions(opts => opts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

            builder.Services.AddHealthChecks();

            builder.Services
                .Configure<ApiSettings>(builder.Configuration.GetSection(ApiConfigSectionNames.Api))
                .SetupTelemetry(builder.Configuration)
                .SetupLogging(SerilogLoggerBootstrap.SerilogLogger)
                .SetupSwagger()
                .SetupAuth(builder.Configuration);

            builder.Services
                .SetupServices()
                .SetupRepositories(builder.Configuration);

            var app = builder.Build();

            var apiSettings = app.Services.GetRequiredService<IOptions<ApiSettings>>().Value;

            // Configure the HTTP request pipeline.
            if (apiSettings.SwaggerEnabled)
            {
                app.UseSwagger();
                app.UseSwaggerUI(o =>
                {
                    o.EnablePersistAuthorization();
                    o.EnableTryItOutByDefault();
                });
            }

            app
                .UseAuthentication()
                .UseAuthorization();

            app.MapControllers().RequireAuthorization(Policies.CarerOnlyPolicyName);

            app
                .SetupHealthChecks(apiSettings.HealthEndpointPath)
                .SetupMiddlewares();

            app.Run();
        }
        finally
        {
            SerilogLoggerBootstrap.Close();
        }
    }
}