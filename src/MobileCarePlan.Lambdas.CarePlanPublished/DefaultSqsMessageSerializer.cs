﻿using Amazon.Lambda.SQSEvents;
using Newtonsoft.Json;

namespace MobileCarePlan.Lambdas.CarePlanPublished
{
    public static class DefaultSqsMessageSerializer
    {
        public static TEvent Deserialize<TEvent>(SQSEvent.SQSMessage sqsMessage)
        {
            if (sqsMessage is null)
            {
                return default;
            }

            try
            {
                return JsonConvert.DeserializeObject<TEvent>(sqsMessage.Body);
            }
            catch (Exception ex)
            {
                throw new JsonSerializationException("Could not deserialize message.", ex);
            }
        }
    }
}
