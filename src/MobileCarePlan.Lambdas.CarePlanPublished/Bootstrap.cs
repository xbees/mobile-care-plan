﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MobileCarePlan.Lambdas.CarePlanPublished.Handler;
using MobileCarePlan.Service.Extensions;

namespace MobileCarePlan.Lambdas.CarePlanPublished
{
    public sealed class Bootstrap : IDisposable
    {
        private readonly ServiceProvider serviceProvider;

        public Bootstrap(Action<ConfigurationBuilder> configurationBuilderAction = null)
        {
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddEnvironmentVariables();
            configurationBuilderAction?.Invoke(configurationBuilder);
            var configuration = configurationBuilder.Build();

            var serviceCollection = new ServiceCollection();

            serviceCollection
                .AddScoped<CarePlanPublishedEventHandler>()
                .RegisterServicesScoped(configuration)
                .RegisterDynamoDB(configuration);

            serviceProvider = serviceCollection.BuildServiceProvider();
        }
        public CarePlanPublishedEventHandler CreateHandler()
            => serviceProvider.GetRequiredService<CarePlanPublishedEventHandler>();

        public void Dispose() => (serviceProvider as IDisposable)?.Dispose();
    }
}
