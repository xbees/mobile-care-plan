﻿using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using MobileCarePlan.Lambdas.CarePlanPublished.Extensions;
using MobileCarePlan.Service.CarePlanEventContracts;
using Newtonsoft.Json;

namespace MobileCarePlan.Lambdas.CarePlanPublished
{
    public class CarePlanPublishedV1Function
    {
        private readonly Bootstrap bootstrap;

        public CarePlanPublishedV1Function()
        {
            bootstrap = new Bootstrap();
        }

        public async Task HandleAsync(SQSEvent sqsEvent, ILambdaContext context)
        {
            context.LogInfo($"Received event. Records: {sqsEvent.Records?.Count}.");

            foreach (var record in sqsEvent.Records)
            {
                context.LogInfo($"Received message. Body: {record.Body}");

                try
                {
                    var @event = DefaultSqsMessageSerializer.Deserialize<CareplanPublishedV1>(record);

                    var eventHandler = bootstrap.CreateHandler();
                    await eventHandler.HandleAsync(@event).ConfigureAwait(false);
                }
                catch (JsonSerializationException ex)
                {
                    context.LogError("Error deserializing!", ex);
                    throw;
                }
                catch (Exception ex)
                {
                    context.LogError("An unhandled error occurred!", ex);
                    throw;
                }
            }
        }
    }
}