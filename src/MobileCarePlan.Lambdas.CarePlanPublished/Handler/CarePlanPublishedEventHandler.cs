﻿using MobileCarePlan.Service;
using MobileCarePlan.Service.CarePlanEventContracts;

namespace MobileCarePlan.Lambdas.CarePlanPublished.Handler
{
    public class CarePlanPublishedEventHandler
    {
        private readonly IMobileCarePlanService mobileCarePlanService;

        public CarePlanPublishedEventHandler(IMobileCarePlanService mobileCarePlanService)
        {
            this.mobileCarePlanService = mobileCarePlanService;
        }

        public async Task HandleAsync(CareplanPublishedV1 message, CancellationToken cancellationToken = default)
            => await mobileCarePlanService.UpdateCarePlanAsync(message, cancellationToken).ConfigureAwait(false);
    }
}
