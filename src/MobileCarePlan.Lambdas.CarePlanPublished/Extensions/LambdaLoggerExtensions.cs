﻿using Amazon.Lambda.Core;

namespace MobileCarePlan.Lambdas.CarePlanPublished.Extensions
{
    internal static class LambdaContextExtensions
    {
        public static void LogInfo(this ILambdaContext context, string message, Exception exception = null)
            => context.Log("INFO", message, exception);

        public static void LogError(this ILambdaContext context, string message, Exception exception = null)
            => context.Log("ERROR", message, exception);

        private static void Log(this ILambdaContext context, string level, string message, Exception exception)
        {
            var msg = $"{level} [{context.AwsRequestId}] {(message + (exception != null ? $"\\n{exception}" : null)).Replace("\n", "\\n")}";
            context.Logger.LogLine(msg);
        }
    }
}
