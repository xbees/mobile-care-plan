﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileCarePlan.Shared.Security
{
    public sealed class ClaimNames
    {
        public const string BranchId = "branch_guid";
        public const string UserId = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        public const string UserType = "user_type";
    }
}
