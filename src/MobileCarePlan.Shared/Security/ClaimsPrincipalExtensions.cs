﻿using System.Security.Claims;

namespace MobileCarePlan.Shared.Security
{
    public static class ClaimsPrincipalExtensions
    {
        public static Guid? GetBranchId(this ClaimsPrincipal claimsPrincipal)
        {
            var c = claimsPrincipal?.Claims?.FirstOrDefault(c => string.Equals(ClaimNames.BranchId, c.Type, StringComparison.OrdinalIgnoreCase));
            if (Guid.TryParse(c?.Value, out var branchId))
            {
                return branchId;
            }

            return null;
        }

        public static Guid? GetUserId(this ClaimsPrincipal claimsPrincipal)
        {
            var c = claimsPrincipal?.Claims?.FirstOrDefault(c => string.Equals(ClaimNames.UserId, c.Type, StringComparison.OrdinalIgnoreCase));
            if (Guid.TryParse(c?.Value, out var userId))
            {
                return userId;
            }

            return null;
        }

        public static string GetUserType(this ClaimsPrincipal claimsPrincipal)
        {
            var c = claimsPrincipal?.Claims?.FirstOrDefault(c => string.Equals(ClaimNames.UserType, c.Type, StringComparison.OrdinalIgnoreCase));

            return c?.Value;
        }
    }
}
