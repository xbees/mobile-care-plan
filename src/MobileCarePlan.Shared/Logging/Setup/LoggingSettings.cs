﻿using Microsoft.Extensions.Logging;

namespace MobileCarePlan.Api.Setup.Logging
{
    public class LoggingSettings
    {
        public LogLevel Level { get; set; }
    }
}
