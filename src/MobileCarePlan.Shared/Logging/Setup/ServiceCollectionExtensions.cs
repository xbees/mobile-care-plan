﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace MobileCarePlan.Api.Setup.Logging
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection SetupLogging(this IServiceCollection serviceCollection, Serilog.ILogger logger)
        {
            return serviceCollection
                .AddLogging(lb =>
                {
                    lb.ClearProviders();
                    lb.AddSerilog(logger, false);
                });
        }
    }
}
