﻿using Microsoft.Extensions.Logging;
using Serilog.Events;

namespace MobileCarePlan.Api.Setup.Logging
{
    public static class LogLevelExtensions
    {
        public static LogEventLevel ToEventLogLevel(this LogLevel logLevel)
            => (LogEventLevel)logLevel;
    }
}
