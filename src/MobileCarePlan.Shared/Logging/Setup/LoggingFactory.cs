﻿using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Formatting.Compact;

namespace MobileCarePlan.Api.Setup.Logging
{
    public static class SerilogLoggerBootstrap
    {
        private static readonly object instanceSync = new();

        private static Serilog.Extensions.Logging.SerilogLoggerFactory serilogLoggerFactory;
        private static Serilog.ILogger serilogLogger;

        public static void Init(LoggingSettings logSettings)
        {
            if (serilogLoggerFactory is not null)
            {
                return;
            }

            lock (instanceSync)
            {
                if (serilogLogger is not null)
                {
                    return;
                }

                serilogLogger = new LoggerConfiguration()
                    .Enrich.FromLogContext()
                    .Enrich.With<LE>()
                    .WriteTo.Console(restrictedToMinimumLevel: logSettings.Level.ToEventLogLevel(), formatter: new CompactJsonFormatter())
                    .Filter.ByExcluding(le => le.Properties["SourceContext"].ToString().Contains("Microsoft"))
                    .CreateLogger();

                serilogLoggerFactory = new Serilog.Extensions.Logging.SerilogLoggerFactory(serilogLogger);
            }
        }

        public static Serilog.ILogger SerilogLogger => serilogLogger;

        public static void Close()
        {
            try
            {
                serilogLoggerFactory?.Dispose();
            }
            catch { }

            serilogLogger = null;

            try
            {
                Log.CloseAndFlush();
            }
            catch { }
        }

        public static Microsoft.Extensions.Logging.ILogger CreateLogger<TContext>()
            => serilogLoggerFactory.CreateLogger<TContext>();

        private class LE : ILogEventEnricher
        {
            public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
            {
                var p = propertyFactory.CreateProperty("LogLevel", logEvent.Level);
                logEvent.AddPropertyIfAbsent(p);
            }
        }
    }
}
