﻿using System.Globalization;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Microsoft.Extensions.Options;
using MobileCarePlan.Data.Models;

namespace MobileCarePlan.Data
{
    public class CarerReviewRepository : ICarerReviewRepository
    {
        private readonly IAmazonDynamoDB _amazonDynamoDb;

        private readonly string _carerReviewsTableName;

        public CarerReviewRepository(IOptions<StorageSettings> mobileCarePlanSettings, IAmazonDynamoDB amazonDynamoDb)
        {
            _carerReviewsTableName = mobileCarePlanSettings.Value.CarerReviewsTableName;
            _amazonDynamoDb = amazonDynamoDb;
        }

        public async Task<CarerReviewData> GetAsync(Guid carerId, Guid serviceUserId, CancellationToken cancellationToken = default)
        {
            var result = await _amazonDynamoDb.GetItemAsync(
                _carerReviewsTableName,
                new Dictionary<string, AttributeValue>
                {
                    { "CarerId", new AttributeValue(carerId.ToString()) },
                    { "ServiceUserId", new AttributeValue(serviceUserId.ToString()) }
                },
                cancellationToken
             ).ConfigureAwait(false);

            if (result == null || result.Item == null)
                return null;

            var keys = new string[] { "CarerId", "ServiceUserId" };
            var sectionReviews = result.Item?
                .Where(x => !keys.Contains(x.Key) && DateTime.TryParse(x.Value.S, out var sectionReviewDateTime))
                .ToDictionary(x => x.Key, x => DateTime.Parse(x.Value.S));

            return new CarerReviewData(carerId, serviceUserId, sectionReviews);
        }

        public async Task UpdateAsync(Guid carerId, Guid serviceUserId, string sectionId, DateTime reviewedAt, CancellationToken cancellationToken = default)
        {
            var req = Requests.Update(_carerReviewsTableName, carerId, serviceUserId, sectionId, reviewedAt);
            await _amazonDynamoDb.UpdateItemAsync(req, cancellationToken).ConfigureAwait(false);
        }

        private static class Requests
        {
            public static UpdateItemRequest Update(string tableName, Guid carerId, Guid serviceUserId, string sectionId, DateTime reviewedAt)
            {
                return new UpdateItemRequest
                {
                    TableName = tableName,
                    Key = new Dictionary<string, AttributeValue>
                    {
                        ["CarerId"] = new AttributeValue(carerId.ToString()),
                        ["ServiceUserId"] = new AttributeValue(serviceUserId.ToString())
                    },
                    ReturnValues = ReturnValue.NONE,
                    UpdateExpression = "SET #SectionId = :ReviewedAt",
                    ExpressionAttributeNames = new Dictionary<string, string>
                    {
                        ["#SectionId"] = sectionId
                    },
                    ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                    {
                        [":ReviewedAt"] = new AttributeValue { S = reviewedAt.ToUniversalTime().ToString("o") }
                    }
                };
            }
        }
    }
}
