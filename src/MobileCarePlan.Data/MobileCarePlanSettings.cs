﻿namespace MobileCarePlan.Data
{
    public class StorageSettings
    {
        public string CarePlanEventsTableName { get; set; }
        public string CarePlansTableName { get; set; }
        public string CarerReviewsTableName { get; set; }
    }
}