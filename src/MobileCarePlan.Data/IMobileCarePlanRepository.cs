﻿using MobileCarePlan.Data.Models;

namespace MobileCarePlan.Data
{
    public interface IMobileCarePlanRepository
    {
        Task<CarePlanData> GetAsync(Guid serviceUserId, CancellationToken cancellationToken = default);
        Task SaveAsync(CarePlanData carePlan, CancellationToken cancellationToken = default);
    }
}
