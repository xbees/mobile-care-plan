﻿using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace MobileCarePlan.Data.Models
{
    internal class CarePlanSectionsDynamoDataConverter : IPropertyConverter
    {
        public object FromEntry(DynamoDBEntry dynamoDBEntry) => dynamoDBEntry.AsListOfDocument().Select(entry => entry.ToSectionData()).ToArray();

        public DynamoDBEntry ToEntry(object value)
        {
            var rules = value as IEnumerable<CarePlanSectionItem>;
            if (rules == null || !rules.Any())
            {
                return null;
            }

            return rules.Select(rule => rule.ToDocument()).ToList();
        }
    }

    internal static class DynamoDBEntryExtensions
    {
        public static CarePlanSectionItem ToSectionData(this Document document)
        {
            var section = new CarePlanSectionItem();

            if (document.TryGetValue(nameof(section.Id), out var id))
            {
                section.Id = id.AsString();
            }

            if (document.TryGetValue(nameof(section.Title), out var title))
            {
                section.Title = title.AsString();
            }

            if (document.TryGetValue(nameof(section.Subtitle), out var subtitle))
            {
                section.Subtitle = subtitle.AsString();
            }

            if (document.TryGetValue(nameof(section.Category), out var category))
            {
                section.Category = category.AsString();
            }

            if (document.TryGetValue(nameof(section.SortOrder), out var sortOrder))
            {
                section.SortOrder = sortOrder.AsInt();
            }

            if (document.TryGetValue(nameof(section.LastUpdatedAt), out var sectionLastUpdatedAt))
            {
                section.LastUpdatedAt = sectionLastUpdatedAt.AsDateTime();
            }

            if (document.TryGetValue(nameof(section.Contents), out var contents))
            {
                section.Contents = contents.AsListOfDocument().Select(doc =>
                {
                    var item = new CarePlanSectionContentItem();

                    if (doc.TryGetValue(nameof(item.Id), out var id))
                    {
                        item.Id = id.AsString();
                    }

                    if (doc.TryGetValue(nameof(item.Title), out var title))
                    {
                        item.Title = title.AsString();
                    }

                    if (doc.TryGetValue(nameof(item.Content), out var content))
                    {
                        item.Content = content.AsString();
                    }

                    if (doc.TryGetValue(nameof(item.Type), out var type))
                    {
                        item.Type = type.AsString();
                    }

                    if (doc.TryGetValue(nameof(item.SortOrder), out var sortOrder))
                    {
                        item.SortOrder = sortOrder.AsUInt();
                    }

                    if (doc.TryGetValue(nameof(item.LastUpdatedAt), out var lastUpdatedAt))
                    {
                        item.LastUpdatedAt = lastUpdatedAt.AsDateTime();
                    }

                    return item;
                }).ToArray();
            }

            return section;
        }

        public static Document ToDocument(this CarePlanSectionItem section)
        {
            var document = new Document();

            if (section.Id != null)
            {
                document.Add(nameof(section.Id), section.Id);
            }

            if (section.Title != null)
            {
                document.Add(nameof(section.Title), section.Title);
            }

            if (section.Subtitle != null)
            {
                document.Add(nameof(section.Subtitle), section.Subtitle);
            }

            if (section.Category != null)
            {
                document.Add(nameof(section.Category), section.Category);
            }

            if (section.LastUpdatedAt.HasValue)
            {
                document.Add(nameof(section.LastUpdatedAt), section.LastUpdatedAt);
            }

            document.Add(nameof(section.SortOrder), section.SortOrder);

            if (section.Contents?.Any() == true)
            {
                document.Add(nameof(section.Contents), section.Contents.Select(content =>
                {
                    var doc = new Document();
                    if (content.Id != null)
                    {
                        doc.Add(nameof(content.Id), content.Id);
                    }

                    if (content.Title != null)
                    {
                        doc.Add(nameof(content.Title), content.Title);
                    }

                    if (content.Content != null)
                    {
                        doc.Add(nameof(content.Content), content.Content);
                    }

                    if (content.Type != null)
                    {
                        doc.Add(nameof(content.Type), content.Type);
                    }

                    if (content.LastUpdatedAt.HasValue)
                    {
                        doc.Add(nameof(content.LastUpdatedAt), content.LastUpdatedAt);
                    }

                    doc.Add(nameof(content.SortOrder), content.SortOrder);

                    return doc;
                }).ToList());
            }

            return document;
        }
    }
}
