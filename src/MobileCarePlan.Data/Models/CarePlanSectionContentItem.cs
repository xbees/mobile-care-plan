﻿namespace MobileCarePlan.Data.Models
{
    public class CarePlanSectionContentItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Type { get; set; }
        public ulong SortOrder { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
    }
}
