﻿namespace MobileCarePlan.Data.Models
{
    public class CarePlanRequirements
    {
        public string PersonalCare { get; set; }
        public string Mobility { get; set; }
        public string HouseKeeping { get; set; }
        public string MedicationSupport { get; set; }
        public string NutritionHydration { get; set; }
        public string Shopping { get; set; }
        public string FinantialTransactions { get; set; }
        public string SocialActivities { get; set; }
        public string ContinenceCare { get; set; }
        public string Oxygen { get; set; }
    }
}
