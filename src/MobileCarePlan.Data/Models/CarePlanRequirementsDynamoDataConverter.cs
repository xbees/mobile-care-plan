﻿using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace MobileCarePlan.Data.Models
{
    internal class CarePlanRequirementsDynamoDataConverter : IPropertyConverter
    {
        public object FromEntry(DynamoDBEntry entry)
        {
            var requirements = new CarePlanRequirements();

            var document = entry.AsDocument();
            if (document.TryGetValue(nameof(requirements.PersonalCare), out var personalCare))
                requirements.PersonalCare = personalCare.AsString();

            if (document.TryGetValue(nameof(requirements.Mobility), out var mobility))
                requirements.Mobility = mobility.AsString();

            if (document.TryGetValue(nameof(requirements.HouseKeeping), out var houseKeeping))
                requirements.HouseKeeping = houseKeeping.AsString();

            if (document.TryGetValue(nameof(requirements.MedicationSupport), out var medicationSupport))
                requirements.MedicationSupport = medicationSupport.AsString();

            if (document.TryGetValue(nameof(requirements.NutritionHydration), out var nutritionHydration))
                requirements.NutritionHydration = nutritionHydration.AsString();

            if (document.TryGetValue(nameof(requirements.Shopping), out var shopping))
                requirements.Shopping = shopping.AsString();

            if (document.TryGetValue(nameof(requirements.FinantialTransactions), out var finantialTransactions))
                requirements.FinantialTransactions = finantialTransactions.AsString();

            if (document.TryGetValue(nameof(requirements.SocialActivities), out var socialActivities))
                requirements.SocialActivities = socialActivities.AsString();

            if (document.TryGetValue(nameof(requirements.ContinenceCare), out var continenceCare))
                requirements.ContinenceCare = continenceCare.AsString();

            if (document.TryGetValue(nameof(requirements.Oxygen), out var oxygen))
                requirements.Oxygen = oxygen.AsString();

            return requirements;
        }

        public DynamoDBEntry ToEntry(object value)
        {
            var requirements = value as CarePlanRequirements;
            if (requirements == null) return null;

            var document = new Document();

            if (requirements.PersonalCare != null)
                document.Add(nameof(requirements.PersonalCare), requirements.PersonalCare);

            if (requirements.Mobility != null)
                document.Add(nameof(requirements.Mobility), requirements.Mobility);

            if (requirements.HouseKeeping != null)
                document.Add(nameof(requirements.HouseKeeping), requirements.HouseKeeping);

            if (requirements.MedicationSupport != null)
                document.Add(nameof(requirements.MedicationSupport), requirements.MedicationSupport);

            if (requirements.NutritionHydration != null)
                document.Add(nameof(requirements.NutritionHydration), requirements.NutritionHydration);

            if (requirements.Shopping != null)
                document.Add(nameof(requirements.Shopping), requirements.Shopping);

            if (requirements.FinantialTransactions != null)
                document.Add(nameof(requirements.FinantialTransactions), requirements.FinantialTransactions);

            if (requirements.SocialActivities != null)
                document.Add(nameof(requirements.SocialActivities), requirements.SocialActivities);

            if (requirements.ContinenceCare != null)
                document.Add(nameof(requirements.ContinenceCare), requirements.ContinenceCare);

            if (requirements.Oxygen != null)
                document.Add(nameof(requirements.Oxygen), requirements.Oxygen);

            return document;
        }
    }
}
