﻿namespace MobileCarePlan.Data.Models
{
    public class CarePlanSectionItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Category { get; set; }
        public int SortOrder { get; set; }
        public IEnumerable<CarePlanSectionContentItem> Contents { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
    }
}
