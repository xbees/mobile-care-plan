﻿namespace MobileCarePlan.Data.Models
{
    public class CarerReviewData
    {
        public Guid CarerId { get; }

        public Guid ServiceUserId { get; }

        public Dictionary<string, DateTime> SectionReviews { get; }

        public CarerReviewData(Guid carerId, Guid serviceUserId, Dictionary<string, DateTime> sectionReviews = null)
        {
            CarerId = carerId;
            ServiceUserId = serviceUserId;
            SectionReviews = sectionReviews ?? new Dictionary<string, DateTime>();
        }
    }
}
