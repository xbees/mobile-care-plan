﻿using Amazon.DynamoDBv2.DataModel;

namespace MobileCarePlan.Data.Models
{
    public class CarePlanData
    {
        [DynamoDBHashKey]
        public Guid ServiceUserId { get; set; }

        [DynamoDBGlobalSecondaryIndexHashKey]
        public DateTime DateTime { get; set; }

        public Guid LastEventId { get; set; }

        public DateTime LastUpdatedAt { get; set; }

        public Guid LastUpdatedById { get; set; }

        public string LastUpdatedByName { get; set; }

        [DynamoDBProperty(typeof(CarePlanRequirementsDynamoDataConverter))]
        public CarePlanRequirements Requirements { get; set; }

        [DynamoDBProperty(typeof(CarePlanSectionsDynamoDataConverter))]
        public IEnumerable<CarePlanSectionItem> Sections { get; set; }
    }
}
