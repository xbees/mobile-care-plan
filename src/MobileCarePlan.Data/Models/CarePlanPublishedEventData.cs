﻿namespace MobileCarePlan.Data.Models
{
    public class CarePlanPublishedEventData
    {
        public Guid ServiceUserId { get; set; }
        public DateTime DateTime { get; set; }
        public Guid EventId { get; set; }
        public Guid CreatedBy { get; set; }
        public string Event { get; set; }
    }
}
