﻿using Amazon.DynamoDBv2.DataModel;
using Microsoft.Extensions.Options;
using MobileCarePlan.Data.Models;

namespace MobileCarePlan.Data
{
    public class MobileCarePlanPublishedEventRepository : IMobileCarePlanPublishedEventRepository
    {
        private readonly IDynamoDBContext _dynamoDBContext;
        private readonly DynamoDBOperationConfig _operationConfig;

        public MobileCarePlanPublishedEventRepository(IOptions<StorageSettings> mobileCarePlanSettings, IDynamoDBContext dynamoDBContext)
        {
            _operationConfig = new DynamoDBOperationConfig
            {
                OverrideTableName = mobileCarePlanSettings.Value.CarePlanEventsTableName,
                IgnoreNullValues = true
            };
            _dynamoDBContext = dynamoDBContext;
        }

        public async Task SaveAsync(CarePlanPublishedEventData carePlanPublishEvent, CancellationToken cancellationToken)
        {
            await _dynamoDBContext.SaveAsync(carePlanPublishEvent, _operationConfig, cancellationToken).ConfigureAwait(false);
        }
    }
}
