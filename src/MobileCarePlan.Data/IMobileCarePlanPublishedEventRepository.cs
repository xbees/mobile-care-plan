﻿using MobileCarePlan.Data.Models;
using System.Threading;
using System.Threading.Tasks;

namespace MobileCarePlan.Data
{
    public interface IMobileCarePlanPublishedEventRepository
    {
        Task SaveAsync(CarePlanPublishedEventData carePlanPublishEvent, CancellationToken cancellationToken = default);
    }
}
