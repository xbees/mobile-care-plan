﻿using Amazon.DynamoDBv2.DataModel;
using Microsoft.Extensions.Options;
using MobileCarePlan.Data.Models;

namespace MobileCarePlan.Data
{
    public class MobileCarePlanRepository : IMobileCarePlanRepository
    {
        private readonly IDynamoDBContext _dynamoDBContext;

        private readonly DynamoDBOperationConfig _operationConfig;

        public MobileCarePlanRepository(IOptions<StorageSettings> mobileCarePlanSettings, IDynamoDBContext dynamoDBContext)
        {
            _operationConfig = new DynamoDBOperationConfig
            {
                OverrideTableName = mobileCarePlanSettings.Value.CarePlansTableName,
                IgnoreNullValues = true
            };

            _dynamoDBContext = dynamoDBContext;
        }

        public Task<CarePlanData> GetAsync(Guid serviceUserId, CancellationToken cancellationToken = default)
            => _dynamoDBContext.LoadAsync<CarePlanData>(serviceUserId, _operationConfig, cancellationToken);

        public async Task SaveAsync(CarePlanData carePlan, CancellationToken cancellationToken)
            => await _dynamoDBContext.SaveAsync(carePlan, _operationConfig, cancellationToken).ConfigureAwait(false);
    }
}
