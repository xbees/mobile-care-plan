﻿using MobileCarePlan.Data.Models;

namespace MobileCarePlan.Data
{
    public interface ICarerReviewRepository
    {
        Task<CarerReviewData> GetAsync(Guid carerId, Guid serviceUserId, CancellationToken cancellationToken = default);
        Task UpdateAsync(Guid carerId, Guid serviceUserId, string sectionId, DateTime reviewedAt, CancellationToken cancellationToken = default);
    }
}
