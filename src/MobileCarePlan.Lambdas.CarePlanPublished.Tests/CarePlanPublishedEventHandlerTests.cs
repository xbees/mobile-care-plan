﻿using MobileCarePlan.Lambdas.CarePlanPublished.Handler;
using MobileCarePlan.Service;
using MobileCarePlan.Service.CarePlanEventContracts;
using Moq;
using Xunit;

namespace MobileCarePlan.Lambdas.CarePlanPublished.Tests
{
    public class CarePlanPublishedEventHandlerTests
    {
        private readonly Mock<IMobileCarePlanService> mobileCarePlanServiceMock;
        private readonly CarePlanPublishedEventHandler carePlanPublishedEventHandler;

        public CarePlanPublishedEventHandlerTests()
        {
            mobileCarePlanServiceMock = new Mock<IMobileCarePlanService>(MockBehavior.Strict);
            carePlanPublishedEventHandler = new CarePlanPublishedEventHandler(mobileCarePlanServiceMock.Object);
        }

        [Fact]
        public async Task HandleAsync()
        {
            // Arrange
            var @event = new CareplanPublishedV1();

            mobileCarePlanServiceMock.Setup(m => m.UpdateCarePlanAsync(@event, It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(true));

            // Act
            await carePlanPublishedEventHandler.HandleAsync(@event).ConfigureAwait(false);

            // Assert
            mobileCarePlanServiceMock.VerifyAll();
        }
    }
}
