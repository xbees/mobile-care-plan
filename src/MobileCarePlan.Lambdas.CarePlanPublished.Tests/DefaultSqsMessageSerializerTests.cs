﻿using MobileCarePlan.Service.CarePlanEventContracts;
using Xunit;
using static Amazon.Lambda.SQSEvents.SQSEvent;

namespace MobileCarePlan.Lambdas.CarePlanPublished.Tests
{
    public class DefaultSqsMessageSerializerTests
    {
        [Fact]
        public void Deserialize()
        {
            // Arrange
            var message = new SQSMessage
            {
                Body = TestData.Message
            };

            // Act
            var e = DefaultSqsMessageSerializer.Deserialize<CareplanPublishedV1>(message);

            // Assert
            Assert.NotNull(e);
            Assert.Equal("56fd9ed7-f374-4310-8982-4f815c8f0b5d", e.Client.Guid.ToString());
            Assert.Equal("709f5ed5-a2ab-4aad-bc94-8e42938f87d3", e.Guid.ToString());
            Assert.Equal("2023-01-10T16:06:45Z", e.PublishedAt.ToString("yyyy-MM-ddTHH:mm:ssZ"));
            Assert.Equal("383044e8-8ebd-43e1-9075-3dd5350f9289", e.PublishedBy.Guid.ToString());
        }
    }
}
