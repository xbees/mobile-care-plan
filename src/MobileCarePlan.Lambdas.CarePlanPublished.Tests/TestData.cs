﻿namespace MobileCarePlan.Lambdas.CarePlanPublished.Tests
{
    internal static class TestData
    {
        public const string Message = @"
            {
              'client': {
                'first_name': 'Edward',
                'guid': '56fd9ed7-f374-4310-8982-4f815c8f0b5d',
                'last_name': 'ApiComplete'
              },
              'guid': '709f5ed5-a2ab-4aad-bc94-8e42938f87d3',
              'package_type': 'LiveIn',
              'published_at': '2023-01-10T16:06:45Z',
              'published_by': {
                'guid': '383044e8-8ebd-43e1-9075-3dd5350f9289',
                'name': 'ct George admin1'
              },
              'sections': {
                'activities_and_financial_support': {
                  'data': {
                    'finances': {
                      'control_measures': 'Harry Hamster\'s control measures',
                      'hazard': 'some details test',
                      'initial_risk': 'Low',
                      'residual_risk': 'Low'
                    },
                    'finances_further_instructions': 'some details test',
                    'shopping': {
                      'control_measures': 'some details test',
                      'hazard': 'some details test',
                      'initial_risk': 'Low',
                      'residual_risk': 'Low'
                    },
                    'social': {
                      'control_measures': 'some details test',
                      'hazard': 'some details test',
                      'initial_risk': 'Low',
                      'residual_risk': 'Low'
                    },
                    'support_with_finances': 'some details test',
                    'support_with_shopping': 'some details test',
                    'support_with_social': 'some details test',
                    'who_supports_su_with_housekeeping': 'ferferf'
                  },
                  'description': 'Risk assessment',
                  'guid': '30fc4dd7-e2f9-6eaa-b43d-30cb995c830c',
                  'is_completed': true,
                  'title': 'Activities and financial support'
                },
                'biography': {
                  'data': {
                    'interests_and_hobbies': 'some random interests ',
                    'life_story': 'a very long life history'
                  },
                  'description': 'Life history, expectations, interests and hobbies',
                  'guid': 'a0a4795e-0fbe-8ddf-91ce-0dbd64466ea7',
                  'is_completed': true,
                  'title': 'Biography'
                },
                'communications_and_accessibility': {
                  'data': {
                    'first_language': {
                      'code': 'GERMAN',
                      'name': 'German'
                    },
                    'glasses': {
                      'details': 'Test String ',
                      'support_required': true
                    },
                    'hearing_aid': {
                      'details': 'Test support',
                      'support_required': true
                    },
                    'learning_disabilities': 'Test support',
                    'preferred_comms': 'DirectlyWithServiceUser',
                    'preferred_comms_format': 'Verbal'
                  },
                  'description': 'Preferred method of communication',
                  'guid': 'b6e6f225-2d7d-c6ad-6007-a0ba4f7778f5',
                  'is_completed': true,
                  'title': 'Communication and accessibility requirements'
                },
                'continence': {
                  'data': {
                    'catheter': {
                      'control_measures': 'catheter control',
                      'details': 'catheter details',
                      'hazard': 'catheter hazard',
                      'initial_risk': 'High',
                      'residual_risk': 'High'
                    },
                    'continence': {
                      'control_measures': 'tyhty',
                      'hazard': 'htyhyth',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Medium'
                    },
                    'stoma': {
                      'stoma_hazard': {
                        'control_measures': 'stoma control',
                        'hazard': 'stoma hazard',
                        'initial_risk': 'High',
                        'residual_risk': 'High'
                      },
                      'stoma_support': 'stoma support',
                      'stoma_type': 'Urostomy'
                    },
                    'su_continence': {
                      'has_continence_aids': true,
                      'su_continent': {
                        'control_measures': 'continence  control',
                        'details': 'Continent details',
                        'hazard': 'continence hazard',
                        'initial_risk': 'High',
                        'residual_risk': 'High'
                      }
                    },
                    'support_with_continence': 'htyhtyh'
                  },
                  'description': 'Risk assessment',
                  'guid': '0f1e63b2-29ce-5ffd-137a-8b66f3e943ef',
                  'is_completed': true,
                  'title': 'Continence (Including catheter & stoma)'
                },
                'daily_routine': {
                  'data': {
                    'food_allegies': [
                      'peanuts',
                      'banana'
                    ],
                    'routines': [
                      {
                        'outcomes': 'Maybe be slimmer',
                        'period': 'Morning',
                        'risks': 'Low risk',
                        'routine': 'Yoga',
                        'self_sufficient': 'Training2',
                        'support_for_transfers': true,
                        'tasks': [
                          {
                            'equipment': 'Ball',
                            'number_of_carers': 10,
                            'safe_system': 'Test safe system',
                            'type': 'InAndOutOfBed'
                          }
                        ]
                      }
                    ]
                  },
                  'description': 'Day to day routine inc food requirements',
                  'guid': 'f8a831c2-bc49-9afa-5a23-31399d0c8cb3',
                  'is_completed': true,
                  'title': 'Daily routine'
                },
                'decisions_around_care': {
                  'data': {
                    'can_make_formal_decisions': true,
                    'can_make_informal_decisions': true,
                    'formal_decision_assistance': {
                      'details': 'Test2',
                      'support_required': true
                    },
                    'mental_capacity_assessment_required': 'NotApplicable'
                  },
                  'description': 'Level of support needed from service user',
                  'guid': '17dda7fe-e878-307c-94e3-5d2922737653',
                  'is_completed': true,
                  'title': 'Decisions around care'
                },
                'health_and_wellbeing': {
                  'data': {
                    'health_conditions_hazards': [
                      {
                        'control_measures': 'String',
                        'details': 'Sciatica',
                        'hazard': 'Hazard test string',
                        'initial_risk': 'Low',
                        'residual_risk': 'Low'
                      },
                      {
                        'control_measures': 'headache control',
                        'details': 'Headache',
                        'hazard': 'sleep',
                        'initial_risk': 'Low',
                        'residual_risk': 'Medium'
                      }
                    ],
                    'medication_allergies': [
                      {
                        'control_measures': 'alergy measures',
                        'details': 'Vitamin-C',
                        'hazard': 'alergy hazard',
                        'initial_risk': 'High',
                        'residual_risk': 'Low'
                      }
                    ]
                  },
                  'description': 'Risk assessment',
                  'guid': '4cecf71b-91fc-8a25-2f80-de79fb427dc0',
                  'is_completed': true,
                  'title': 'Health and wellbeing'
                },
                'health_conditions': {
                  'data': [
                    {
                      'details': 'Mobility - hard to walk and move around',
                      'diagnosis_month': 1,
                      'diagnosis_year': 2021,
                      'name': 'Sciatica',
                      'treatment': 'Regular application of ointment'
                    },
                    {
                      'details': 'Sleep - it\'s hard to sleep',
                      'diagnosis_month': 12,
                      'diagnosis_year': 2022,
                      'name': 'Headache',
                      'treatment': 'treatment'
                    }
                  ],
                  'description': 'Conditions and how we manage them',
                  'guid': '6a9b503d-d243-64c8-a50e-d9baf7798990',
                  'is_completed': true,
                  'title': 'Health conditions'
                },
                'home_and_work_environment': {
                  'data': {
                    'access_risks': {
                      'control_measures': 'Test measures',
                      'details': 'Test details',
                      'hazard': 'Test hazard',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Medium'
                    },
                    'appropriate_lighting': {
                      'control_measures': 'c m',
                      'details': 'light detail',
                      'hazard': 'fwfwefwe',
                      'initial_risk': 'Medium',
                      'is_lighting_appropriated': true,
                      'residual_risk': 'Medium'
                    },
                    'entry_hazard': {
                      'control_measures': 'Phone service user',
                      'details': 'Intercom system',
                      'hazard': 'not working',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Low'
                    },
                    'escape_routes': 'Fire escape to the left of the front door',
                    'fire_alarms': {
                      'control_measures': 'cm fire',
                      'details': 'working fire alarm',
                      'has_fire_alarms': true,
                      'hazard': 'hazard fire',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Medium'
                    },
                    'fire_evacuation': 'Go to the nearest fire exit and meet across the road',
                    'location_of_electricity_meter': 'Cupboard next to the front door',
                    'location_of_fuse_box': 'Cupboard next to the front door',
                    'location_of_gas_meter': 'Outside the front door',
                    'location_of_water_mains': 'Under the kitchen sink',
                    'personal_safety_risks': {
                      'control_measures': 'Test measures',
                      'details': 'Test details',
                      'hazard': 'Test hazard',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Medium'
                    },
                    'pets_in_home': {
                      'control_measures': 'treats',
                      'details': '15 cats',
                      'hazard': 'Trip hazzard',
                      'initial_risk': 'Low',
                      'residual_risk': 'Medium'
                    },
                    'property_type': 'Cottage'
                  },
                  'description': 'Risk assessment',
                  'guid': '381bd523-4faa-112e-d9f2-9c2e7bc5d7dc',
                  'is_completed': true,
                  'title': 'Home and working environment'
                },
                'infection_control': {
                  'data': {
                    'covid_19': {
                      'control_measures': 'something here',
                      'hazard': 'string',
                      'initial_risk': 'Low',
                      'residual_risk': 'High'
                    },
                    'mrsa': {
                      'control_measures': 'NA',
                      'hazard': 'NA',
                      'initial_risk': 'Low',
                      'residual_risk': 'Low'
                    },
                    'norovirus': {
                      'control_measures': 'nothing',
                      'hazard': 'some hazard',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Medium'
                    },
                    'other': {
                      'control_measures': 'test1',
                      'details': 'some other virus',
                      'hazard': 'test2',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Medium'
                    },
                    'seasonal_flu': {
                      'control_measures': 'test measures',
                      'hazard': 'test hazard',
                      'initial_risk': 'Low',
                      'residual_risk': 'Low'
                    }
                  },
                  'description': 'Risk assessment',
                  'guid': '7435053f-5d25-9ffb-3b53-737e293a7acd',
                  'is_completed': true,
                  'title': 'Infection control'
                },
                'key_details': {
                  'data': {
                    'able_to_open_door': true,
                    'alias': 'fgghdghj',
                    'contacts': [
                      {
                        'address': 'sdfghdfsgh dfghssghfd',
                        'name': 'sdfg sdfggdfs dfgs',
                        'phone_number': '234568876',
                        'relation': 'dgh'
                      },
                      {
                        'address': 'Mojš 338',
                        'name': 'Contact name',
                        'phone_number': '123456',
                        'relation': 'Mother'
                      }
                    ],
                    'email': 'edapitest@ceracare.co.uk',
                    'ethnicity': 'Other',
                    'ethnicity_other': 'Saudi',
                    'gender': 'Male',
                    'house_mate': 'Anna',
                    'key_safe_number': '123456',
                    'relationship_status': 'Married',
                    'religion': 'Jewish',
                    'sexual_orientation': 'Heterosexual'
                  },
                  'description': 'Other personal details and key contacts',
                  'guid': 'd5ad90b3-6a4e-617b-5b54-4d847a730ff5',
                  'is_completed': true,
                  'title': 'Details and key contacts'
                },
                'live_in_arrangements': {
                  'data': {
                    'breaks_and_food': 'Five Guysh',
                    'sleeping_and_environmental': 'Waterbed'
                  },
                  'description': 'Breaks, food and arrangements for carers',
                  'guid': '33f0c966-9799-3941-0912-393405a63f31',
                  'is_completed': true,
                  'title': 'Live-in arrangements'
                },
                'medication': {
                  'data': {
                    'blood_thinning_medication': {
                      'control_measures': 'btm measures',
                      'details': 'string',
                      'hazard': 'btm hazard',
                      'initial_risk': 'High',
                      'residual_risk': 'Low'
                    },
                    'covert_medications': {
                      'control_measures': 'cm measures',
                      'details': 'cv details',
                      'hazard': 'cm hazard',
                      'initial_risk': 'High',
                      'residual_risk': 'Medium',
                      'best_interest_inc_gp': true
                    },
                    'homely_remedies': {
                      'control_measures': 'hr measures',
                      'details': 'hr details',
                      'hazard': 'hr hazard',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Low'
                    },
                    'medication_allergies': [
                      {
                        'control_measures': 'alergy measures',
                        'details': 'paracetamol',
                        'hazard': 'alergy hazard',
                        'initial_risk': 'High',
                        'residual_risk': 'Low'
                      }
                    ],
                    'medication_ordered_by': 'Someone',
                    'medication_storage': 'storage',
                    'medication_support': {
                      'control_measures': 'ms measures',
                      'details': 'ms details',
                      'hazard': 'ms hazard',
                      'initial_risk': 'High',
                      'residual_risk': 'High'
                    },
                    'prn_medications': {
                      'control_measures': 'prn measures',
                      'details': 'prn details',
                      'hazard': 'prn hazard',
                      'initial_risk': 'High',
                      'residual_risk': 'Low'
                    },
                    'time_sensitive_medication': {
                      'control_measures': 'tsm measures',
                      'details': 'tsm-details edited',
                      'hazard': 'tsm hazard',
                      'initial_risk': 'High',
                      'residual_risk': 'Medium'
                    }
                  },
                  'description': 'Risk assessment',
                  'guid': 'e0334c2b-c13a-31fc-403c-ed1e5fb1200e',
                  'is_completed': true,
                  'title': 'Medication'
                },
                'nutrition_and_hydration': {
                  'data': {
                    'dentures': {
                      'control_measures': 'some details test',
                      'details': 'dentures some details test',
                      'hazard': 'some details test',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Low'
                    },
                    'difficulty_swallowing': {
                      'control_measures': 'some details test',
                      'details': 'sw some details test',
                      'hazard': 'some details test',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Medium'
                    },
                    'feeding_tube': {
                      'control_measures': 'some details test',
                      'details': 'tube some details test',
                      'hazard': 'some details test',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Medium'
                    },
                    'food_allergies': [
                      {
                        'hazard': {
                          'control_measures': 'test',
                          'hazard': 'test',
                          'initial_risk': 'Low',
                          'residual_risk': 'Low'
                        },
                        'name': 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'
                      }
                    ],
                    'nutrition_and_hydration_support': {
                      'control_measures': 'some details test',
                      'details': 'support some details test2',
                      'hazard': 'some details test',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Medium'
                    },
                    'risk_of_choking': {
                      'control_measures': 'some details test',
                      'details': 'choking some details test',
                      'hazard': 'some details test',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Medium'
                    },
                    'risk_of_dehydration': {
                      'details': 'dehydration details'
                    },
                    'risk_of_malnutrition': {
                      'details': 'malnutrition some details test',
                      'hazard': {
                        'control_measures': 'some details test',
                        'hazard': 'some details test',
                        'initial_risk': 'Medium',
                        'residual_risk': 'Medium'
                      }
                    },
                    'speech_and_language_therapy': {
                      'control_measures': 'some details test',
                      'details': 'language_therapy some details test',
                      'hazard': 'some details test',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Medium'
                    },
                    'support_with_drinking': {
                      'details': 'drinking some details test',
                      'hazard': {
                        'control_measures': 'some details test',
                        'hazard': 'some details test',
                        'initial_risk': 'Medium',
                        'residual_risk': 'Medium'
                      }
                    },
                    'support_with_eating': {
                      'details': 'eating details',
                      'hazard': {
                        'control_measures': 'some details test',
                        'hazard': 'some details test',
                        'initial_risk': 'Medium',
                        'residual_risk': 'Medium'
                      }
                    }
                  },
                  'description': 'Risk assessment',
                  'guid': 'f41aac8f-f533-8e3c-3a04-d018445078eb',
                  'is_completed': true,
                  'title': 'Nutrition and hydration'
                },
                'overall_significant_risks': {
                  'data': {
                    'details': 'Some significant risks details ...'
                  },
                  'description': 'Risk assessment',
                  'guid': '5573656a-fa10-c361-bb42-9164ccdc1715',
                  'is_completed': true,
                  'title': 'Overall significant risks and associated control measures'
                },
                'mobility': {
                  'data': {
                      'ability_to_bear_weight': {
                        'control_measures': 'string',
                        'details': 'string',
                        'hazard': 'string',
                        'initial_risk': 'Low',
                        'residual_risk': 'Low'
                      },
                      'height_in_cm': 165,
                      'in_and_out_of_bed': {
                        'control_measures': 'string',
                        'details': 'string',
                        'hazard': 'string',
                        'initial_risk': 'Low',
                        'residual_risk': 'Low'
                      },
                      'independence_with_dressing': {
                        'control_measures': 'string',
                        'details': 'string',
                        'hazard': 'string',
                        'initial_risk': 'Low',
                        'residual_risk': 'Low'
                      },
                      'other_equipment': [
                        {
                          'concerns': 'string',
                          'date_of_last_inspection': '2023-01-10T05:39:51',
                          'date_of_next_inspection': '2023-01-10T05:39:51',
                          'hazard': {
                            'control_measures': 'string',
                            'hazard': 'string',
                            'initial_risk': 'Low',
                            'residual_risk': 'Low'
                          },
                          'support_su_weight': 'need to support weight',
                          'make_and_model': 'string',
                          'provider': 'string',
                          'type': 'Other equipment type'
                        }
                      ],
                      'shower_or_bath': {
                        'uses_shower_or_bath': true,
                        'control_measures': 'string',
                        'details': 'string',
                        'hazard': 'string',
                        'initial_risk': 'Low',
                        'residual_risk': 'Low'
                      },
                      'transfer_to_and_from_chair': {
                        'control_measures': 'string',
                        'details': 'string',
                        'hazard': 'string',
                        'initial_risk': 'Low',
                        'residual_risk': 'Low'
                      },
                      'transfer_to_toilet': {
                        'control_measures': 'string',
                        'details': 'string',
                        'hazard': 'string',
                        'initial_risk': 'Low',
                        'residual_risk': 'Low'
                      },
                      'weight_in_kgs': 60,
                      'wheel_chair': {
                        'concerns': 'concern',
                        'date_of_last_inspection': '2022-12-17T17:17:56',
                        'date_of_next_inspection': '2023-02-17T17:17:56',
                        'hazard': {
                          'control_measures': 'control',
                          'hazard': 'test hazard',
                          'initial_risk': 'Medium',
                          'residual_risk': 'Medium'
                        },
                        'make_and_model': 'test model',
                        'provider': 'Master'
                      }
                   },
                  'description': 'Risk assessment',
                  'guid': '5573656a-fa10-c361-bb42-9164ccdc1719',
                  'is_completed': true,
                  'title': 'Mobility, moving, handling and falls'
                },
                'oxygen': {
                  'data': {
                    'oxygen_environmental': {
                      'control_measures': 'control measures env',
                      'hazard': 'some hazard',
                      'initial_risk': 'Medium',
                      'residual_risk': 'Low'
                    },
                    'oxygen_support': {
                      'control_measures': 'o2 support c m',
                      'details': 'support details update',
                      'hazard': 'o2 support hazard',
                      'initial_risk': 'Low',
                      'residual_risk': 'Low'
                    }
                  },
                  'description': 'Risk assessment',
                  'guid': '87d92461-3649-3d5e-b2c9-e5b27ca04db6',
                  'is_completed': true,
                  'title': 'Oxygen'
                },
                'personal_details': {
                  'data': {
                    'address': 'London SE1, UK',
                    'advanced_statement': 'DNACPR advanced decision or statement',
                    'authors': [
                      {
                        'name': 'Andre',
                        'relationship': 'Cooworker.'
                      },
                      {
                        'name': 'Matej',
                        'relationship': 'Cooworker'
                      }
                    ],
                    'care_plan_created_by': 'Someone',
                    'date_of_birth': '1973-12-21T00:00:00Z',
                    'dependency': {
                      'level': 'Low',
                      'medication_level': 1,
                      'reason': 'no reason'
                    },
                    'dnacpr_location': 'Derby town.',
                    'effective_from': '2022-12-01T00:00:00Z',
                    'event_of_death': 'nobody',
                    'full_name': 'Edward ApiComplete',
                    'funeral_plan': 'funeral plan1',
                    'gp': {
                      'address': 'Baker Street',
                      'name': 'Dr Watson',
                      'phone_number': '12345678901'
                    },
                    'last_wishes': {
                      'details': 'dfgdfgdfg',
                      'support_details': 'Cera supprt with last wishes',
                      'support_required': true
                    },
                    'national_identity_reference': '12345677777',
                    'next_of_kin': {
                      'address': '452121',
                      'name': 'nextKin',
                      'phone_number': '5465465464',
                      'relation': 'daughter'
                    },
                    'pharmacy': {
                      'name': 'The Groves',
                      'phone_number': '09876543212'
                    },
                    'phone_number': '96454611122',
                    'poa': [
                      'Advocates',
                      'HealthAndWelfare'
                    ],
                    'start_date': '2022-12-11T00:00:00Z'
                  },
                  'description': 'Personal details and advance care planning inc DNACPR, next of kin and POA',
                  'guid': '872c5ffe-4a9a-a9b1-1575-31914ca41d94',
                  'is_completed': true,
                  'title': 'Personal details and advance care planning'
                }
              }
            }
        ";
    }
}
