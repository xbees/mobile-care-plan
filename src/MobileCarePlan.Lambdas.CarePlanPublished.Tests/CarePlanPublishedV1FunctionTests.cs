﻿using MobileCarePlan.Lambdas.CarePlanPublished;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace MobileCarePlan.Lambdas.CarePlanPublished.Tests
{
    [ExcludeFromCodeCoverage]
    public class CarePlanPublishedV1FunctionTests
    {
        [Fact]
        public void CreateCarePlanPublishedV1Function()
        {
            Environment.SetEnvironmentVariable("Aws:Region", "eu-west-2");
            Environment.SetEnvironmentVariable("MobileCarePlanSettings:CarePlanPublishedEventTableName", "mobile-care-plan-events");
            Environment.SetEnvironmentVariable("MobileCarePlanSettings:CarePlanTableName", "mobile-care-plans");
            new CarePlanPublishedV1Function();
        }
    }
}
