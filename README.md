# mobile-care-plan
This repository includes:
- Lambda function for updating the care plan for the service user when a new update care plan event is detected on event bus
- API endpoints for retriving a care plan

## Useful links:
- [Digital Care Plans](https://ceracare.atlassian.net/wiki/spaces/PT/pages/2183299073/Digital+Care+Plans)

## Development
For local execution you must make use of the following settings in your `secrets.json`.

### Secrets
```json
{
    "Jwt": {
        "Key": "<see dev environment key for local testing>"
    }
}
```

### Local Lambda testing
- [Debugging .NET Core AWS Lambda functions using the AWS .NET Mock Lambda Test Tool](https://aws.amazon.com/blogs/developer/debugging-net-core-aws-lambda-functions-using-the-aws-net-mock-lambda-test-tool/)
- [Install AWS .NET Mock Lambda Test Tool](https://github.com/aws/aws-lambda-dotnet/tree/master/Tools/LambdaTestTool)

## Architecture and involved components

### Lambda functions
* MobileCarePlanPublishedV1 - triggered when a new updated care plan event is detected on SQS

### DynamoDB
Data relevant to this service is stored in DynamoDB collections, namely:
- mobile-care-plan-events
- mobile-care-plans