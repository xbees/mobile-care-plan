resource "aws_alb_target_group" "ecs_service" {
  name                 = var.service_name
  port                 = var.containerPort
  protocol             = "HTTP"
  vpc_id               = data.terraform_remote_state.core.outputs.vpc_id
  target_type          = "ip"
  deregistration_delay = 30

  health_check {
    healthy_threshold   = var.healthy_threshold
    unhealthy_threshold = var.unhealthy_threshold
    path                = var.healthcheck_path
    matcher             = 200
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_alb_listener_rule" "ecs_service" {
  listener_arn = data.terraform_remote_state.core.outputs.services_https_listener_arn
  priority     = var.unique_service_number

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.ecs_service.arn
  }

  condition {
    host_header {
      values = ["${var.service_name}-api.${var.domain_name}"]
    }
  }
}