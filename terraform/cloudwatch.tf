resource "aws_cloudwatch_log_group" "ecs_service" {
  name              = "/ecs/${var.service_name}"
  retention_in_days = var.retention_in_days
}