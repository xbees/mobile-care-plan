# -----------------------
# Role for ECS tasks
# -----------------------
module "task_role" {
  source                = "Cloud-42/ec2-iam-role/aws"
  version               = "4.0.0"
  principal_identifiers = ["ecs-tasks.amazonaws.com"]
  name                  = "${var.service_name}-ecs-role${var.role_suffix}"

  policy_arn = [
    module.task_access.arn,
    aws_iam_policy.dynamo_readwrite.arn
  ]
}
# -----------------------
# Access for ECS tasks
# -----------------------
module "task_access" {
  source  = "grodzik/iam_policy/aws"
  version = "0.1.0"
  name    = "${var.service_name}-task_access${var.role_suffix}"
  path    = "/"
  statements = [
    {
      sid = "SSM"
      actions = [
        "ssm:List*",
        "ssm:Describe*",
        "ssm:Get*",
      ]
      effect    = "Allow"
      resources = ["*"]
    },
    {
      sid = "KMS"
      actions = [
        "kms:List*",
        "kms:Get*",
        "kms:Describe*",
        "kms:Decrypt",
        "kms:Encrypt",
        "kms:GenerateDataKey",
      ]
      effect    = "Allow"
      resources = ["*"]
    },
    {
      sid = "SNS"
      actions = [ "sns:Publish" ]
      effect    = "Allow"
      resources = ["*"]
    },
    {
      sid = "CWLogs"
      actions = [
        "logs:List*",
        "logs:Get*",
        "logs:Describe",
        "logs:CreateLogGroup",
        "logs:CreateLogDelivery",
        "logs:PutLogEvents",
        "logs:FilterLogEvents",
        "logs:CreateLogStream",
      ]
      effect    = "Allow"
      resources = ["*"]
    }
  ]
}
# ---------------------------
# Access for DynomoDb tasks
# ---------------------------
data "aws_iam_policy_document" "dynamo" {
  statement {
    actions = [
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:UpdateItem",
      "dynamodb:DescribeTable",
      "dynamodb:Query",
    ]
    resources = [
      aws_dynamodb_table.mobile_care_plan_events.arn,
      aws_dynamodb_table.mobile_care_plans.arn,
      aws_dynamodb_table.mobile_care_plan_carer_reviews.arn
    ]
    effect = "Allow"
  }
}

resource "aws_iam_policy" "dynamo_readwrite" {
  name        = "${var.service_name}-dynamo-readwrite${var.role_suffix}"
  path        = "/"
  description = "IAM policy for reading and writing from dynamo"

  policy = data.aws_iam_policy_document.dynamo.json
}
