# -----------------------------
# ALB records
# PRD has no zone so no record is created
# -----------------------------
resource "aws_route53_record" "svc_dns_cname" {
  count   = var.env != "prd" ? 1 : 0
  zone_id = var.env != "demo" ? data.terraform_remote_state.core.outputs.zone_id : data.terraform_remote_state.core.outputs.demo_zone_id
  name    = "${var.service_name}-api"
  type    = "CNAME"
  ttl     = "60"

  records = [data.terraform_remote_state.core.outputs.services_alb.dns_name]
}
