locals {
  aws = {
    account_id  = var.account_id
    region      = var.region
    role_suffix = var.role_suffix
  }
}

module "care_plan_published_v1_queue" {
  source = "./modules/sqs"

  aws           = local.aws
  is_fifo_queue = false

  function   = module.care_plan_published_v1_function
  queue_name = "${var.service_name}-CarePlanPublishedV1"
}


resource "aws_sns_topic_subscription" "care_plan_published_subscription" {
  topic_arn            = "arn:aws:sns:${var.region}:${var.account_id}:${var.env}-dcp-careplan-published"
  protocol             = "sqs"
  endpoint             = module.care_plan_published_v1_queue.queue.arn
  raw_message_delivery = true
}
