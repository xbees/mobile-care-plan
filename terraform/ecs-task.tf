locals {
  build_number = var.build_number == "SSM" ? data.aws_ssm_parameter.current_deployed_version.value : var.build_number
}

module "container_definition" {
  source  = "cloudposse/ecs-container-definition/aws"
  version = "0.58.1"

  container_name  = var.service_name
  container_image = "${var.account_id}.dkr.ecr.${var.region}.amazonaws.com/${var.env}_${var.service_name}:${local.build_number}"

  essential = true

  log_configuration = {
    logDriver = var.logDriver
    options = {
      awslogs-region        = var.region
      awslogs-group         = "/ecs/${var.service_name}"
      awslogs-stream-prefix = var.log_stream_prefix
    }
  }

  port_mappings = [{
    containerPort = var.containerPort
    hostPort      = var.hostPort
    protocol      = var.protocol
  }]

  map_environment = {
    "ASPNETCORE_ENVIRONMENT"                 = "aws${var.env}"
    "DOTNET_ENVIRONMENT"                     = "aws${var.env}"
    "ENVIRONMENT"                            = "aws${var.env}"
    "Aws__Region"                            = var.region
    "Jwt__AccessTokenExpiration"             = "01:00:00"
    "Jwt__Enabled"                           = "true"
    "Jwt__Issuer"                            = var.jwt_issuer
    "Logs__Level"                            = var.log_level
    "Api__SwaggerEnabled"                    = var.swagger_enabled
    "Api__HealthEndpointPath"                = var.healthcheck_path
    "Storage__CarePlanEventsTableName"       = var.app_careplaneventstablename
    "Storage__CarePlansTableName"            = var.app_careplanstablename
    "Storage__CarePlanCarerReviewsTableName" = var.app_careplancarerreviewstablename
    "Instrumentation__CollectorEndpoint"     = data.aws_ssm_parameter.otel_collector_url.value
    "Instrumentation__CollectorTimeout"      = var.app_instrumentation_collectortimeout
  }

  secrets = [
    {
      name      = "Jwt__Key"
      valueFrom = aws_ssm_parameter.jwt_secret.name
    }
  ]
}

module "task" {
  source = "git::https://github.com/Cloud-42/terraform-aws-ecs.git//task-definition?ref=v0.1.0"

  container_definitions = module.container_definition.json_map_encoded_list
  family                = "${var.service_name}${var.role_suffix}"
  execution_role_arn    = "arn:aws:iam::${var.account_id}:role/ecsTaskExecutionRole"
  task_role_arn         = module.task_role.role.arn

  fargate_cpu    = var.container_cpu
  fargate_memory = var.container_memory
}
