locals {
  buildVersion    = var.build_number == "SSM" ? data.aws_ssm_parameter.current_deployed_version.value : var.build_number
  lambda_s3_key   = "${var.lambda_name}/${local.buildVersion}/MobileCarePlan.Lambdas.CarePlanPublished.zip"
  function_prefix = "${var.service_name}"
  lambda_env = {
    LAMBDA_NET_SERIALIZER_DEBUG      = var.lambda_debug,
    ASPNETCORE_ENVIRONMENT           = "aws${replace(var.env, "prd", "prod")}",
    Logs__Level                      = var.log_level
    Storage__CarePlanEventsTableName = var.app_careplaneventstablename,
    Storage__CarePlansTableName      = var.app_careplanstablename
  }
}

module "care_plan_published_v1_function" {
  source = "terraform-aws-modules/lambda/aws"

  create_package = false
  s3_existing_package = {
    bucket = local.lambda_s3_bucket_name
    key    = local.lambda_s3_key
  }
  function_name         = "${local.function_prefix}-CarePlanPublishedV1${var.role_suffix}"
  handler               = "MobileCarePlan.Lambdas.CarePlanPublished::MobileCarePlan.Lambdas.CarePlanPublished.CarePlanPublishedV1Function::HandleAsync"
  runtime               = "dotnet6"
  timeout               = 30
  memory_size           = 256
  environment_variables = local.lambda_env
  attach_tracing_policy = true
  tracing_mode          = "Active"
}

resource "aws_iam_role_policy_attachment" "care_plan_published_v1_dynamodb" {
  role       = module.care_plan_published_v1_function.lambda_role_name
  policy_arn = aws_iam_policy.dynamo_readwrite.arn
}
