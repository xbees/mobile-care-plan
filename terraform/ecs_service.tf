module "ecs_service" {
  source = "git::https://github.com/Cloud-42/terraform-aws-ecs.git//service?ref=v0.3.0"

  for_each = {
    for item in var.alb_services : item.service => item
  }

  name            = var.service_name
  cluster         = var.env
  task_definition = module.task.arn
  min_count       = var.min_count

  load_balancers = [{
    container_name   = "${var.service_name}"
    container_port   = var.containerPort
    target_group_arn = aws_alb_target_group.ecs_service.arn
  }]

  network_configurations = [{
    subnets          = data.terraform_remote_state.core.outputs.private_subnets
    security_groups  = [module.service_sg.security_group_id]
    assign_public_ip = false
  }]
}