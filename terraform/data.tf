locals {
  s3_bucket = var.region == "eu-west-2" ? "ceracare-${var.env}-tfstate" : "${replace(var.region, "-", "")}-ceracare-${var.env}-tfstate"
}

data "terraform_remote_state" "core" {
  backend = "s3"

  config = {
    bucket = local.s3_bucket
    key    = "network/terraform.tfstate"
    region = var.region
  }
}

data "aws_ssm_parameter" "current_deployed_version" {
  name = local.deployed_version
}

data "vault_generic_secret" "jwt_secret" {
  path = "secret/services/environments/${var.env}/jwt-secret"
}

data "aws_ssm_parameter" "otel_collector_url" {
  name = "${var.env}-otel-collector-service-url"
}
