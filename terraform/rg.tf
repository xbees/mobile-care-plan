resource "aws_resourcegroups_group" "service" {
  name = "${var.service_name}-resources"

  resource_query {
    query = <<JSON
{
  "ResourceTypeFilters": [
    "AWS::AllSupported"
  ],
  "TagFilters": [
    {
      "Key": "Service",
      "Values": ["${var.service_name}"]
    }
  ]
}
JSON
  }
}