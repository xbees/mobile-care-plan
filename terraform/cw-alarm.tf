resource "aws_cloudwatch_log_metric_filter" "ecs_service_errors_metric_filter" {
  name           = "${var.service_name}-errors-metric"
  pattern        = "{ $.LogLevel = \"Critical\" || $.LogLevel = \"Error\" || $.LogLevel = \"Warning\" }"
  log_group_name = aws_cloudwatch_log_group.ecs_service.name

  metric_transformation {
    name          = "${var.service_name}-ErrorsCount"
    namespace     = "${var.env}-${var.service_name}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_metric_alarm" "ecs_service_errors_alarm" {
  alarm_name          = "${var.service_name}-errors-alarm"
  alarm_description   = "Alarm to alert errors happen on the ECS ${var.env}-${var.service_name}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 1
  period              = 10
  treat_missing_data  = "notBreaching"

  namespace   = "${var.env}-${var.service_name}"
  metric_name = "${var.service_name}-ErrorsCount"
  statistic   = "Sum"

  alarm_actions = [aws_sns_topic.cw_errors_alarm_topic.arn]
  ok_actions    = [aws_sns_topic.cw_errors_alarm_topic.arn]
}

resource "aws_cloudwatch_metric_alarm" "ecs_service_request_latency_alarm" {
  alarm_name          = "${var.service_name}-monitoring-request-latency-anomaly"
  alarm_description   = "Alarm to alert request latency happen on the ECS ${var.env}-${var.service_name}"
  comparison_operator = "GreaterThanThreshold"
  datapoints_to_alarm = 2
  evaluation_periods  = 10
  threshold           = 0.5
  period              = 60
  treat_missing_data  = "missing"
  evaluate_low_sample_count_percentiles = "evaluate"

  namespace   = "AWS/ApplicationELB"
  metric_name = "TargetResponseTime"
  extended_statistic = "p90"
  
  dimensions = {
    TargetGroup  = aws_alb_target_group.ecs_service.arn_suffix,
    LoadBalancer = data.terraform_remote_state.core.outputs.services_alb_arn_suffix
  }

  alarm_actions = [aws_sns_topic.cw_errors_alarm_topic.arn]
  ok_actions    = [aws_sns_topic.cw_errors_alarm_topic.arn]
}

resource "aws_iam_role" "chatbot_role" {
  name               = "${var.service_name}-chatbot-role-${var.env}"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "chatbot.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "chatbot_role_attachment" {
  role       = aws_iam_role.chatbot_role.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess"
}

module "chatbot_slack_configuration" {
  source  = "waveaccounting/chatbot-slack-configuration/aws"
  version = "1.0.0"

  configuration_name = "${var.service_name}-chatbot-monitoring-${var.env}"
  iam_role_arn       = aws_iam_role.chatbot_role.arn
  slack_channel_id   = var.cloudwatch_alarm_chatbot_slack_channel_id
  slack_workspace_id = var.cloudwatch_alarm_chatbot_slack_workspace_id

  sns_topic_arns = [
    aws_sns_topic.cw_errors_alarm_topic.arn,
  ]
}
