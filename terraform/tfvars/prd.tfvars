env                                       = "prd"
region                                    = "eu-west-2"
account_id                                = "505011278047"
jwt_issuer                                = "https://api.ceradcp.care"
domain_name                               = "ceradcp.care"
swagger_enabled                           = false
dynamodb_read_capacity                    = 100
dynamodb_write_capacity                   = 70
lambda_debug                              = false
log_level                                 = "Information"
cloudwatch_alarm_chatbot_slack_channel_id = "C04FVUNTZ2L"
