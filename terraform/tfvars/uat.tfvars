env                     = "uat"
region                  = "eu-west-2"
account_id              = "698853163533"
domain_name             = "beta.ceradev.co.uk"
jwt_issuer              = "https://uat.ceradev.co.uk"
swagger_enabled         = true
dynamodb_read_capacity  = 12
dynamodb_write_capacity = 46
lambda_debug            = true
log_level               = "Information"
