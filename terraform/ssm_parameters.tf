resource "aws_ssm_parameter" "jwt_secret" {
  name  = "${var.service_name}-jwt-secret"
  type  = "SecureString"
  value = data.vault_generic_secret.jwt_secret.data["value"]
}
