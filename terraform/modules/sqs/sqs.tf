resource "aws_sqs_queue" "queue" {
  name       = "${local.queue_name}${local.queue_type_extension}"
  policy     = data.aws_iam_policy_document.sqs_policy.json
  fifo_queue = var.is_fifo_queue

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.dead_letter_queue.arn
    maxReceiveCount     = 3
  })
}

resource "aws_sqs_queue" "dead_letter_queue" {
  name       = "${local.queue_name}-dl${local.queue_type_extension}"
  fifo_queue = var.is_fifo_queue
}
