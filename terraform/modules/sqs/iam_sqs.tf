data "aws_iam_policy_document" "sqs_policy" {
  policy_id = local.queue_arn

  statement {
    sid    = "account_sns_to_sqs"
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions = ["SQS:SendMessage"]
    resources = [local.queue_arn]

    condition {
      test     = "StringLike"
      variable = "aws:SourceAccount"

      values = [var.aws.account_id]
    }
  }

  statement {
    sid    = "account_sqs_over_http"
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = [var.aws.account_id]
    }

    actions = ["SQS:SendMessage"]
    resources = [local.queue_arn]
  }
}