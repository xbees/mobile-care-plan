output "queue" {
  value = aws_sqs_queue.queue
}

output "name" {
  value = local.queue_name
}