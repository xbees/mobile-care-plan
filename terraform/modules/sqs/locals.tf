locals {
  queue_name           = "${var.queue_name}${var.aws.role_suffix}"
  queue_arn            = "arn:aws:sqs:${var.aws.region}:${var.aws.account_id}:${local.queue_name}"
  queue_type_extension = var.is_fifo_queue == true ? ".fifo" : ""
}
