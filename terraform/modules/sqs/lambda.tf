resource "aws_lambda_event_source_mapping" "event_source" {
  count = var.function == null ? 0 : 1
  batch_size       = 1
  event_source_arn = aws_sqs_queue.queue.arn
  enabled          = true
  function_name    = var.function.lambda_function_arn
}

resource "aws_lambda_permission" "allows_sqs_to_trigger_lambda" {
  count = var.function == null ? 0 : 1
  statement_id  = "AllowExecutionFromSQS"
  action        = "lambda:InvokeFunction"
  function_name = var.function.lambda_function_name
  principal     = "sqs.amazonaws.com"
  source_arn    = aws_sqs_queue.queue.arn
}