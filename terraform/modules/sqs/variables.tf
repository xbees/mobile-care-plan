variable "queue_name" {}
variable "function" {
    default = null
}
variable "aws" {}
variable "is_fifo_queue" {
    type    = bool
    default = false
}
