data "aws_iam_policy_document" "lambda_policy" {
  statement {
    actions   = [
      "sqs:DeleteMessage",
      "sqs:ReceiveMessage",
      "sqs:GetQueueAttributes"
    ]
    resources = [aws_sqs_queue.queue.arn]
    effect = "Allow"
  }
}

resource "aws_iam_policy" "sqs_read_write" {
  name        = "sqs-${local.queue_name}-read"
  path        = "/"
  description = "IAM policy for pulling messages from sqs"

  policy = data.aws_iam_policy_document.lambda_policy.json
}

resource "aws_iam_role_policy_attachment" "lambda_sqs_policy" {
  count      = var.function == null ? 0 : 1
  role       = var.function.lambda_role_name
  policy_arn = aws_iam_policy.sqs_read_write.arn
}