locals {
  deployed_version    = "${var.env}-${var.service_name}-deployed-version"
  aws_region_ssm_name = "${var.service_name}-Region"
  default_aws_tags = {
    Environment = var.env
    Service     = "${var.service_name}"
    Owner       = var.team_name
    ManagedBy   = "terraform"
  }

  lambda_s3_bucket_name = "${var.env}-cera-mobile-lambda-builds-${var.region}"
  cw_errors_alarm_topic_name  = "${var.service_name}-errors-alarm-topic"
  cw_errors_alarm_topic_arn   = "arn:aws:sns:${var.region}:${var.account_id}:${local.cw_errors_alarm_topic_name}"
}
