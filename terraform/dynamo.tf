resource "aws_dynamodb_table" "mobile_care_plan_events" {
  name           = var.app_careplaneventstablename
  hash_key       = "ServiceUserId"
  billing_mode   = "PROVISIONED"
  range_key      = "DateTime"
  read_capacity  = var.dynamodb_read_capacity
  write_capacity = var.dynamodb_write_capacity

  attribute {
    name = "ServiceUserId"
    type = "S"
  }

  attribute {
    name = "DateTime"
    type = "S"
  }
}

resource "aws_dynamodb_table" "mobile_care_plans" {
  name         = var.app_careplanstablename
  hash_key     = "ServiceUserId"
  billing_mode = "PROVISIONED"

  read_capacity  = var.dynamodb_read_capacity
  write_capacity = var.dynamodb_write_capacity

  attribute {
    name = "ServiceUserId"
    type = "S"
  }
}

resource "aws_dynamodb_table" "mobile_care_plan_carer_reviews" {
  name         = var.app_careplancarerreviewstablename
  hash_key     = "CarerId"
  billing_mode = "PROVISIONED"
  range_key    = "ServiceUserId"

  read_capacity  = var.dynamodb_read_capacity
  write_capacity = var.dynamodb_write_capacity

  attribute {
    name = "CarerId"
    type = "S"
  }

  attribute {
    name = "ServiceUserId"
    type = "S"
  }
}
