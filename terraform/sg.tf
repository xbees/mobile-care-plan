module "service_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "4.4.0"

  name        = "${var.service_name}-sg"
  description = "Security group for Service - ${var.service_name}"
  vpc_id      = data.terraform_remote_state.core.outputs.vpc_id

  ingress_with_source_security_group_id = [
    {
      rule                     = "all-tcp"
      source_security_group_id = data.terraform_remote_state.core.outputs.services_alb_sg
    },
  ]
  egress_rules = ["all-all"]
}