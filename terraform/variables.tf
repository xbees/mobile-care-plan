variable "env" {}
variable "service_name" { default = "mobile-care-plan" }
variable "region" { default = "eu-west-2" }
variable "account_id" {}
variable "build_number" {
  type        = string
  description = "Azure Pipelines build number"
  default     = "SSM"
}
variable "domain_name" {}
variable "jwt_issuer" {}
variable "swagger_enabled" { default = false }
variable "unique_service_number" { default = 113 }
variable "healthy_threshold" { default = 2 }
variable "unhealthy_threshold" { default = 4 }
variable "healthcheck_path" { default = "/health" }
variable "alb_services" {
  default = [
    {
      service : "mobile-care-plan"
      internal : false
      alias : "mobilecareplan"
    }
  ]
}
variable "min_count" { default = 1 }
variable "logDriver" { default = "awslogs" }
variable "log_stream_prefix" { default = "ecs" }
variable "retention_in_days" { default = 90 }
variable "container_cpu" { default = 512 }
variable "container_memory" { default = 1024 }
variable "containerPort" { default = "80" }
variable "hostPort" { default = "80" }
variable "protocol" { default = "tcp" }
variable "vault_address" { default = "https://vault.infra.ceradcp.care/" }
variable "vault_login_approle_role_id" {}
variable "vault_login_approle_secret_id" {}
variable "dynamodb_read_capacity" {}
variable "dynamodb_write_capacity" {}

variable "log_level" { default = "Information" }

variable "lambda_debug" { default = false }
variable "lambda_name" { default = "CarePlanPublished" }

variable "app_careplaneventstablename" { default = "mobile-care-plan-events" }
variable "app_careplanstablename" { default = "mobile-care-plans" }
variable "app_careplancarerreviewstablename" { default = "mobile-care-plan-carer-reviews" }
variable "app_instrumentation_collectortimeout" { default = "00:00:05" }

variable "cloudwatch_alarm_chatbot_slack_workspace_id" { default = "TACTT0CPP" }
variable "cloudwatch_alarm_chatbot_slack_channel_id" { default = "C04F376SBT7" }

variable "role_suffix" { default = "" }

variable "team_name" { default = "CarerExperience" }
