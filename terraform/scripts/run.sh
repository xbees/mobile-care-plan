#!/usr/bin/env bash
set -ex

env=$1
tfArgs="${@:2}"
apply=0
refresh=0

build=${BUILD_NUMBER}

if [[ $build == "" ]]; then
  build="SSM"
fi

if [[ $2 == "apply" ]]; then
  apply=1
fi

if [[ $2 == "refresh" ]]; then
  refresh=1
fi

_terraform() {
  tfenv install
  tfenv use

  terraform --version

  rm -rf .terraform && \
  terraform init -input=false -backend-config=backend/${env}.config && \
  terraform validate && \
  terraform plan -var-file="tfvars/${env}.tfvars" -var="build_number=${build}" -out=plan.${env}.out ${tfArgs}

}

_terraform