data "aws_iam_policy_document" "cw_errors_alarm_sns_policy" {
  policy_id = local.cw_errors_alarm_topic_arn

  statement {
    actions = [
      "SNS:Publish"
    ]

    condition {
      test      = "StringEquals"
      variable  = "AWS:SourceOwner"
      values    = [ var.account_id ]
    }

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    effect      = "Allow"
    resources   = [ local.cw_errors_alarm_topic_arn ]
    sid         = "intra_account_access"
  }
}

resource "aws_sns_topic" "cw_errors_alarm_topic" {
  name      = "${var.service_name}-errors-alarm-topic"
  policy    = data.aws_iam_policy_document.cw_errors_alarm_sns_policy.json
}