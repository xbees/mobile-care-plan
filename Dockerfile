### build image
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

ARG PAT
ARG BUILD_NUMBER

ENV APPLICATION_BUILD_NUMBER BUILD_NUMBER


## ************ install tools - we should probably have our own base image with these...

ENV NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED true
ENV VSS_NUGET_EXTERNAL_FEED_ENDPOINTS '{"endpointCredentials":[{"endpoint":"https://pkgs.dev.azure.com/CeraCare/_packaging/cera-framework%40Local/nuget/v3/index.json","username":"NoRealUserNameAsIsNotRequired","password":"'${PAT}'"}]}'
RUN wget -O - https://raw.githubusercontent.com/Microsoft/artifacts-credprovider/master/helpers/installcredprovider.sh  | bash

## ************

WORKDIR /code
COPY MobileCarePlan.sln .
COPY global.json .
COPY NuGet.Config .
COPY src/*/*.csproj ./
# copy all csproj files to their respective folders -> src/<project file name>/<project file name>.csproj
RUN for file in $(ls *.csproj); do mkdir -p src/${file%.*}/ && mv $file src/${file%.*}/; done

# restore using only the project files
RUN dotnet restore MobileCarePlan.sln --use-current-runtime
# copy everything else
COPY . .

RUN dotnet build MobileCarePlan.sln --use-current-runtime -c Release
# --version-suffix APPLICATION_BUILD_NUMBER
RUN dotnet publish "src/MobileCarePlan.Api/MobileCarePlan.Api.csproj" --use-current-runtime --no-build -c Release -o /app/


### runtime image
FROM mcr.microsoft.com/dotnet/runtime:6.0 AS artifact

WORKDIR /app
COPY --from=build /app/ .

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["dotnet", "MobileCarePlan.Api.dll"]